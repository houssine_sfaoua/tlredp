@extends('admin.layouts.master')

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{!! Form::open(['route' => 'formats.store', 'class' => 'form-horizontal'])!!}

{!! Form::hidden('edit',$format->id, ['id'=>'format-id']) !!}

<div class="form-group">
	{!! Form::label('nom_frm', trans('quickadmin::admin.format-create-nom_frm'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('nom_frm', $format->nom_frm, ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.format-create-nom_frm')]) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('pseudo_frm', trans('quickadmin::admin.format-create-pseudo_frm'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('pseudo_frm', $format->pseudo_frm, ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.format-create-pseudo_frm')]) !!}
	</div>
</div>

<div class="form-group">
        {!! Form::label('page_frm', trans('quickadmin::admin.format-create-print_frm'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::select('page_frm', ['interieure'=>'Intérieure','la_une'=>'La Une', 'derniere'=>'Dernière','centrale'=>'Centrale','2e_couverture'=>'2e Couverture','3e_couverture'=>'3e Couverture','4e_couverture'=>'4e Couverture','ouverture'=>'Ouverture','fermeture'=>'Fermeture','face_sommaire'=>'Face Sommaire','face_edito'=>'Face Edito','face_contributeurs'=>'Face Contributeurs','1e_tiers_de_magazine'=>'1e tiers de magazine','preferentiel'=>'Préférentiel','carnet_adresses'=>'Carnet d\'adresses','premium'=>'Premium','face_edito'=>'Face Edito'], $format->page_frm, ['class'=>'form-control']) !!}
        </div>
</div>

<div class="form-group">
        {!! Form::label('print_frm', trans('quickadmin::admin.format-create-print_frm'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::select('print_frm', ['couleur'=>'Couleur','noirblanc'=>'Noir & Blanc'], $format->print_frm, ['class'=>'form-control']) !!}
        </div>
</div>

<div class="form-group">
        {!! Form::label('value_ad_frm', trans('quickadmin::admin.format-create-value_ad_frm'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('value_ad_frm',$format->value_ad_frm, ['class'=>'form-control','placeholder'=> trans('quickadmin::admin.format-create-value_ad_frm')]) !!}
        </div>
</div>

<div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.criterias-create-btncreate'), ['class' => 'btn btn-primary']) !!}
        </div>
</div>

{!! Form::close() !!}

<script type="text/javascript">
$(document).ready(function(){

    //$('#value-ad-select').chosen();
});

</script>



@endsection