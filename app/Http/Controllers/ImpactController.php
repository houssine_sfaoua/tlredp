<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Author;
use App\Rubrique;
use App\Format;
use App\Impact;
use App\ImpactClient;
use App\SupportFormat;
use App\ClientCompany;
use App\Criteria;
use App\Subcriteria;
use App\Appear;
use Image;
use App\User;
use Auth;
use Yajra\Datatables\Facades\Datatables;

set_time_limit(0);
ini_set("memory_limit",-1);
ini_set('max_execution_time', 0);

class ImpactController extends Controller
{
    
    public function index(){
        //dd(Auth::user());
        // $todayDate = \Carbon\Carbon::now();
        // $formatedToday = \Carbon\Carbon::parse($todayDate)->format('Y-m-d');
        // $imapcts = Impact::whereDate_integ_imp($formatedToday)->get();
        // $path = Storage::disk('public')->getAdapter()->getPathPrefix();
        // //dd($path);
        // // if (file_exists($path)) {
        // //     dd("file exist");
        // //     // mkdir($save_path, 666, true);
        // // }
        // $img = Image::make($path.'/impacts/2/21542795116_copie.png')->resize(null, 1800, function ($constraint) {
        //     $constraint->aspectRatio();
        // });
        // $img->save($path.'/impacts/2/21542795116_copie.png');
        //dd($img);
        //$imapcts = Impact::whereUserid_imp(Auth::user()->id)->get();
        
        /*$imapcts = Impact::join('impact_clients','impact_clients.impactid_imp_cl','=','impacts.userid_imp')
                                ->join('client_companies','impact_clients.clientid_imp_cl','=','client_companies.id')
                                ->where("impacts.userid_imp",Auth::user()->id)
                                ->get();

        //dd($imapcts);
        // $impact = Impact::where('title_imp', 'retombe scan')->get();
        // dd(Datatables::collection($impact)->make());
        // dd(Datatables::collection(Impact::all())->make());
        foreach ($imapcts as $key => $impact) {
            $clientImpacts = ImpactClient::whereImpactid_imp_cl($impact->id)->get();
            $aClientCompagny = [];
            $aCriteriaImpact = [];
            $aSubCriteriaImpact = [];
            foreach($clientImpacts as $j => $clientImpact) {
                $criteria = Criteria::find($clientImpact->criteriaid_imp_cl);
                $subCriteria = Subcriteria::find($clientImpact->subcriteriaid_imp_cl);

                $aClientCompagny[] = ClientCompany::find($clientImpact->clientid_imp_cl);
                $aCriteriaImpact[] = (isset($criteria) && !is_null($criteria) ? $criteria : []);
                $aSubCriteriaImpact[] = (isset($subCriteria) && !is_null($subCriteria) ? $subCriteria : []);
            }
            //dump($aSubCriteriaImpact);
            $impact->clientCompany = $aClientCompagny;
            $impact->criteriaImpact = $aCriteriaImpact;
            $impact->subCriteriaImpact = $aSubCriteriaImpact;
            //dd($impact);
        }
        // dd($imapcts);
       //dd(Datatables::of($imapcts)->make(true));
       compact('imapcts')*/
       //$data = User::latest()->get();
           //dd(Datatables::of($data));
        return view('admin.impacts.index');
    }

    /**
    * create impact view
    */
    public function create(){

        /*$supportTemp = array();
        array_push($supportTemp,array(""=>"autre"));

        $aSupport = \App\Support::all();

        array_push($supportTemp,$aSupport);


        dd($supportTemp);*/
        


        $impact = new Impact();

        return view('admin.impacts.create',compact('impact'));
    }

    public function edit($id){
        
        $impact  = Impact::findOrFail($id);

        $impact->img=$impact->scan_imp;

        $impact->impctclient = ImpactClient::whereImpactid_imp_cl($impact->id)->get();

        return view('admin.impacts.edit', compact('impact'));
    }


    /**
    * store form impact
    */
    public function store(Request $request){
        $data = $request->all();
        
//dd($data);
        $validatedData = $request->validate([
            'title' => 'required',
            'language' => 'required',
            'author'=>'required',
            'date'=>'required',
            'date_integ'=>'required',
            'pace'=>'required',
            'support'=>'required',
            'num_edit'=>'',
            'num_page'=>'',
            'rubid'=>'required',
            'format_imp'=>'required',
            'appear'=>'required',
            'client_imp'=>'required',
            'nature_imp'=>'required'
        ]);

        //REMOVE pub value
        // 'pub_value'=>'required',
        //'print'=>'required',

        $title = $data['title'];
        $language = $data['language'];
        $resume = (isset($data['resume']) ? $data['resume']: NULL);
        $imgname = (isset($data['scan']) ? $data['scan']: NULL);
        $selectionpage = 1;
        $authorId = $data['author'];
        $aNewAuthors = $data['new_author_lbl'];
        $date = date('Y-m-d', strtotime($data['date']));
        $current_time = \Carbon\Carbon::now()->format('H:i:s');
        $dateInte = $data['date_integ'];
        $dateIntegration = date('Y-m-d H:i:s', strtotime("$dateInte $current_time"));
        $pace = $data['pace'];

        $supportId = $data['support'];
        $numEdit = $data['num_edit'];
        $numpage = $data['num_page'];
        $linkpage = $data['link_page'];


        $rubId = $data['rubid'];

        $aNewRub = $data['new_rub_lbl'];

        $formatId = $data['format_imp'];
        //$aNewFormat = $data['new_format_lbl'];
        //$print = $data['print'];
        //$pubvalue = $data['pub_value'];
        $appear = $data['appear'];
        $appearId = $data['appear_select_imp'];
        $appeartxt = $data['appear-input'];
        $natureimp = $data['nature_imp'];

        $typeImp = $data['type_imp'];
        $programName = $data['program_name_imp'];

        if(isset($appeartxt) && $appearId == "autre"){
            $oAppear = Appear::create(array("label_appe"=>$appeartxt));
            $appearId = $oAppear->id;
        }

        //dd($appearId);

        // save new author
        foreach ($aNewAuthors as $key => $newauthor) {
            //dd(isset($newauthor));
            if($newauthor!=null){
                 $author= Author::create(array("name_aut"=>$newauthor));
                 $authorId = $author->id;
            }   
        }

         // save new rubrique
        foreach ($aNewRub as $key => $newrub) {
            //print_r($newrub);
            if($newrub!=null){
                $rubrique= Rubrique::create(array("nom_rub"=>$newrub));
                $rubId = $rubrique->id;
            } 
        }

         // save new rubrique
        /*foreach ($aNewFormat as $key => $newformat) {
            //print_r($newrub);
            if($newformat!=null){
                $format = Format::create(array("nom_frm"=>$newformat,"print_frm"=>$print,"value_ad_frm"=>$pubvalue));
                $formatId = $format->id;

                SupportFormat::create(array("support_id"=>$supportId,"format_id"=>$formatId));
            }
        }*/




        $aImapct = array(
                "title_imp" =>$title,
                "resume_imp"=>$resume,
                "date_imp"=> $date,
                "date_integ_imp"=> $dateIntegration,
                "selection_imp"=>$selectionpage,
                "authorid_imp" =>$authorId,
                "userid_imp" => strval(Auth::user()->id),
                "period_tag_imp" => $data['period_tag'],
                "pace_imp"=>$pace,
                "supportid_imp"=>$supportId,
                "num_edit_imp"=>$numEdit,
                "num_page_imp"=>$numpage,
                "rubid_imp"=>$rubId,
                "formatid_imp"=>$formatId,
                "appear_imp"=>$appear,
                "appearid_imp"=>$appearId,
                "link_imp"=> $linkpage,
                "nature_imp"=>$natureimp,
                "type_imp"=>$typeImp,
                "program_name_imp"=>$programName
            );

            //dd($aImapct);
            if(isset($data['edit'])){
                $result = Impact::where('id', $data['edit'])->update($aImapct);
                
                $impact = Impact::where('id', $data['edit'])->first();

                $deletedItem = ImpactClient::whereImpactid_imp_cl($data['edit'])->delete();

                // update support format

                // save impact client

                foreach ($data['client_imp'] as $key => $clientValue) {
                    $criteriaid= $data['criteria_imp'][$key];
                    $subcriteriaid= (isset($data['subcriteria_imp'])) ? $data['subcriteria_imp'][$key]: 0;

                    $aImpactClient = array(
                        "impactid_imp_cl"=>$data['edit'],
                        "clientid_imp_cl"=>$clientValue,
                        "criteriaid_imp_cl"=>$criteriaid,
                        "subcriteriaid_imp_cl"=>$subcriteriaid
                    );

                    $impctClient = ImpactClient::create($aImpactClient);
                }
                
            }else{
                $impact = Impact::create($aImapct);

                // save impact client
                foreach ($data['client_imp'] as $key => $clientValue) {
                    $criteriaid= $data['criteria_imp'][$key];
                    $subcriteriaid= (isset($data['subcriteria_imp'])) ? $data['subcriteria_imp'][$key]: 0;


                    $aImpactClient = array(
                        "impactid_imp_cl"=>$impact->id,
                        "clientid_imp_cl"=>$clientValue,
                        "criteriaid_imp_cl"=>$criteriaid,
                        "subcriteriaid_imp_cl"=>$subcriteriaid
                    );

                    $impctClient = ImpactClient::create($aImpactClient);
                }
            }
            //dd($impact);
            // store source image
            if ( NULL !== $request->file('source') && $request->file('source')->isValid()) {
                // test file exist
                if (isset($data['edit'])) {
                    if (Storage::disk('public')->exists($impact['source_imp'])) {
                        //dd($impact['source_imp']);
                        Storage::disk('public')->delete($impact['source_imp']);
                        $resultStorage = Storage::disk('public')->putFile('impacts/source/'.$impact->id, $request->file('source'));
                        $impact->source_imp = $resultStorage;
                        $impact->update();
                    }
                } else {
                    $resultStorage = Storage::disk('public')->putFile('impacts/source/'.$impact->id, $request->file('source'));
                    $impact->source_imp = $resultStorage;
                    //dd($resultStorage);
                    $impact->save();
                }
            }
            // store video audio files 
            if ( NULL !== $request->file('recordscan') && $request->file('recordscan')->isValid()) {
                $files =   Storage::disk('public')->allFiles('impacts/audiovideo/'.$impact->id.'/');
                //dd($files);
                Storage::delete($files);
                $file = $request->file('recordscan');
                $extention = $file->getClientOriginalExtension();
                if (str_contains($file->getMimeType(), 'audio/')) {
                    $name =  'audio_'.$impact->id.'.' . $extention;
                } else {
                    $name =  'video_'.$impact->id.'.' . $extention;
                }
                $resultAVStorage = Storage::disk('public')->putFileAs('impacts/audiovideo/'.$impact->id,$file,$name);
                $impact->record_imp = $resultAVStorage;
                $impact->save();
            }

            if(isset($request->logo) && count($request->logo) > 0){

                //dd($request->logo);
                //File::exists($myfile);

                $files =   Storage::disk('public')->allFiles('impacts/'.$impact->id.'/');
                //dd($files);
                Storage::delete($files);
                
                

                $scanArray = array();
                $scanTempArray = array();
                $aScanImage = array();
                $newScanArray = array();

                $aScanImage = json_decode($impact->scan_imp);
                // remove scan item
                if (isset($aScanImage) && isset($request->ids)) {
                    foreach ($aScanImage as $keyS => $scan) {
                        if (isset($request->ids[$keyS])) {
                            // dump($request->ids[$keyS]);
                            if($scan->position == $request->ids[$keyS]) {
                                //dump($scan);
                                unset($aScanImage[$keyS]);
                            }
                        }
                    }
                }
                // dd($aScanImage);
                // if (isset($request->file("source"))) {

                // }
                
                foreach ($request->logo as $key => $logo) {
                    $current_time = \Carbon\Carbon::now()->timestamp;

                    if(isset($logo)){
                        $image = $logo;
                        $image = str_replace('data:image/png;base64,', '', $image);
                        $image = str_replace(' ', '+', $image);

                        array_push($scanArray,array("position"=>$request->position[$key],"scan"=>$request->position[$key].''.$current_time.'.png'));
                        
                        $newScanArray = json_decode(json_encode($scanArray));

                        if(isset($impact)){
                            $path = Storage::disk('public')->getAdapter()->getPathPrefix();
                            $result = Storage::disk('public')->put('impacts/'.$impact->id.'/'.$request->position[$key].''.$current_time.'.png',base64_decode($image));
                            $img = Image::make($path.'/impacts/'.$impact->id.'/'.$request->position[$key].''.$current_time.'.png')->resize(1165, null, function ($constraint) {
                                $constraint->aspectRatio();
                            });
                            $img->save($path.'/impacts/'.$impact->id.'/'.$request->position[$key].''.$current_time.'.png');
                        }
                    }
                }

                //dd($newScanArray);
                $impact->scan_imp = json_encode($newScanArray);
                $impact->update();

                        // if(isset($aScanImage) && is_array($aScanImage)){
                        //     $scanTempArray = array_merge($aScanImage,$newScanArray);
                        // }else{
                        //     $scanTempArray = array_merge($scanTempArray,$newScanArray);
                        // }

                        //dd(json_encode($scanTempArray));
                //dd($scanTempArray);
                // if($result && count($scanTempArray)>0 ){
                //         $impact->scan_imp = json_encode($scanTempArray);
                //         //dd($impact);
                //         $impact->save();
                // } else {
                //     $impact->scan_imp = json_encode($scanTempArray);
                //         //dd($impact);
                //         $impact->update();
                // }
            }
        

        return redirect()->route('impacts.index')->withMessage(trans('quickadmin::admin.item-controller-successfully_created'));

    }

    /**
    *
    */
    public function getFile(Request $request){
        //print_r($request->file('pngimageData'));die;
        //print_r(base64_decode($request->pngimageData));die;
        //print_r(config('quickadmin.homeRoute'));
        //$image = base64_decode($request->pngimageData);
        //file_put_contents('C:/wamp64/www/tlredp/public/quickadmin/images/uploads/newImage.JPG',$image);
        // add directory linked to customer
        // save file into dir

        $image = $request->pngimageData; // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = 'test.png';

        print_r(Storage::disk('public')->put('2/'.$imageName,base64_decode($image)));die;
        
    }

    /**
     * Destroy specific user
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id){

        //dd($id);
        $impact = Impact::findOrFail($id);
        $result = Impact::destroy($id);
        
        return redirect()->route('impacts.index')->withMessage(trans('quickadmin::admin.item-controller-successfully_deleted'));
    }
}
