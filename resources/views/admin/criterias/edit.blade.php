@extends('admin.layouts.master') 
@section('content') 
{{csrf_field()}}


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


{!! Form::open(['route' => 'criteria.store', 'class' => 'form-horizontal']) !!}

{!! Form::hidden('edit',$criteria->id, ['id'=>'criteria-id']) !!}

<div class="form-group">
	{!! Form::label('label_cri', trans('quickadmin::admin.criteria-create-labelcr'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('label_cri', $criteria->label_cri, ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.criteria-create-labelcr')]) !!}
	</div>
</div>

<div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.criterias-create-btncreate'), ['class' => 'btn btn-primary']) !!}
        </div>
</div>

{!! Form::close() !!}

@endsection