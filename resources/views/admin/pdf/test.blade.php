<!DOCTYPE html>
<html>
<head>
	<title>{{ $namePdf }}</title>
    @include('admin.pdf.style')
    <style>
        body {
            font-family: 'arabicfont', sans-serif;
        }
        #img-support{
            width: 9%;
        }
        .fiche-container p {
            line-height:0;
            padding:0;
            margin:0;
            font-size:12px;
            font-weight: bold;
        }
        p.page-paraph {
            /* text-align:right; */
            margin:10% 0 0 80% !important;
            padding:0;
            
        }
    </style>
</head>
<body>
<div class="container">

    <!-- <p style="text-align:center;font-size:18px;">Toutelarevuedepresse.ma</p> -->
    <p style="color:blue;margin:0;padding:0;">Veille rédactionnelle > <span style="color:gray;font-size:12px;">{{ $aImpact->clientCampany->nom_cl }}</span></p>
    @if(isset($aImpact->images))
        @foreach ($aImpact->images as $key=>$image)
        <div style="height:100%">
            <div class="header-field">
            <div class="support-section" style="margin-top:12px;">
                    @if($aImpact->supportobj->logo_sup!= null)
                            <img src="{{ env('APP_URL').'/storage/app/public/uploads/support/'.$aImpact->supportobj->id.'/'.$aImpact->supportobj->logo_sup }}" alt="img support" id="img-support" style="">
                        @else
                            <img src="{{ env('APP_URL').'/storage/app/public/newspapericon.jpg' }}" alt="img support" id="img-support" style="">
                        @endif
                <div id="support-info" >
                    <!-- <h2 class="support-name"></h2> -->
                    <p style="">{{$aImpact->supportobj->name_sup}} <br/> {{ $aImpact->supportobj->form_edit_sup }} . {{$aImpact->supportobj->period_sup }} . {{$aImpact->supportobj->lng_edit_sup }} . {{$aImpact->supportobj->postition_sup }}</p>
                </div>
                </div>
                <div style="clear:both;"></div>
                <!-- <br/> -->
                <div class="fiche-container" style="margin-top:15px;">
                    <h4 style="padding:0;margin:0;text-transform:uppercase;font-size:11px;"><span class="strong">{{$aImpact->title_imp}}</span></h4>
                    <p>Article <span class="strong">{{ $aImpact->pace_imp }}</span> paru le <span class="strong">{{ Carbon\Carbon::parse($aImpact->date_imp)->format('d-m-Y') }}</span> dans la rubrique <span class="strong">« {{ $aImpact->rubriqueobj->nom_rub }} »</span>, signé par <span class="strong">{{ $aImpact->authorobj->name_aut }}</span></p>
                    <p>Article de <span class="strong">{{ $aImpact->formatobj->nom_frm }}</span>, édité en <span class="strong">{{ $aImpact->formatobj->print_frm }}</span>, d’une valeur publicitaire équivalente à {{ $aImpact->formatobj->value_ad_frm }} MAD </p>
                </div>
            </div>
        <br/>
        <!-- <div class="title-container">
            <p class="resume-impact" style="">{{$aImpact->resume_imp}}</p>
        </div> -->
        
        
        <!-- <p class="info-impact" style="font-size: 13px;">Article parue a ( date )en page ( page ) au format (publicitaire de tiers en hauteur)</p> -->
        
        <!-- <div class="image-container">
            <img src="{{ env('APP_URL').'/storage/app/public/uploads/impacts/'.$aImpact->id.'/'.$image->scan }}" alt="" class="img-scan" style="">
        </div>    -->
        <table style="width:100%;height:100%;margin: 0;padding: 0;border: 0;">
            <tbody>
                <tr>
                    <td align="center">
                        <img src="{{ env('APP_URL').'/storage/app/public/uploads/impacts/'.$aImpact->id.'/'.$image->scan }}" alt=""  style="">
                    </td>
                </tr>
            </tbody>
        </table>
            <!-- <p class="page-paraph" style="">page {{ ($key+1) }}/ {{ count($aImpact->images) }} </p> -->
            <!-- <div class="fiche-container">
                <p style="text-decoration: underline;">Fiche de la retombée</p>
                <p >Nom de la rubrique: {{ $aImpact->rubriqueobj->nom_rub }}</p>
                <p >Auteur: {{ $aImpact->authorobj->name_aut }}</p>
                <p >Date de parution: {{ $aImpact->date_imp }}</p>
                <p>Ton de la retombée: {{ $aImpact->pace_imp }}</p>
                <p>Format: {{ $aImpact->formatobj->nom_frm }}</p>
                <p>Impression: {{ $aImpact->formatobj->print_frm }}</p>
            </div> -->
    </div>
       
        @endforeach
@endif
    
	
	
</div>
</body>
</html>