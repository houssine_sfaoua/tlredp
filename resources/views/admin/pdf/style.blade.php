<style>
    #img-support{
        float:left;
    }
    #support-info{
        font-size:10px;
    }

    p.title-impact{
        line-height: 2;
        margin: 15px 0 0 0 !important;
        width: 100%;
        font-size: 15px;
        font-weight: bold;
        text-transform: uppercase;
        text-align: center;
    }

    p.resume-impact{
        margin: 0 0 0 0 !important;
        width: 100%;
        font-size:12px;
    }
    
    .support-name{
        font-size:10px;
        margin: 0;
    }
    .support-subname{
        margin: 0;
        font-size: 5px;
    }    
    .img-scan{
        width: 60%;
        display: block;
    }
    .image-container{
        /* width: 595px;
        height: 842px; */
        width: 100%;
        display: block;
        margin-top: 2%;
        text-align: center;
        /*margin-left: 10%; */
    }
    .resume-container {
        margin-top: 2%;
    }
    #support-info{
        /* width: 10%; */
        float: left;
        margin-left: 15px !important;
    }
    #support-info p {
        margin:0;
        padding: 0;
    }
    div.header-field {
        /* background-color: lightblue;
        border: 1px solid gray; */
        padding: 12px;
    }
</style>