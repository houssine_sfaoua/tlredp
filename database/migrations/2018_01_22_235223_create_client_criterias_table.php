<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientCriteriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_criterias', function (Blueprint $table) {
            $table->integer('criteria_id')->unsigned()->nullable();
            $table->foreign('criteria_id')->references('id')
                    ->on('criterias');

            $table->integer('client_id')->unsigned()->nullable();
            $table->foreign('client_id')->references('id')
                    ->on('client_companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_criterias');
    }
}
