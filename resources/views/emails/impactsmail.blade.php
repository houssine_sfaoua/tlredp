<div>
@foreach($aImpactCriteria as $key => $impactCriteria)
                <h2 style="font-size: 15px;">{{ \App\Criteria::getNameCriteria($key) }}</h2>
    <table width="954" cellspacing="0" cellpadding="2" border="0" style="border-collapse:collapse">
        <tbody>
            <tr style="border-bottom:1px solid #a9a9a94d;">
            <td width="100" height="25" style="background-color:rgb(211,228,246);font-size: 12px;font-weight: bold;font-family: Arial;text-align: center;">Date de parution</td>
                <td width="140" height="25" style="background-color:rgb(211,228,246);font-size: 12px;font-weight: bold;font-family: Arial;text-align: center;">Nom du support</td>
                <td width="500" height="25" style="background-color:rgb(211,228,246);font-size: 12px;font-weight: bold;font-family: Arial;text-align: center;">Titre</td>
                <td width="100" height="25" style="background-color:rgb(211,228,246);font-size: 12px;font-weight: bold;font-family: Arial;text-align: center;">Image</td>
                <td width="100" height="25" style="background-color:rgb(211,228,246);font-size: 12px;font-weight: bold;font-family: Arial;text-align: center;">Source</td>
                <td width="100" height="25" style="background-color:rgb(211,228,246);font-size: 12px;font-weight: bold;font-family: Arial;text-align: center;">Résumé</td>
            </tr>
                @foreach($impactCriteria as $impact)
                
                    <tr style="border-bottom:1px solid #a9a9a94d;">
                        <td width="140" height="25" style="font-size: 12px;font-family: Arial;text-align: center;">@if($impact->date_imp!=null){{ Carbon\Carbon::parse($impact->date_imp)->format('d-m-Y') }}@endif</td>
                        <td width="500" height="25" style="font-size: 12px;font-family: Arial;text-align: center;"><a name="myname" style="text-transform:uppercase;">{{ $impact->name_sup }}</a></td>
                        <td width="100" height="25" style="font-size: 12px;font-family: Arial;text-align: center;"><a style="text-decoration: none;" href="{{ env('APP_URL') }}/public/extranet/{{ Crypt::encrypt($impact->impactid_imp_cl) }}">{{ $impact->title_imp }}</a></td>
                        <td width="100" height="25" style="font-size: 12px;font-family: Arial;text-align: center;">@if($impact->source_imp!=null)<a href="{{ env('APP_URL') }}/public/extranet/images/{{ $impact->impactid_imp_cl }}" style="text-decoration: none;">&#x2713;</a>@else <span>--</span>@endif</td>
                        <td width="100" height="25" style="font-size: 12px;font-family: Arial;text-align: center;">@if($impact->link_imp!=null)<a href="{{ $impact->link_imp }}" style="text-decoration: none;">&#x2713;</a>@else <span>--</span>@endif</td>
                        <td width="100" height="25" style="font-size: 12px;font-family: Arial;text-align: center;">@if($impact->resume_imp!=null)<a href="{{ env('APP_URL') }}/public/extranet/resume/{{ Crypt::encrypt($impact->id) }}" style="text-decoration: none;">&#x2713;</a>@else <span>--</span>@endif</td>
                    </tr>
                @endforeach
            
        </tbody>
    </table>
    @endforeach
    <a href="{{ env('APP_URL') }}/public/extranet/joint/{{ $impactFile }}">Click here</a>
</div>
<script type="text/javascript">
    document.addEventListener('click', function (e) {
    if (this.classList.contains('disabled')) {
        e.stopPropagation();
        e.preventDefault();
    }
}, true);
</script>