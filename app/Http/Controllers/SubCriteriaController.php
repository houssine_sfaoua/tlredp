<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subcriteria;

class SubCriteriaController extends Controller
{
    
    public function index(){

        $subCriteria = Subcriteria::all();

        return view("admin.subcriterias.index",compact('subCriteria'));
    }


    public function create(){
        return view("admin.subcriterias.create");
    }

    public function edit($id){

        $subcriteria  = Subcriteria::findOrFail($id);

        return view("admin.subcriterias.edit",compact('subcriteria'));
    }

    public function store(Request $request){
        //dd($request->all());
        $data = $request->all();

        if(isset($data['edit'])){

            $aSubCriteria = array("label_subcr"=>$data['label_subcr'],"criteria_id_subcr"=>$data['criteria_id_subcr']);

            Subcriteria::where('id', $data['edit'])->update($aSubCriteria);

        }else{

            $subCriteria=Subcriteria::create($request->all());
        }
        
        return redirect()->route('subcriteria.index')->withMessage(trans('quickadmin::admin.item-controller-successfully_created'));
    }
}