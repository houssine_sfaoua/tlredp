<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => '',
	'subject'               => '',
	'keywords'              => '',
	'creator'               => 'Laravel Pdf',
	'display_mode'          => 'fullpage',
	'font_path' => base_path('resources/fonts/'),
	'font_data' => [
		'arabicfont' => [
			'R'  => 'XB Riyaz.ttf',    // regular font
			'B'  => 'XB Riyaz.ttf',       // optional: bold font
			'I'  => 'XB Riyaz.ttf',     // optional: italic font
			'BI' => 'XB Riyaz.ttf', // optional: bold-italic font
			'useOTL' => 0xFF,    // required for complicated langs like Persian, Arabic and Chinese
			'useKashida' => 75,  // required for complicated langs like Persian, Arabic and Chinese
		]
	]
];
