<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PdfOrdersNum extends Model
{
    protected $table = "pdf_orders_num";
    
    public $timestamps = false;

    protected $fillable = array("pdfnum_pon");

    public static function getPdfNumOrder() {
        $pdfOrderNum = PdfOrdersNum::All()->last();
        //dd($pdfOrderNum);
        $stamp = "0000000";
        if (isset($pdfOrderNum)){
            //dd(intval($pdfOrderNum[0]->pdfnum_pon));
            $number = intval($pdfOrderNum->pdfnum_pon);
            $number++;
            if ($number > 0 && $number <=9) {
                $stamp = sprintf('%07d', $number);
            } else if ($number >= 10 && $number <= 99) {
                $stamp = sprintf('%06d', $number);
            } else if ($number >=  100 && $number <= 999) {
                $stamp = sprintf('%05d', $number);
            } else if ($number >= 1000 && $number <= 9999) {
                $stamp = sprintf('%04d', $number);
            }
        }
        PdfOrdersNum::create(array("pdfnum_pon"=>$stamp));
        return $stamp;
    }
}