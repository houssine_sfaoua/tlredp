<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Impact;
use PDF;
use Mail;
use Image;
use Auth;
use App\ImpactClient;
use App\Cronjob;
use App\ClientCompany;
use Illuminate\Support\Facades\Crypt;
use App\Jobs\GenerateListPdf;
use PDFTCPDF;
use TCPDF_FONTS;

set_time_limit(0);
ini_set("memory_limit",-1);
ini_set('max_execution_time', 0);

class ReviewController extends Controller
{
    public function index(){
        $aImpactCriteria = [];
        $impactFile = "";
        //dump(Auth::user()->id);

       
        //dd(config('quickadmin.chargeresumes'));
        

        return view('admin.review.index',compact('aImpactCriteria','impactFile'));
    }
    // generate pdf with TCPDF
    public function generateTcpdf(Request $request) {
        $data = $request->all();
        //dd($data);
        $impactIds = array();
        if(isset($data['impactid'])){
            $impactIds=explode(",", $data['impactid']);
        }

        $aImpactArray = array();
        foreach ($impactIds as $key => $impactid) {
            if (isset($impactid) && $impactid != null && !is_null($impactid)){
                $aImpact = Impact::find($impactid);
                dump($aImpact['type_imp']);
                $clientImpact = ImpactClient::whereImpactid_imp_cl($aImpact->id)->first();
                $aImpact->authorobj = $aImpact->author;
                $aImpact->supportobj = $aImpact->support;
                $aImpact->rubriqueobj = $aImpact->rubrique;
                $aImpact->formatobj = $aImpact->format;
                $aImpact->clientCampany = ClientCompany::find($clientImpact->clientid_imp_cl);
                $aImpact->images = json_decode($aImpact->scan_imp);
                array_push($aImpactArray,$aImpact);
            }
        }

        dd($aImpactArray);
        //dd($aImpactArray);
        /******************************************************************************** */
        PDFTCPDF::SetTitle('Hello World');
        PDFTCPDF::SetAutoPageBreak(false, 0);
        PDFTCPDF::setImageScale(PDF_IMAGE_SCALE_RATIO);

        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'ltr';
        $lg['a_meta_language'] = 'fr';
        $lg['w_page'] = 'page';

        $dinfontname = TCPDF_FONTS::addTTFfont('http://localhost:8888/tlredp/public/fonts/din-bold.ttf', 'TrueTypeUnicode', '', 32);
        $geometosfontname = TCPDF_FONTS::addTTFfont('http://localhost:8888/tlredp/public/fonts/Geometos.ttf', 'TrueTypeUnicode', '', 32);
        $geometosroundedfontname = TCPDF_FONTS::addTTFfont('http://localhost:8888/tlredp/public/fonts/Geometos_Rounded.ttf', 'TrueTypeUnicode', '', 32);
        $dinarabicfontname = TCPDF_FONTS::addTTFfont('http://localhost:8888/tlredp/public/fonts/DINNEXTLTARABIC-LIGHT.ttf', 'TrueTypeUnicode', '', 32);
        //dd($fontname);
        // use the font
        PDFTCPDF::SetFont($geometosfontname, '', 13, '', false);

        // set some language-dependent strings (optional)
        PDFTCPDF::setLanguageArray($lg);

        foreach ($aImpactArray as $key => $impact) {
            if(isset($impact->images))
            {

            foreach ($impact->images as $key=>$image)
            {

            //dd($impact);
            PDFTCPDF::AddPage();
            //PDFTCPDF::SetMargins(100, 100, 10, true);
            $html = "<h1>TEST TEST left </h1>";
            $righthtml = "<h1>Veille rédactionnelle</h1>";
            $clientName = "<h1></h1>";
            $container = "<div></div>";
            $descImpact = "<p>Article ".$impact->pace_imp." paru le ".\Carbon\Carbon::parse($impact->date_imp)->format('d-m-Y') ." dans la rubrique« ".$impact->rubriqueobj->nom_rub." », signé par ".$impact->authorobj->name_aut." <br/>Article de ".$impact->formatobj->nom_frm.", édité en ".$impact->formatobj->print_frm.", d’une valeur publicitaire équivalente à ".$impact->formatobj->value_ad_frm." MAD</p>";
            $containerImage = "<div></div>";
            $titleArticle = "<p>".$impact->title_imp."</p>";
            $supportDesc = "<p >".$impact->supportobj->form_edit_sup." . ".$impact->supportobj->period_sup ." . ".$impact->supportobj->lng_edit_sup." . ".$impact->supportobj->postition_sup."</p>";
            $y = PDFTCPDF::getY();
            $width = PDFTCPDF::getPageWidth();
            $heigh = PDFTCPDF::getPageHeight();

            $containerHeight = $heigh / 5;
            
            // dump($heigh);
            // dump($width);
            // dd($heigh - PDFTCPDF::getY());
            //$containerImageY = PDFTCPDF::getY();
            //PDFTCPDF::Rect(0, PDFTCPDF::getY(), $width,$heigh, 'F', array(), array(128,255,128));

            PDFTCPDF::SetFillColor(244, 244, 244);
            PDFTCPDF::writeHTMLCell($width, $containerHeight,0, 0, $container, 0, 1, 1, true, '', false);

            //PDFTCPDF::Image('http://localhost:8888/tlredp/storage/app/public/uploads/support/1/logo-2016.jpg', 0, 0, 75, 113, 'JPEG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 1, false, false, false);
            // LOGO
            PDFTCPDF::Image('http://localhost:8888/tlredp/public/image/logotlredp.png', 5, 5,40,'', 'PNG', '', 'T', false, 0, '', false, false, 1, false, false, false);
            //Image( $file, $x = '', $y = '', $w = 0, $h = 0, $type = '', $link = '', $align = '', $resize = false, $dpi = 300, $palign = '', $ismask = false, $imgmask = false, $border = 0, $fitbox = false, $hidden = false, $fitonpage = false, $alt = false, $altimgs = array() )

            // Print text using writeHTMLCell()
            PDFTCPDF::SetTextColor(0, 0, 0);
            // PDFTCPDF::SetFillColor(224, 82, 69);
            // //PDFTCPDF::Cell(0, 0, 'A4 PORTRAIT', 0, 0,'R');
            //dd(PDFTCPDF::getFontsList());
            // Veille redactionnelle title
            PDFTCPDF::SetFont($geometosfontname, '', PDFTCPDF::pixelsToUnits('13'), '', false);
            PDFTCPDF::writeHTMLCell(0,0, 5, 5, $righthtml, 0, 1, 1, true, 'R', true, true, '', false);
            // Client name
            PDFTCPDF::writeHTMLCell(0,0, 50, PDFTCPDF::getY(), $clientName, 0, 1, 1, true, 'R', true, true, '', false);
            //$this->Image($image_file, 90, 5, 40, '', 'PNG', '', 'T', false, 300, 'C', false, false, 0, false, false, false);
            // Support LOGO
            PDFTCPDF::Image('http://localhost:8888/tlredp/storage/app/public/uploads/support/'.$impact->supportobj->id.'/'.$impact->supportobj->logo_sup.'', 0, PDFTCPDF::getY() -2,20,'', 'PNG', '', 'T', false, 0, 'C', false, false, 1, false, false, false);
            // //writeHTMLCell( $w, $h, $x, $y, $html = '', $border = 0, $ln = 0, $fill = false, $reseth = true, $align = '', $autopadding = true )
            // // cell $w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M' )
            // Title support
            PDFTCPDF::SetFont($geometosroundedfontname, '', 13, '', false);
            PDFTCPDF::writeHTMLCell(0,0, 5, PDFTCPDF::getImageRBY() + 3, $impact->supportobj->name_sup, 0, 1, 1, true, 'C', true, true, '', false);
            // Description Support
            PDFTCPDF::SetFont($geometosroundedfontname, '', 7, '', false);
            PDFTCPDF::writeHTMLCell(0,0, 5, PDFTCPDF::getY() + 1,$supportDesc, 0, 1, 1, true, 'C', true, true, '', false);
            PDFTCPDF::Line( 60, PDFTCPDF::getY() + 4, $width - 60 , PDFTCPDF::getY() + 4);

            //PDFTCPDF::SetFont ('helvetica', 'B' ,10, 'default', true );
            // PDFTCPDF::SetFontSize(9);
            // Title article
            PDFTCPDF::writeHTMLCell(0,0, 0, PDFTCPDF::getY() + 7, $titleArticle, 0, 1, 1, true, 'C', true, true, '', false);
            // Description Support / Format
            PDFTCPDF::SetFont($dinarabicfontname, 'B', 9, '', false);
            // PDFTCPDF::SetFont('aealarabiya', 'B', 9);
            PDFTCPDF::writeHTMLCell(0,0, 5, PDFTCPDF::getY(), $descImpact, 0, 1, 1, true, 'C', true, true, '', false);
            // Impact Image
            PDFTCPDF::Image('http://localhost:8888/tlredp/storage/app/public/uploads/impacts/'.$impact->id.'/'.$image->scan.'', 0, $containerHeight ,$width-50, $heigh - $containerHeight - 10, 'PNG', '', '', true, 300, 'C', false, false, 0, 'CM', false, false);
            
            PDFTCPDF::SetFillColor(244, 244, 244);
            PDFTCPDF::writeHTMLCell($width,10, 0, $heigh - 10, '', 0, 0, 1, true, 'C', true, true, '', false);
            }
        }// foreach image 
    } // forache impact
        PDFTCPDF::Output('hello_world.pdf');
        /******************************************************************************** */
    }

    public function printAllImpactPdf(Request $request){
        $data = $request->all();
        //dd($data);
        $impactIds = array();
        if(isset($data['impactid'])){
            $impactIds=explode(",", $data['impactid']);
        }

        $impactArray = array();
        foreach ($impactIds as $key => $impactid) {
            if (isset($impactid) && $impactid != null && !is_null($impactid)){
                $aImpact = Impact::find($impactid);
                $clientImpact = ImpactClient::whereImpactid_imp_cl($aImpact->id)->first();
                $aImpact->authorobj = $aImpact->author;
                $aImpact->supportobj = $aImpact->support;
                $aImpact->rubriqueobj = $aImpact->rubrique;
                $aImpact->formatobj = $aImpact->format;
                $aImpact->clientCampany = ClientCompany::find($clientImpact->clientid_imp_cl);
                $aImpact->images = json_decode($aImpact->scan_imp);
                array_push($aImpactArray,$aImpact);
            }
        }
        //$cronjob = Cronjob::create(array("type"=>"type","data"=>$data['impactid']));
        // dd($cronjob);
        $generatePdfJob = (new GenerateListPdf())->delay(\Carbon\Carbon::now()->addSeconds(10));
        dispatch($generatePdfJob);
        echo 'pdf generated';
        // $pdf = PDF::loadView('admin.pdf.impactspdf',compact('aImpactArray'));
        // //print_r($pdf->stream('test.pdf'));die;
        // $pdf->download("pdfTest.pdf");
        // dd('printAllImpactPdf');
    }

    public function sendEmptyMail(Request $request) {
        $data = $request->all();
        //dd($data);
        $date = \Carbon\Carbon::now();
        $emails = [];

        $client = ClientCompany::findOrFail($data['client_review']);

        if(isset($client['email_cl'])){
            $emails=explode(";", $client['email_cl']);
        }

        if (isset($emails) && !empty($emails)){
            Mail::send('emails.impactemptymail', [], function ($m) use ($emails,$date,$client) {
                
                $m->from('toutelarevuedepresse@gmail.com', 'Toute la revue de presse');
                //$m->attach(storage_path('app/public/uploads/pdfs/13_pdfname.pdf'));
    
                $m->to($emails)->subject('VEILLE REDACTIONNELLE DE '.strtoupper($client->nom_cl).' du '.\Carbon\Carbon::parse($date)->format('d/m/Y').'');
            });
            return redirect()->route('reviews.index')->withMessage(trans('quickadmin::admin.item-controller-successfully_sended'));
        } else {
            return redirect()->route('reviews.index')->withMessage(trans('quickadmin::admin.item-controller-successfully_failed'));
        }
    }

    public function sendImpactsByMail(Request $request){
        $data = $request->all();
        $client = ClientCompany::findOrFail($data['client_review']);
        $emails = [];
        $impactFile = $data['pdf_name'];
        //dd($impactFile);
        $date = \Carbon\Carbon::now();
        $aImpacts = [];
        $aImpactCriteria = [];
        // $array = ImpactClient::all()->where("criteriaid_imp_cl",21)->groupBy('criteriaid_imp_cl')->toArray();  
        // dd($array);
        if(isset($data['impacts_email'])){
            $impactIds=explode(",", $data['impacts_email']);
            foreach ($impactIds as $key => $impactid) {
                if (isset($impactid) && $impactid != null && !is_null($impactid)){
                    // get items of impact from impact_clients
                    //dump($impactid);                
                    $aImpacts[] = ImpactClient::whereImpactid_imp_cl($impactid)->get();
                    //$aImpacts[] = Impact::find($impactid);
                }
            }
        }
        foreach ($aImpacts as $key => $impactCl) {
            if (isset($impactCl) && $impactCl->count() > 0){
                if(!in_array($impactCl[0]->criteriaid_imp_cl, $aImpactCriteria)) {
                    $aImpactCriteria[$impactCl[0]->criteriaid_imp_cl][] = ImpactClient::join('impacts','impact_clients.impactid_imp_cl','=','impacts.id')
                                ->join('criterias','impact_clients.criteriaid_imp_cl','=','criterias.id')
                                ->leftJoin('supports','impacts.supportid_imp','=','supports.id')
                                ->where("impact_clients.impactid_imp_cl",$impactCl[0]->impactid_imp_cl)
                                ->where("impact_clients.criteriaid_imp_cl",$impactCl[0]->criteriaid_imp_cl)
                                ->first();
                    
                    $this->array_sort_by_column($aImpactCriteria[$impactCl[0]->criteriaid_imp_cl], "date_imp");                                
                }
            }
        }
        //dump($aCriteria);
        //dd($aImpactCriteria);
        
        if(isset($client['email_cl'])){
            $emails=explode(";", $client['email_cl']);
        }
        // sort array by date
        //$this->array_sort_by_column($aImpacts, "date_imp");
        //dd($aImpactCriteria);
        if (isset($emails) && !empty($emails)){
            Mail::send('emails.impactsmail', ['aImpactCriteria'=>$aImpactCriteria,'impactFile'=>$impactFile], function ($m) use ($emails,$date,$client) {
                
                $m->from('veille@lafabriquenumerique.ma', 'Kaoutar Azad');
                //$m->attach(storage_path('app/public/uploads/pdfs/13_pdfname.pdf'));
    
                $m->to($emails)->subject('VEILLE REDACTIONNELLE DE '.strtoupper($client->nom_cl).' du '.\Carbon\Carbon::parse($date)->format('d/m/Y').'');
            });
            return redirect()->route('reviews.index')->withMessage(trans('quickadmin::admin.item-controller-successfully_sended'));
        } else {
            return redirect()->route('reviews.index')->withMessage(trans('quickadmin::admin.item-controller-successfully_failed'));
        }
    }

    // show image from source
    function showImage($impactid) {
        $impact = Impact::find($impactid);
        $storagePath = storage_path('app/public/uploads/'. $impact->source_imp.'');
        //dd($storagePath);
        return Image::make($storagePath)->response();
    }

    function array_sort_by_column(&$array, $column, $direction = SORT_DESC) {
        $reference_array = array();
    
        foreach($array as $key => $row) {
            $reference_array[$key] = $row[$column];
        }
    
        array_multisort($reference_array, $direction, $array);
    }

    function getNameCfriteria($criteriaId) {
        dd($criteriaId);
    }
}
