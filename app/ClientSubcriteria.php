<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientSubcriteria extends Model
{
    protected $table = "client_subcriterias";
    
    public $timestamps = false;

    protected $fillable = array("subcriteria_id","client_id");

    public function subcriteria(){
        return $this->belongsTo('App\Subcriteria','subcriteria_id');
    }
}
