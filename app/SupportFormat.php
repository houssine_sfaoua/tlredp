<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupportFormat extends Model
{
    protected $table = "support_formats";
    
    public $timestamps = false;

    protected $fillable = array("support_id","format_id");

    public function support(){
        return $this->belongsTo('App\Support','support_id');
    }

    public function format(){
        return $this->belongsTo('App\Format','format_id');
    }
}
