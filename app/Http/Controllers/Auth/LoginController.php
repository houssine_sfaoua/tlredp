<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //dd(config('quickadmin.route'));

        $this->redirectTo = config('quickadmin.route');
        $this->middleware('guest', ['except' => 'logout']);
    }

    protected function authenticated(Request $request, $user)
    {
        
        if ( $user) {// do your margic here
            return redirect()->action('\Laraveldaily\Quickadmin\Controllers\QuickadminController@index');
        }

        //return redirect('/login');
    }

    /**
    * logout user and redirect to login form
    *
    */
    public function logout(Request $request)
    {
        $this->performLogout($request);
        return redirect()->route('login');
    }
}