<!DOCTYPE html>
<html>
<head>
	<title>Toute la revue de presse </title>
    @include('admin.pdf.style')
    <style>
        body {
            font-family: 'arabicfont', sans-serif;
        }
        #img-support{
            width: 9%;
        }
        .fiche-container p {
            line-height:0;
            padding:0;
            margin:0;
            font-size:12px;
            font-weight: bold;
        }
        p.page-paraph {
            /* text-align:right; */
            margin:10% 0 0 80% !important;
            padding:0;
        }
    </style>
</head>
<body>
<div class="container">
    @foreach($aImpactArray as $aImpact)
    <div class"page" style="height:100%;">
    <p style="color:blue;margin:0;padding:0;">Veille rédactionnelle > <span style="color:gray;">{{ $aImpact->clientCampany->nom_cl }}</span></p>
    @if(isset($aImpact->images))
        @foreach ($aImpact->images as $key=>$image)
        <div style="height:100%">
            <div class="header-field">
                    <div class="support-section" style="margin-top:12px;">
                    @if($aImpact->supportobj->logo_sup!= null)
                            <img src="{{ env('APP_URL').'/storage/app/public/uploads/support/'.$aImpact->supportobj->id.'/'.$aImpact->supportobj->logo_sup }}" alt="img support" id="img-support" style="">
                        @else
                            <img src="{{ env('APP_URL').'/storage/app/public/newspapericon.jpg' }}" alt="img support" id="img-support" style="">
                        @endif
                    <div id="support-info" >
                        <!-- <h2 class="support-name"></h2> -->
                        <p style="">{{$aImpact->supportobj->name_sup}} <br/> {{ $aImpact->supportobj->form_edit_sup }} . {{$aImpact->supportobj->period_sup }} . {{$aImpact->supportobj->lng_edit_sup }} . {{$aImpact->supportobj->postition_sup }}</p>
                    </div>
                </div>
                <div style="clear:both;"></div>
                <br/>
                <div class="fiche-container">
                    <p>Article <span class="strong">{{$aImpact->title_imp}}</span> paru le <span class="strong">{{ $aImpact->date_imp }}</span> dans la rubrique <span class="strong">{{ $aImpact->rubriqueobj->nom_rub }} </span>
                    , signé par <span class="strong">{{ $aImpact->authorobj->name_aut }}</span></p>
                    <p>Article de <span class="strong">{{ $aImpact->formatobj->nom_frm }}</span>, édité en <span class="strong">{{ $aImpact->formatobj->print_frm }} </span>, d’une valeur publicitaire équivalente à {{ $aImpact->formatobj->value_ad_frm }} </p>
                </div>
            </div>
        <br/>
        <div class="image-container">
            <img src="{{ env('APP_URL').'/storage/app/public/uploads/impacts/'.$aImpact->id.'/'.$image->scan }}" alt="" class="img-scan" style="">
        </div> 
    </div>
    <div style="clear:both;"></div>
    @endforeach
    @endif
    </div>
@endforeach
    
	
	
</div>
</body>
</html>