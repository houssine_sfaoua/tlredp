<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Support;
use App\SupportFormat;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Constraint\IsEmpty;

class SupportController extends Controller
{
    
    public function index(){

        $aSupport = Support::all();

        return view("admin.support.index",compact('aSupport'));
    }

    public function edit($id){

        $support  = Support::findOrFail($id);
        //dd($support);

         $aSupportFormat =SupportFormat::where('support_id',$support->id)->pluck('format_id')->toArray();

         $support->formatids = $aSupportFormat;
        //dd($support);

        return view("admin.support.edit",compact('support'));
    }


    public function create(){
        return view("admin.support.create");
    }

    public function store(Request $request){

        $data = $request->all();
        //dd($data);

        if(isset($data['edit'])){
            $logoRequired = "";
        }else{
            $logoRequired = "required";
        }

        $validatedData = $request->validate([
            'name_sup' => 'required',
            'logosup' => $logoRequired,
        ]);




        if(isset($data['edit'])){

            $aSupport = array(
                "name_sup"=>$data['name_sup'],
                "origin_sup"=>$data['origin_sup'],
                "cat_sup"=>$data['cat_sup'],
                "edit_orie_sup"=>$data['edit_orie_sup'],
                "period_sup"=>$data['period_sup'],
                "form_edit_sup"=>$data['form_edit_sup'],
                "lng_edit_sup"=>$data['lng_edit_sup'],
                "postition_sup"=>$data['postition_sup'],
            );
            
            Support::where('id', $data['edit'])->update($aSupport);
            //dd(Support::where($data['edit'])->get());
            $support = Support::where('id',$data['edit'])->first();

        }else{
            $support=Support::create($request->all());
        }
        //dd($data);
         if(isset($data['format_id_sup'])){
            $aFormat =explode(",",$data['formatitems']);
            //dd($aFormat);
            if(isset($data['edit'])){
                $res = SupportFormat::where('support_id',$data['edit'])->delete();
                //dd($res);
            }

            foreach ($aFormat as $key => $formatid) {
                $temp = array("support_id"=>$support->id,"format_id"=>$formatid);
                //dd($temp);
                SupportFormat::create($temp);
            }
        } else {
            if(isset($data['edit'])){
                $res = SupportFormat::where('support_id',$data['edit'])->delete();
                //dd($res);
            }
        }
        
        if(isset($request->logosup)){
            $image = $request->logo;
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);

            if(isset($support)){

                 $result = Storage::disk('public')->put('support/'.$support->id.'/'.$request->logosup,base64_decode($image));
                 //dd($result);
                 if($result){
                    $support->logo_sup = $request->logosup;
                    $support->save();
                 }
            }
        }
        
        return redirect()->route('support.index')->withMessage(trans('quickadmin::admin.item-controller-successfully_created'));
    }
}
