<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Impact extends Model
{
     protected $table = "impacts";
    
    public $timestamps = false;

    protected $fillable = array("title_imp","language_imp","resume_imp","source_imp","scan_imp","date_imp","date_integ_imp","selection_imp","authorid_imp","userid_imp","period_tag_imp","pace_imp","supportid_imp","num_edit_imp","num_page_imp","link_imp","rubid_imp","formatid_imp","appear_imp","appearid_imp","nature_imp","type_imp","program_name_imp");

     public function author(){
        return $this->belongsTo('App\Author','authorid_imp');
    }

    public function support(){
        return $this->belongsTo('App\Support','supportid_imp');
    }

    public function rubrique(){
        return $this->belongsTo('App\Rubrique','rubid_imp');
    }

    public function format(){
        return $this->belongsTo('App\Format','formatid_imp');
    }

    
}
