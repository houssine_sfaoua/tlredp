<?php

return [

    // dashboard
    'dashboard-title'                       => 'Welcome to your project dashboard',

    // partials-header
    'partials-header-title'                 => 'QuickAdmin en',

    // partials-sidebar
    'partials-sidebar-menu'                 => 'Menu',
    'partials-sidebar-impacts'              => 'Impacts',
    'partials-sidebar-criteria'             => 'Criteria',
    'partials-sidebar-subcriteria'          => 'Sub-criteria',
    'partials-sidebar-support'              => 'Support',
    'partials-sidebar-format'               => 'format',
    'partials-sidebar-client'               => 'Client',
    'partials-sidebar-users'                => 'Users',
    'partials-sidebar-roles'                => 'Roles',
    'partials-sidebar-user-actions'         => 'User actions',
    'partials-sidebar-logout'               => 'Logout',
    'partials-sidebar-reviews'              => 'Press review',


    //presse review
    'reviews-index-date_begin'=>'Date begin',
    'reviews-index-date_end'=>'Date end',


    // partials-topbar
    'partials-topbar-title'                 => 'Toute la revue de presse',

    // users-create
    'users-create-create_user'              => 'Create user',
    'users-create-name'                     => 'Name',
    'users-create-name_placeholder'         => 'Name',
    'users-create-email'                    => 'Email',
    'users-create-email_placeholder'        => 'Email',
    'users-create-password'                 => 'Password',
    'users-create-password_placeholder'     => 'Password',
    'users-create-role'                     => 'Role',
    'users-create-btncreate'                => 'Create',

    // users-edit
    'users-edit-edit_user'                  => 'Edit user',
    'users-edit-name'                       => 'Name',
    'users-edit-name_placeholder'           => 'Name',
    'users-edit-email'                      => 'Email',
    'users-edit-email_placeholder'          => 'Email',
    'users-edit-password'                   => 'Password',
    'users-edit-password_placeholder'       => 'Password',
    'users-edit-role'                       => 'Role',
    'users-edit-btnupdate'                  => 'Update',

    // users-index
    'users-index-add_new'                   => 'Add new',
    'users-index-users_list'                => 'Users list',
    'users-index-name'                      => 'Name',
    'users-index-pseudo'                    => 'Pseudo name',
    'users-index-edit'                      => 'Edit',
    'users-index-delete'                    => 'Delete',
    'users-index-are_you_sure'              => 'Are you sure?',
    'users-index-no_entries_found'          => 'No entries found',


    // users-controller
    'users-controller-successfully_created' => 'User was successfully created!',
    'users-controller-successfully_updated' => 'User was successfully updated!',
    'users-controller-successfully_deleted' => 'User was successfully deleted!',

    

    // roles-index
    'roles-index-add_new'                   => 'Add new',
    'roles-index-roles_list'                => 'Roles list',
    'roles-index-title'                     => 'Title',
    'roles-index-edit'                      => 'Edit',
    'roles-index-delete'                    => 'Delete',
    'roles-index-are_you_sure'              => 'Are you sure?',
    'roles-index-no_entries_found'          => 'No entries found',

    // roles-create
    'roles-create-create_role'              => 'Create role',
    'roles-create-title'                    => 'Title',
    'roles-create-title_placeholder'        => 'Title',
    'roles-create-btncreate'                => 'Create',

    // roles-edit
    'roles-edit-edit_role'                  => 'Edit role',
    'roles-edit-title'                      => 'Title',
    'roles-edit-title_placeholder'          => 'Title',
    'roles-edit-btnupdate'                  => 'Update',

    // roles-controller
    'roles-controller-successfully_created' => 'Role was successfully created!',
    'roles-controller-successfully_updated' => 'Role was successfully updated!',
    'roles-controller-successfully_deleted' => 'Role was successfully deleted!',


    //Impacts
    'impact-create-title' => 'Titre de la retombée',
    'impact-create-language' => 'Langue',
    'impact-create-resume'      => 'Résumé',
    'impact-create-period-tag' => 'Période',
    'impact-create-selection'=> 'Sélection',
    'impact-create-author'=> 'Auteur',
    'impact-create-date'=> 'Date de parution',
    'impact-create-date-integration'=> 'Date d integration',
    'impact-create-pace'=> 'Ton de la retombée',
    'impact-create-support'=> 'Nom du support',
    'impact-create-cat_sup'=> 'Catégorie du support',
    'impact-create-orient_sup'=> 'Orientation éditoriale du support', 
    'impact-create-perd_sup'=> 'Périodicité du support',
    'impact-create-type_sup'=> 'Type de l’édition',
    'impact-create-lng_sup'=> 'Langue d’édition du support',
    'impact-create-posit_sup'=> 'Positionnement du support',
    'impact-create-num_edit'=> 'Numéro de l’édition', 
    'impact-create-num_page'=> 'Numéro de la page',
    'impact-create-nom_rub'=> 'Nom de la rubrique',
    'impact-create-link_press'=> 'Lien', 
    'impact-create-format'=> 'Format',
    'impact-create-nature_imp' => 'Nature',
    'impact-create-print'=> 'Impression',
    'impact-create-pub_value'=> 'Valeur publicitaire', 
    'impact-create-clientcmp'=> 'Client', 
    'impact-create-appear'=>'Parution',
    'impact-create-scan' =>'Scan de la retombée',
    'impact-index-impact_list'=> 'Impacts list',
    'impact-index-name'=>'Nom du support',
    'impact-index-date-parution'=>'Date de parution',
    'impact-index-date-integration'=>'Date d\'intégration',
    'impact-index-user-add-impact'=>'Responsable',
    'impact-index-title'=>'Titre',
    'impact-create-record'=>'Enregistrement de la retombée',
    'impact-create-typeimpact'=>'Type retombé',
    'impact-create-programnameimpact'=>'Nom du programme',
    'impact-create-source' => 'Image source',

    //criteria
    'criteria-create-labelcr'=>'Titre du critère',
    'criterias-create-btncreate'=>'Enregistrer',
    'impact-index-criteria_list'=> 'Criteria list',

    //subcriteria
    'subcriteria-create-labelsubcr'=>'Titre du sous critère',
    'subcriteria-labelsubcr'=>'sous critère',
    'subcriteria-create-critid'=>'Titre du critère',
    'item-controller-successfully_created'=>'Item was successfully created!',
    'item-controller-successfully_sended'=>'Item was successfully sent!',
    'item-controller-successfully_failed'=>'Failure!',
    'item-controller-successfully_deleted' => 'Item was successfully deleted!',
    'subcriteria-index-subcriteria_list'=> 'SubCriteria list',

    //support
    'support-create-name'=>'Nom du support',
    'support-create-logo' => 'Logo',
    'support-create-origin'=>'Origine du support',
    'support-create-catsup'=>'Catégorie du support',
    'support-create-subcatsupp'=>'Sous-catégorie du support',
    'support-create-orientation'=>'Orientation éditoriale du support',
    'support-create-period'=>'Périodicité du support',
    'support-create-typeedit'=>'Form de l’édition',
    'support-create-lngedit'=>'Langue d’édition du support',
    'support-create-position'=>'Positionnement du support',
    'support-index-support_list'=> 'Support list',

    //client
    'client-create-namecl'=>'Nom',
    'client-create-emailcl'=>'Email',
    'client-create-telcl'=>'Tel.',
    'client-create-newcriteriacl'=>'Critère',
    'client-create-logocl'=>'Logo',

    //Formats
    'format-index-format_list'=>'Format list',
    'format-create-nom_frm'=>'Nom',
    'format-create-print_frm'=>'Print',
    'format-create-value_ad_frm'=>'Value ad',
    'format-create-pseudo_frm' => 'Pseudo format'
];

