<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCompany extends Model
{
    protected $table = "client_companies";
    
    public $timestamps = false;

    protected $fillable = array("nom_cl","email_cl","tel_cl");

    public function criteria()
    {
        return $this->belongsToMany('App\Criterias','criteria_id');
    }
}
