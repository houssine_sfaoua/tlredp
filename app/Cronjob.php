<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Cronjob extends Model
{
    // use SoftDeletes;
    protected $table = "cronjobs";
    
    public $timestamps = false;

    protected $fillable = array("type","data");
}
