@extends('admin.layouts.master')

@section('content')

{!! Form::open(['route' => 'criteria.store', 'class' => 'form-horizontal'])!!}
<div class="form-group">
	{!! Form::label('label_cri', trans('quickadmin::admin.criteria-create-labelcr'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('label_cri', "", ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.criteria-create-labelcr')]) !!}
	</div>
</div>

<div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.criterias-create-btncreate'), ['class' => 'btn btn-primary']) !!}
        </div>
</div>

{!! Form::close() !!}
@endsection