@extends('admin.layouts.master') @section('content') {{csrf_field()}}


<style>
	img {
		/*width: 100%;*/
	}

	#crop-image {
		height: 31em;
		max-width: 100%;
		display: block;
		margin: 0 auto;
	}
	#caman-img{
		height:auto !important;
		width : 50% !important;
	}

	#Filters{
		display: inline-block;
		/*width: 30%;*/
		float: left;
	}

#Filters .Filter {
    float: left;
    width: 300px;
}

#Filters .Filter .FilterName {
    float: left;
}


#Filters .Filter .FilterSetting {
    float: right;
}

#Filters .Filter>div {
    padding: 5px 10px;
}

#Filters .Filter .FilterSetting input[type="range"] {
    -webkit-appearance: none!important;
    width: 150px;
    background-color: #434341;
    height: 1px;
    vertical-align: 1px;
	display: inline-block;
}

#Filters .Filter .FilterSetting .FilterValue {
    width: 30px;
    display: inline-block;
}

#reset-logo-image,#delete-client-wrapper{
	display: inline-block;
    width: 49.6%;
    height: 33px;
    padding-top: 6px;
    margin-top: 13px;
    margin-bottom: 16px;
    margin-left: 17.6%;
}

#submit-create-client{
	margin-top: 19px;
}
ul.chosen-results li, a.chosen-single{
	text-transform: uppercase !important;
}
div.record-scan-container{
	display: none;
}
</style>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


{{-- <img src="" id="test-crop"> --}} 
{!! Form::open(['route' => 'impact.store', 'class' => 'form-horizontal','files' => true, 'enctype'=>'multipart/form-data' ])!!}

<div class="form-group">
	{!! Form::label('name', trans('quickadmin::admin.impact-create-title'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('title', "", ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.impact-create-title')]) !!}
	</div>
</div>
<div class="form-group">
		{!! Form::label('nature_imp', trans('quickadmin::admin.impact-create-nature_imp'), ['class'=>'col-sm-2 control-label']) !!}
		<div class="col-sm-10">
			{!! Form::select('nature_imp',['Article dédié'=>'Article dédié','Citation'=>'Citation'], "", ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.impact-create-nature_imp')])
		!!}
		</div>
	</div>
<div class="form-group">
	{!! Form::label('language_bl', trans('quickadmin::admin.impact-create-language'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('language', ['ar'=>'Arabe','fr'=>'Français','en'=>'Anglais'],'',['placeholder' => trans('quickadmin::admin.impact-create-language'),'class'=>'form-control']) }}
	</div>
</div>
@if(Auth::user()->role_id != config('quickadmin.chargeveille'))
<div class="form-group">
	{!! Form::label('name', trans('quickadmin::admin.impact-create-resume'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::textarea('resume', "", ['size' => '30x5','class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.impact-create-resume')])
		!!}
	</div>
</div>
@endif
<div class="form-group col-md-12">
	{!! Form::label('period', trans('quickadmin::admin.impact-create-period-tag'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
        {{ Form::checkbox('period_tag',1,true, array('id'=>'first_period','class'=>'check_class')) }}
        {!! Form::label('period', "Matin", ['class'=>'']) !!}
        {{ Form::checkbox('period_tag',2,false, array('id'=>'second_period','class'=>'check_class')) }}
        {!! Form::label('period', "Après-midi", ['class'=>'']) !!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('typeimpact', trans('quickadmin::admin.impact-create-typeimpact'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('type_imp', ['audiovisuel'=>'Audiovisuel','other'=>'Autre'],'other',['class'=>'form-control','id'=>'type-impact']) }}
	</div>
</div>
<div class="form-group">
	{!! Form::label('source', trans('quickadmin::admin.impact-create-source'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::file('source', ['class' => 'form-control']) !!}
	</div>
</div>
<div class="form-group record-scan-container">
	{!! Form::label('recordscan', trans('quickadmin::admin.impact-create-record'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::file('recordscan', ['class' => 'form-control','id'=>'record-scan-impact']) !!}
	</div>
</div>
<div class="form-group record-scan-container">
	{!! Form::label('programname', trans('quickadmin::admin.impact-create-programnameimpact'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
	{!! Form::text('program_name_imp', "", ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.impact-create-programnameimpact')]) !!}
	</div>
</div>
<div class="">
	{!! Form::label('scan', trans('quickadmin::admin.impact-create-scan'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::file('scan', ['class' => 'form-control','style'=>'display:none','id'=>'scan-file']) !!}
	</div>
</div>
<a href="#" id="openFileDialog" class="btn btn-xs btn-danger" style="width: 51%;height: 33px;padding-top: 6px;">Open</a>

<div class="form-group logo-wrapper">
	{{-- <div class="col-sm-10 col-sm-offset-2 logo-container logo-container-0">
		<img src="#" style="display:none;width: 17%;" id="img-logo-client-0">
		
		<input type="text" id="logo-data-0" name="logo[]" style="display:none;" />
	</div> --}}

	<button class="btn btn-xs btn-danger" id="reset-logo-image" style="">Reset</button>
</div>


{{-- <div class="form-group">
	{!! Form::label('selection', trans('quickadmin::admin.impact-create-selection'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::radio('selection','1',true) }} {!! Form::label('selection','Première page') !!} {{ Form::radio('selection', '2')
		}} {!! Form::label('selection','Suite') !!}
	</div>
</div> --}}

<div class="form-group">
	{!! Form::label('author', trans('quickadmin::admin.impact-create-author'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('author', [],'',['placeholder' => 'Choisir un auteur','class'=>'form-control','id'=>'author-select']) }}
	</div>
</div>

<div id="wrapper-author">
	<div class="form-group" id="newauthor">
		{!! Form::label('new_author_lbl', trans('quickadmin::admin.impact-create-author'), ['class'=>'col-sm-2 control-label']) !!}
		<div class="col-sm-10">
			{!! Form::text('new_author_lbl[]', "", ['class'=>'form-control','placeholder'=> trans('quickadmin::admin.impact-create-author')]) !!}
		</div>
	</div>
	
	
</div>
{{-- <button id="sub-add-author" class="sub_add_author_button btn btn-primary col-md-6 col-md-offset-2">add</button> --}}
<div style="clear:both;"></div>

<div class="form-group">
	{!! Form::label('date', trans('quickadmin::admin.impact-create-date'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('date', "", ['class'=>'form-control datepicker','id'=>'date-impact', 'placeholder'=> trans('quickadmin::admin.impact-create-date')])
		!!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('date_integ', trans('quickadmin::admin.impact-create-date-integration'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('date_integ', "", ['class'=>'form-control datetimepicker','id'=>'date-integ-impact', 'placeholder'=> trans('quickadmin::admin.impact-create-date-integration')])
		!!}
	</div>
</div>
<div class="form-group">
	{!! Form::label('pace', trans('quickadmin::admin.impact-create-pace'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('pace', ['positif'=>'Positif', 'negatif'=>'Négatif', 'neutre'=>'Neutre'],'',['class'=>'form-control']) }}
	</div>
</div>

<!--trie par ordre alphabetique-->
<div class="form-group">
	{!! Form::label('support', trans('quickadmin::admin.impact-create-support'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('support', \App\Support::pluck('name_sup', 'id'),'',['placeholder' => 'Choisir support','id'=>'support-impt','class'=>'form-control']) }}
	</div>
</div>

{{--  --}}
{{-- <div class="form-group">
	{!! Form::label('cat_sup', trans('quickadmin::admin.impact-create-cat_sup'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('cat_sup', ['Agence de presse'=>'Agence de presse','Portail du Maroc'=>'Portail du Maroc','Presse electronique'=>'Presse electronique','Presse Papier'=>'Presse Papier'],'',['class'=>'form-control']) }}
	</div>
</div>

<div class="form-group">
	{!! Form::label('orient_sup', trans('quickadmin::admin.impact-create-orient_sup'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('orient_sup', ['Presse partisane'=>'Presse partisane', 'Presse independante'=>'Presse independante','Presse officielle'=>'Presse officielle'],'',['class'=>'form-control']) }}
	</div>
</div>

<div class="form-group">
	{!! Form::label('perd_sup', trans('quickadmin::admin.impact-create-perd_sup'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('perd_sup', ['Bi-mensuel'=>'Bi-mensuel','Quotidien'=>'Quotidien', 'Hebdomadaire'=>'Hebdomadaire','Mensuel'=>'Mensuel','Bimestriel'=>'Bimestriel','Trimestriel'=>'Trimestriel','En continu'=>'En continu'],'',['class'=>'form-control']) }}
	</div>
</div>
<div class="form-group">
	{!! Form::label('form_edit_sup', trans('quickadmin::admin.support-create-typeedit'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('form_edit_sup', ['Journal'=>'Journal', 'Magazine'=>'Magazine','En ligne'=>'En ligne'],'',['class'=>'form-control']) }}
	</div>
</div>

<div class="form-group">
	{!! Form::label('lng_sup', trans('quickadmin::admin.impact-create-lng_sup'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('lng_sup', ['Arabophone'=>'Arabophone', 'Francophone'=>'Francophone','Espagnol'=>'Espagnol','Anglophone'=>'Anglophone','Bilingue'=>'Bilingue','Trilingue'=>'Trilingue','Multilingue'=>'Multilingue'],'',['class'=>'form-control']) }}
	</div>
</div>
<div class="form-group">
	{!! Form::label('posit_sup', trans('quickadmin::admin.impact-create-posit_sup'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('posit_sup', ['Presse generaliste'=>'Presse generaliste','Presse MRE'=>'Presse MRE',
		'Presse officielle'=>'Presse officielle','Presse politique'=>'Presse politique','Presse politique (Sahara )'=>'Presse politique (Sahara )',
		'Presse RH'=>'Presse RH','Presse RSE'=>'Presse RSE','Presse satirique'=>'Presse satirique','Presse sportive'=>'Presse sportive',
		'Presse TIC'=>'Presse TIC','Presse Toursime'=>'Presse Toursime',
		'Presse medecine'=>'Presse medecine',
		'Presse masculine'=>'Presse masculine',
		'Presse maritime'=>'Presse maritime','Presse Management'=>'Presse Management',
		'Presse juridique'=>'Presse juridique','Presse Industrie'=>'Presse Industrie',
		'Presse Histoire'=>'Presse Histoire','Agregateur generaliste'=>'Agregateur generaliste',
		'presse financière'=>'presse financière','Presse feminine'=>'Presse feminine',
		'Presse economique'=>'Presse economique','Presse Ecologie & Environnement'=>'Presse Ecologie & Environnement','Presse eco & TIC'=>'Presse eco & TIC',
		'Presse Conso'=>'Presse Conso','Presse Cityguide'=>'Presse Cityguide',
		'Presse Art'=>'Presse Art','Presse auto/moto'=>'Presse auto/moto',
		'Presse BTP'=>'Presse BTP','Presse Art de vivre'=>'Presse Art de vivre',
		'Presse Agricole'=>'Presse Agricole','Presse Agroalimentaire'=>'Presse Agroalimentaire',
		'Généraliste'=>'Généraliste', 'économique'=>'économique','financier'=>'financier',
		'féminin'=>'féminin','satirique'=>'satirique','TIC'=>'TIC','Santé'=>'Santé','Industrie'=>'Industrie'],'',['class'=>'form-control']) }}
	</div>
</div> --}}

<div class="form-group num-press">
	{!! Form::label('num_edit', trans('quickadmin::admin.impact-create-num_edit'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('num_edit', "", ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.impact-create-num_edit')])
		!!}
	</div>
</div>

<div class="form-group num-press">
	{!! Form::label('num_page', trans('quickadmin::admin.impact-create-num_page'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('num_page', "", ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.impact-create-num_page')])
		!!}
	</div>
</div>

<div class="form-group link-press">
		{!! Form::label('link_page', trans('quickadmin::admin.impact-create-link_press'), ['class'=>'col-sm-2 control-label']) !!}
		<div class="col-sm-10">
			{!! Form::text('link_page', "", ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.impact-create-link_press')])
			!!}
		</div>
	</div>
<div class="form-group">
	{!! Form::label('rubid', trans('quickadmin::admin.impact-create-nom_rub'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::select('rubid',[], "", ['class'=>'form-control','id'=>'rubrique-select', 'placeholder'=> trans('quickadmin::admin.impact-create-nom_rub')])
		!!}
	</div>
</div>

<div id="wrapper-rubrique">
	<div class="form-group" id="newrub">
		{!! Form::label('new_rub_lbl', trans('quickadmin::admin.impact-create-nom_rub'), ['class'=>'col-sm-2 control-label']) !!}
		<div class="col-sm-10">
			{!! Form::text('new_rub_lbl[]', "", ['class'=>'form-control','placeholder'=> trans('quickadmin::admin.impact-create-nom_rub')]) !!}
		</div>
	</div>

	{{-- <button class="sub_add_rub_button">add</button> --}}
</div>


<div class="form-group">
	{!! Form::label('foramt_imp', trans('quickadmin::admin.impact-create-format'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::select('format_imp',[], "", ['class'=>'form-control','id'=>'format-select', 'placeholder'=> trans('quickadmin::admin.impact-create-format')])
		!!}
	</div>
</div>
{{-- 
<div id="wrapper-format">
	<div class="form-group" id="newformat">
		{!! Form::label('new_format_lbl', trans('quickadmin::admin.impact-create-format'), ['class'=>'col-sm-2 control-label']) !!}
		<div class="col-sm-10">
			{!! Form::text('new_format_lbl[]', "", ['class'=>'form-control','placeholder'=> trans('quickadmin::admin.impact-create-format')]) !!}
		</div>
	</div>
</div>

<div class="form-group">
	{!! Form::label('print', trans('quickadmin::admin.impact-create-print'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::radio('print','couleur',true,['class'=>'print-radio']) }} {!! Form::label('print','Couleur') !!} {{ Form::radio('print','noirblanc',false,['class'=>'print-radio']) }} {!! Form::label('print','N&amp;B')
		!!}
	</div>
</div> 
<div class="form-group">
	{!! Form::label('pub_value', trans('quickadmin::admin.impact-create-pub_value'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('pub_value', "", ['id'=>'pub_value','class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.impact-create-pub_value')])
		!!}
	</div>
</div>--}}

<div class="form-group">
	{!! Form::label('appear', trans('quickadmin::admin.impact-create-appear'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-2">
		{{ Form::radio('appear','1',false,['class'=>'appear-radio']) }} {!! Form::label('appear','Oui') !!} {{ Form::radio('appear','2',true,['class'=>'appear-radio']) }} {!! Form::label('appear','Non')
		!!}
	</div>

	{{-- List appear --}}
	<div class="col-sm-10 appear-select-imp">
		{!! Form::select('appear_select_imp',\App\Appear::pluck('label_appe', 'id'), "", ['class'=>'form-control','id'=>'appear-select', 'placeholder'=> trans('quickadmin::admin.impact-create-appear')])
		!!}
	</div>

	<div class="appear-wrapper col-sm-8">
		{!! Form::text('appear-input', "", ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.impact-create-appear')])!!}
	</div>
</div>

<div class="form-group client-imp-wrapper" >
	<div class="client-add-wrapper-0">
		{!! Form::label('client_imp', trans('quickadmin::admin.impact-create-clientcmp'), ['class'=>'col-sm-2 control-label']) !!}
		<div class="col-sm-4">
			{{ Form::select('client_imp[]',\App\ClientCompany::pluck('nom_cl','id'),'',['placeholder' => 'Choisir un client','class'=>'form-control','id'=>'client-select-0']) }}
		</div>
		<div class="col-sm-3">
			{{ Form::select('criteria_imp[]',[],'',['class'=>'form-control','id'=>'criteria-select-0']) }}
		</div>
		<div class="col-sm-3">
			{{ Form::select('subcriteria_imp[]',[],'',['class'=>'form-control','id'=>'subcriteria-select-0']) }}
		</div>
	</div>
	
</div>
{{-- <div class="row"><div class="col-md-7 col-md-offset-1">test</div></div> --}}

<button id="add-new-client" class="btn btn-primary col-md-6 col-md-offset-2">Add new client</button>

<div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.criterias-create-btncreate'), ['class' => 'btn btn-primary','id'=>'submit-create-client']) !!}
        </div>
</div>

{!! Form::close() !!}

<!-- Modal -->
<div id="fileModal" class="modal fade" role="dialog">
	<div class="modal-dialog" style="width: 100%;">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				{{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
				<h4 class="modal-title">Modal Header</h4>
			</div>
			<div class="modal-body">
				<div id="copper-container">
					<img id="crop-image" src="" style="">
					{{-- <canvas id="canvas" style=""></canvas> --}}
				</div>
				<div id="caman-container">
					<div id="Filters">
						<div class="Filter">
							<div class="FilterName">
								<p>contrast</p>
							</div>
							<div class="FilterSetting">
								<input type="range" min="-100" max="100" step="1" value="0" id="contrast-range" data-filter="contrast">
								<span class="FilterValue" id="filter-contrast-val">0</span>
							</div>
						</div>
						<div class="Filter">
							<div class="FilterName">
								<p>brightness</p>
							</div>
							<div class="FilterSetting">
								<input type="range" min="-100" max="100" step="1" value="0" id="brightness-range" data-filter="brightness">
								<span class="FilterValue" id="filter-brightness-val">0</span>
							</div>
							</div>
					</div>
				</div>
			</div>

				{{-- <div>
						<div class="img-container">
							<img src="" id="crop-image"/>
						</div>
				</div> --}}
	
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="crop-next">Suivant</button>
				<button type="button" class="btn btn-default" id="crop-save">Enregistrer</button>
				<button type="button" class="btn btn-default"  id="close-modal">Annuler</button>
			</div>
		</div>

	</div>
</div>

{{-- <script type="text/javascript">
	

</script>
 --}}
 @include('admin.partials.imp_create_js') 
@endsection