<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Impact;
use App\ImpactClient;
use App\ClientCompany;
use PDF;
use PDFTCPDF;
use TCPDF_FONTS;
use Illuminate\Support\Facades\Crypt;

class ExtranetController extends Controller
{
    public function __construct()
    {
        // Alternativly
        $this->middleware('auth', ['except' => ['index','printPdf','resumeExtranet','printResumePdf','downloadPdfFile','printtcpdf']]);
    }
    public function index($id){
        $aImpact = Impact::find(Crypt::decrypt($id));
        $aImpact->authorobj = $aImpact->author;
        $aImpact->supportobj = $aImpact->support;
        $aImpact->rubriqueobj = $aImpact->rubrique;
        $aImpact->formatobj = $aImpact->format;

        $aImpact->images = json_decode($aImpact->scan_imp);

        return view('admin.extranet.index',compact('aImpact'));
    }
    public function resumeExtranet($id) {
        $aImpact = Impact::find(Crypt::decrypt($id));
        $aImpact->authorobj = $aImpact->author;
        $aImpact->supportobj = $aImpact->support;
        $aImpact->rubriqueobj = $aImpact->rubrique;
        $aImpact->formatobj = $aImpact->format;

        $aImpact->images = json_decode($aImpact->scan_imp);

        return view('admin.extranet.resumeextra',compact('aImpact'));
    }
    public function printResumePdf(Request $request){
        $data = $request->all();
        $aImpact = Impact::find($data["impactid"]);
        $clientImpact = ImpactClient::whereImpactid_imp_cl($aImpact->id)->first();

        $aImpact->authorobj = $aImpact->author;
        $aImpact->supportobj = $aImpact->support;
        $aImpact->rubriqueobj = $aImpact->rubrique;
        $aImpact->formatobj = $aImpact->format;
        $aImpact->clientCampany = ClientCompany::find($clientImpact->clientid_imp_cl);
        
        $pdf = PDF::loadView('admin.pdf.resumePdf',compact('aImpact'));
        print_r($pdf->stream('resumePdf.pdf'));die;
    }
    public function printPdf(Request $request){
        $data = $request->all();

        $aImpact = Impact::find($data["impactid"]);
        $clientImpact = ImpactClient::whereImpactid_imp_cl($aImpact->id)->first();

        $aImpact->authorobj = $aImpact->author;
        $aImpact->supportobj = $aImpact->support;
        $aImpact->rubriqueobj = $aImpact->rubrique;
        $aImpact->formatobj = $aImpact->format;
        $aImpact->clientCampany = ClientCompany::find($clientImpact->clientid_imp_cl);

        $aImpact->images = json_decode($aImpact->scan_imp);
        //dd($aImpact);

        $pdf = PDF::loadView('admin.pdf.test',compact('aImpact'));
        print_r($pdf->stream('test.pdf'));die;

        //return $pdf->download('test2.pdf');
        //return view('admin.pdf.test',compact('aImpact'));
    }

    public function printtcpdf(Request $request) {
        $data = $request->all();

        $aImpact = Impact::find($data["impactid"]);
        $date = \Carbon\Carbon::now();
        $clientImpact = ImpactClient::whereImpactid_imp_cl($aImpact->id)->first();

        $aImpact->authorobj = $aImpact->author;
        $aImpact->supportobj = $aImpact->support;
        $aImpact->rubriqueobj = $aImpact->rubrique;
        $aImpact->formatobj = $aImpact->format;
        $aImpact->clientCampany = ClientCompany::find($clientImpact->clientid_imp_cl);

        $aImpact->images = json_decode($aImpact->scan_imp);

        $namePdf =  str_replace([' ','.'],'_',$aImpact->supportobj->name_sup)."_".\Carbon\Carbon::parse($date)->format('d_m_Y');

        /******************************************************************************** */
        PDFTCPDF::SetTitle($namePdf);
        PDFTCPDF::SetAutoPageBreak(false, 0);
        PDFTCPDF::setImageScale(PDF_IMAGE_SCALE_RATIO);

        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'ltr';
        $lg['a_meta_language'] = 'fr';
        $lg['w_page'] = 'page';

        $dinfontname = TCPDF_FONTS::addTTFfont('http://localhost:8888/tlredp/public/fonts/din-bold.ttf', 'TrueTypeUnicode', '', 32);
        $geometosfontname = TCPDF_FONTS::addTTFfont('http://localhost:8888/tlredp/public/fonts/Geometos.ttf', 'TrueTypeUnicode', '', 32);
        $geometosroundedfontname = TCPDF_FONTS::addTTFfont('http://localhost:8888/tlredp/public/fonts/Geometos_Rounded.ttf', 'TrueTypeUnicode', '', 32);
        $dinarabicfontname = TCPDF_FONTS::addTTFfont('http://localhost:8888/tlredp/public/fonts/DINNEXTLTARABIC-LIGHT.ttf', 'TrueTypeUnicode', '', 32);
    //dd($aImpact);
        // use the font
        PDFTCPDF::SetFont($geometosfontname, '', 13, '', false);

        // set some language-dependent strings (optional)
        PDFTCPDF::setLanguageArray($lg);

            if(isset($aImpact->images))
            {

            foreach ($aImpact->images as $key=>$image)
            {

            //dd($aImpact);
            PDFTCPDF::AddPage();
            //PDFTCPDF::SetMargins(100, 100, 10, true);
            $html = "<h1>TEST TEST left </h1>";
            $righthtml = "<h1>Veille rédactionnelle</h1>";
            $clientName = "<h1></h1>";
            $container = "<div></div>";
            $descImpact = "<p>Article ".$aImpact->pace_imp." paru le ".\Carbon\Carbon::parse($aImpact->date_imp)->format('d-m-Y') ." dans la rubrique« ".$aImpact->rubriqueobj->nom_rub." », signé par ".$aImpact->authorobj->name_aut." <br/>Article de ".$aImpact->formatobj->nom_frm.", édité en ".$aImpact->formatobj->print_frm.", d’une valeur publicitaire équivalente à ".$aImpact->formatobj->value_ad_frm." MAD</p>";
            $containerImage = "<div></div>";
            $titleArticle = "<p>".$aImpact->title_imp."</p>";
            $supportDesc = "<p >".$aImpact->supportobj->form_edit_sup." . ".$aImpact->supportobj->period_sup ." . ".$aImpact->supportobj->lng_edit_sup." . ".$aImpact->supportobj->postition_sup."</p>";
            $y = PDFTCPDF::getY();
            $width = PDFTCPDF::getPageWidth();
            $heigh = PDFTCPDF::getPageHeight();
            $containerHeight = $heigh / 5;
            
            // dump($heigh);
            // dump($width);
            // dd($heigh - PDFTCPDF::getY());
            //$containerImageY = PDFTCPDF::getY();
            //PDFTCPDF::Rect(0, PDFTCPDF::getY(), $width,$heigh, 'F', array(), array(128,255,128));

            PDFTCPDF::SetFillColor(244, 244, 244);
            PDFTCPDF::writeHTMLCell($width, $containerHeight,0, 0, $container, 0, 1, 1, true, '', false);

            //PDFTCPDF::Image('http://localhost:8888/tlredp/storage/app/public/uploads/support/1/logo-2016.jpg', 0, 0, 75, 113, 'JPEG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 1, false, false, false);
            // LOGO
            PDFTCPDF::Image('http://localhost:8888/tlredp/public/image/logotlredp.png', 5, 5,40,'', 'PNG', '', 'T', false, 0, '', false, false, 1, false, false, false);
            //Image( $file, $x = '', $y = '', $w = 0, $h = 0, $type = '', $link = '', $align = '', $resize = false, $dpi = 300, $palign = '', $ismask = false, $imgmask = false, $border = 0, $fitbox = false, $hidden = false, $fitonpage = false, $alt = false, $altimgs = array() )

            // Print text using writeHTMLCell()
            PDFTCPDF::SetTextColor(0, 0, 0);
            // PDFTCPDF::SetFillColor(224, 82, 69);
            // //PDFTCPDF::Cell(0, 0, 'A4 PORTRAIT', 0, 0,'R');
            //dd(PDFTCPDF::getFontsList());
            // Veille redactionnelle title
            PDFTCPDF::SetFont($geometosfontname, '', PDFTCPDF::pixelsToUnits('13'), '', false);
            PDFTCPDF::writeHTMLCell(0,0, 5, 5, $righthtml, 0, 1, 1, true, 'R', true, true, '', false);
            // Client name
            PDFTCPDF::writeHTMLCell(0,0, 50, PDFTCPDF::getY(), $clientName, 0, 1, 1, true, 'R', true, true, '', false);
            //$this->Image($image_file, 90, 5, 40, '', 'PNG', '', 'T', false, 300, 'C', false, false, 0, false, false, false);
            // Support LOGO
            PDFTCPDF::Image('http://localhost:8888/tlredp/storage/app/public/uploads/support/'.$aImpact->supportobj->id.'/'.$aImpact->supportobj->logo_sup.'', 0, PDFTCPDF::getY() -2,20,'', 'PNG', '', 'T', false, 0, 'C', false, false, 1, false, false, false);
            // //writeHTMLCell( $w, $h, $x, $y, $html = '', $border = 0, $ln = 0, $fill = false, $reseth = true, $align = '', $autopadding = true )
            // // cell $w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M' )
            // Title support
            PDFTCPDF::SetFont($geometosroundedfontname, '', 13, '', false);
            PDFTCPDF::writeHTMLCell(0,0, 5, PDFTCPDF::getImageRBY() + 3, $aImpact->supportobj->name_sup, 0, 1, 1, true, 'C', true, true, '', false);
            // Description Support
            PDFTCPDF::SetFont($geometosroundedfontname, '', 7, '', false);
            PDFTCPDF::writeHTMLCell(0,0, 5, PDFTCPDF::getY() + 1,$supportDesc, 0, 1, 1, true, 'C', true, true, '', false);
            PDFTCPDF::Line( 60, PDFTCPDF::getY() + 4, $width - 60 , PDFTCPDF::getY() + 4);

            //PDFTCPDF::SetFont ('helvetica', 'B' ,10, 'default', true );
            // PDFTCPDF::SetFontSize(9);
            // Title article
            PDFTCPDF::writeHTMLCell(0,0, 0, PDFTCPDF::getY() + 7, $titleArticle, 0, 1, 1, true, 'C', true, true, '', false);
            // Description Support / Format
            PDFTCPDF::SetFont($dinarabicfontname, 'B', 9, '', false);
            // PDFTCPDF::SetFont('aealarabiya', 'B', 9);
            PDFTCPDF::writeHTMLCell(0,0, 5, PDFTCPDF::getY(), $descImpact, 0, 1, 1, true, 'C', true, true, '', false);
            // Impact Image
            PDFTCPDF::Image('http://localhost:8888/tlredp/storage/app/public/uploads/impacts/'.$aImpact->id.'/'.$image->scan.'', 0, $containerHeight ,$width-50, $heigh - $containerHeight - 10, 'PNG', '', '', true, 300, 'C', false, false, 0, 'CM', false, false);
            
            PDFTCPDF::SetFillColor(244, 244, 244);
            PDFTCPDF::writeHTMLCell($width,10, 0, $heigh - 10, '', 0, 0, 1, true, 'C', true, true, '', false);
            }
        }// foreach image 
        PDFTCPDF::Output($namePdf.'.pdf');
    }

    // download generated PDF file
    public function downloadPdfFile($filename) {
        return response()->download(storage_path('app/public/uploads/pdfs/'.$filename));
    }
}
