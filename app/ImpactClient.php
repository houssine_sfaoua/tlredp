<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImpactClient extends Model
{
    protected $table = "impact_clients";
    
    public $timestamps = false;

    protected $fillable = array("impactid_imp_cl","clientid_imp_cl","criteriaid_imp_cl","subcriteriaid_imp_cl");
}
