@extends('admin.layouts.master')

@section('content')

    <p>{!! link_to_route('formats.create', trans('quickadmin::admin.users-index-add_new'), [], ['class' => 'btn btn-success']) !!}</p>

    @if($aFormat->count() > 0)
<div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.format-index-format_list') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>{{ trans('quickadmin::admin.users-index-name') }}</th>
                        <th>{{ trans('quickadmin::admin.users-index-pseudo') }}</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($aFormat as $format)
                        <tr>
                            <td>{{ $format->nom_frm }}</td>
                            <td>{{ $format->pseudo_frm }}</td>
                            <td>
                                {!! link_to_route('formats.edit', trans('quickadmin::admin.users-index-edit'), [$format->id], ['class' => 'btn btn-xs btn-info']) !!}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        {{ trans('quickadmin::admin.users-index-no_entries_found') }}
    @endif


@endsection

{{-- {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => 'return confirm(\'' . trans('quickadmin::admin.users-index-are_you_sure') . '\');',  'route' => array('impact.destroy', $criteria->id)]) !!}
                                {!! Form::submit(trans('quickadmin::admin.users-index-delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                {!! Form::close() !!} --}}