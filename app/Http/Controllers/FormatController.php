<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Format;
use Illuminate\Support\Facades\Storage;

class FormatController extends Controller
{
    
    public function index(){

        $aFormat = Format::all();
        //dd($aFormat);
        return view("admin.formats.index",compact('aFormat'));
    }

    public function create(){
        //$rangeAdValue = range(1000, 100000);
        return view("admin.formats.create",compact('rangeAdValue'));
    }

    public function store(Request $request) {
        $data = $request->all();
        //dd($data);
        $validatedData = $request->validate([
            'nom_frm' => 'required',
            'pseudo_frm' => 'required',
            'page_frm' => 'required',
            'print_frm' => 'required',
            'value_ad_frm' => 'required'
        ]);

        $aFormat = array(
            "nom_frm" =>$data["nom_frm"],
            "pseudo_frm"=>$data["pseudo_frm"],
            "page_frm"=>$data["page_frm"],
            "print_frm"=>$data["print_frm"],
            "value_ad_frm"=>$data["value_ad_frm"]
        );

        if(isset($data['edit'])){
            $result = Format::where('id', $data['edit'])->update($aFormat);
        } else {
            $format=Format::create($request->all());
        }
        return redirect()->route('formats.index')->withMessage(trans('quickadmin::admin.item-controller-successfully_created'));
    }


    public function edit($id) {
        
        $format  = Format::findOrFail($id);
        //dd($format);
        return view('admin.formats.edit', compact('format'));
    }


}
