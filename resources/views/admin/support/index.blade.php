@extends('admin.layouts.master')

@section('content')

    <p>{!! link_to_route('support.create', trans('quickadmin::admin.users-index-add_new'), [], ['class' => 'btn btn-success']) !!}</p>

@if(Auth::user()->role_id == config('quickadmin.defaultRole'))
    @if($aSupport->count() > 0)
<div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.support-index-support_list') }}</div>
            </div>
            <div class="portlet-body">
                <table id="datatable" class="table table-striped table-hover table-responsive datatable">
                    <thead>
                    <tr>
                        <th>{{ trans('quickadmin::admin.users-index-name') }}</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($aSupport as $support)
                        <tr>
                            <td>{{ $support->name_sup }}</td>
                            <td>
                                {!! link_to_route('support.edit', trans('quickadmin::admin.users-index-edit'), [$support->id], ['class' => 'btn btn-xs btn-info']) !!}
                                {{-- {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => 'return confirm(\'' . trans('quickadmin::admin.users-index-are_you_sure') . '\');',  'route' => array('impact.destroy', $support->id)]) !!}
                                {!! Form::submit(trans('quickadmin::admin.users-index-delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                {!! Form::close() !!} --}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        {{ trans('quickadmin::admin.users-index-no_entries_found') }}
    @endif
@endif


@endsection