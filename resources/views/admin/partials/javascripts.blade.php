{{-- <script src="//code.jquery.com/jquery-1.11.3.min.js"></script> --}}
<script src="{{ url('quickadmin/js') }}/jquery-1.9.1.min.js"></script>
<script src="{{ url('quickadmin/js') }}/jquery-ui.min.js"></script>

<script src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.jqueryui.min.js"></script>
<script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/camanjs/4.0.0/caman.full.min.js"></script>
<script src="{{ url('quickadmin/js') }}/timepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.min.js"></script>
<script src="//cdn.ckeditor.com/4.5.4/full/ckeditor.js"></script>
<script src="{{ url('quickadmin/js') }}/bootstrap.min.js"></script>
<script src="{{ url('quickadmin/js') }}/main.js"></script>
{{-- <script src="{{ url('quickadmin/js/jquery.picture.cut/src') }}/jquery.picture.cut.js"></script> --}}
<script src="{{ url('quickadmin/js/') }}/cropper.js"></script>
{{-- <script src="{{ url('quickadmin/js/selectize') }}/selectize.js"></script> --}}
<script src="{{ url('quickadmin/js') }}/chosen.jquery.js"></script>
{{-- <script src="{{ url('quickadmin/js') }}/jquery.cycle2.min.js"></script> --}}
<script src="{{ url('quickadmin/js') }}/jPages.min.js"></script>


<script>

    $('.datepicker').datepicker({
        autoclose: true,
        dateFormat: "{{ config('quickadmin.date_format_jquery') }}"
    });

    $('.datetimepicker').datetimepicker({
            dateFormat: "yy-mm-dd",
            timeFormat:  "hh:mm:ss"
    });
    

    $('#datatable').dataTable({
        "language": {
            "url": "{{ trans('quickadmin::strings.datatable_url_language') }}"
        },
        "pageLength": 50
    });

</script>
