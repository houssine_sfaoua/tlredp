<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    protected $table = "criterias";
    
    public $timestamps = false;

    protected $fillable = array("label_cri");

    public function clientCompany()
    {
        return $this->belongsToMany('App\ClientCompany','client_id');
    }
    
    public static function getNameCriteria($criteriaid) {
        $criteria = Criteria::find($criteriaid);
        return $criteria->label_cri;
    }
}
