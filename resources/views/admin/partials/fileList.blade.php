<style>
    #fileListModal-body {
        width: 50%;
        margin: 0 auto;
    }
</style>
<div>
<a href="#" id="refres-list-pdf" class="btn btn-xs btn-danger" style="width: 51%;height: 33px;padding-top: 6px;">Reload</a>
@if(!empty($downloadedFiles))
    <table width="954" cellspacing="0" cellpadding="2" border="0" style="border-collapse:collapse">
        <tbody>
            <tr style="border-bottom:1px solid #a9a9a94d;">
                <td width="100" height="25" style="background-color:rgb(211,228,246);font-size: 12px;font-weight: bold;font-family: Arial;text-align: center;">Date</td>
                <td width="140" height="25" style="background-color:rgb(211,228,246);font-size: 12px;font-weight: bold;font-family: Arial;text-align: center;">Fichier</td>
            </tr>
            
                @foreach($downloadedFiles as $fileName)
                    <tr style="border-bottom:1px solid #a9a9a94d;">
                            <td width="140" height="25" style="font-size: 12px;font-family: Arial;text-align: center;"></td>
                            <td width="500" height="25" style="font-size: 12px;font-family: Arial;text-align: center;"><a href="{{ env('APP_SITE_URL') }}extranet/joint/{{ $fileName }}" target="_blank" >{{ $fileName }}</a></td>
                    </tr>
                @endforeach
        </tbody>
    </table>
    @else 
        <h2>Nothing</h2>
    @endif
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#refres-list-pdf').click(function(e){
                console.log(e);
                generatePdfList();
        })
         // get list PDF file
         function generatePdfList(){
                $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                            },
                            url: '{{URL::to("ajax/getPdfList")}}',
                            type: 'GET',
                            error: function (e) {
                                //console.log("error >>>", e);
                            },
                            success: function (res) {
                                //console.log("generatePdfList",res);
                                $("#fileListModal-body").html(res);
                            }
                });
            }
})
</script>