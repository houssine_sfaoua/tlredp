<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcriteria extends Model
{
    protected $table = "subcriterias";
    
    public $timestamps = false;

    protected $fillable = array("label_subcr","criteria_id_subcr");

    /**
    * get get subcriteria
    */
    public function criteria(){
        return $this->belongsTo('App\Criteria','criteria_id_subcr');
    }


    public function getSubcriteriaByCriteriaId($criteriaId){
        //print_r($this->whereCriteria_id_subcr($criteriaId));die;
        return $this->whereCriteria_id_subcr($criteriaId)->get();
    }
}
