$(document).ready(function(){
    console.log("support js");

    $('#openFileDialog').click(function(){
        console.log('openfiledialog');
        $('#logosup-file').trigger('click');
    });

    $('#reset-logo-image').hide();


    $('#format-select').chosen().change(function(event){
				console.log($(this).val());
				
				if($(this).val()!=null){
                    console.log($(this).val());
					if(event.target == this){
					//console.log($(this).val().join());
						console.log($(this).val().join());

						$('#formatitems').val($(this).val());
					}
				}else{
					console.log("value null");
				}
			});

    $('#logosup-file').change(function (ev) {
                //var tmppath = null;
                console.log(event.target.files[0]);
                console.log(this.files);

                var tmppath = URL.createObjectURL(event.target.files[0]);
                console.log(tmppath);

                $('#crop-image').attr('src', tmppath);

                $('#fileModal').modal('show');

            });

    $('#fileModal').on('shown.bs.modal', function () {
                console.log("modal shown");

                $('#reset-logo-image').show();

                $('.modal .modal-body').css('overflow-y', 'auto'); 
                $('.modal .modal-body').css('max-height', $(window).height() * 0.7);

                $('#crop-image').cropper({
                    autoCropArea: 0.5,
                    crop: function(e) {
                        //console.log(e);
                    }
                });

            }).on('hidden.bs.modal', function () {
                console.log("modal hidden");

            });

            $("#crop-save").click(function(){

                console.log("click save btn");
			
                var imageData = $('#crop-image').cropper('getCroppedCanvas').toDataURL("image/png");

                $("#img-logo-support").css('display','block');
                $("#img-logo-support").attr('src', imageData);
                $("#logo-data").attr('value',imageData);

                $('#crop-image').cropper('destroy');

                $('#fileModal').modal('hide');
            });

             $("#close-modal").click(function () {
                // reset input file
                $("#logosup-file").wrap('<form>').closest('form').get(0).reset();
                $("#logosup-file").unwrap();

                $('#fileModal').modal('hide');

                $('#crop-image').cropper('destroy');
            });

            
			$('#reset-logo-image').click(function(event){

				event.preventDefault();

				$("#logosup-file").wrap('<form>').closest('form').get(0).reset();
				$("#logosup-file").unwrap();

				//$('#fileModal').modal('hide');

				$("#img-logo-support").css('display','none');
				$("#img-logo-support").attr('src', '');
				$("#logo-data").attr('value','');

				$('#crop-image').cropper('destroy');

                $('#reset-logo-image').hide();
			})


})