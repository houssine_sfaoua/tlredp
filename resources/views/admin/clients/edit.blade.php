@extends('admin.layouts.master')

<style>

	#reset-logo-image{
	display: inline-block;
    width: 49.6%;
    height: 33px;
    padding-top: 6px;
    margin-top: 13px;
    margin-bottom: 16px;
    margin-left: 17.6%;
	}

	#img-logo-client{
		width: 40%;
	}
</style>

@section('content') 
{{csrf_field()}}


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


{!! Form::open(['route' => 'client.store', 'class' => 'form-horizontal','id'=>'client-form']) !!}

{!! Form::hidden('edit','', ['id'=>'client-id']) !!}

<div class="form-group">
	{!! Form::label('nom_cl', trans('quickadmin::admin.client-create-namecl'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('nom_cl', $clientCompany->nom_cl, ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.client-create-namecl')])
		!!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('email_cl', trans('quickadmin::admin.client-create-emailcl'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('email_cl', $clientCompany->email_cl, ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.client-create-emailcl')])
		!!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('tel_cl', trans('quickadmin::admin.client-create-telcl'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('tel_cl', $clientCompany->tel_cl, ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.client-create-telcl')]) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('logo_cl', trans('quickadmin::admin.client-create-logocl'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::file('logo_cl', ['class' => 'form-control','id'=>'logo-file']) !!}
	</div>
</div>


<div class="form-group">
	<div class="col-sm-10 col-sm-offset-2">
		{{-- <img src="{{ asset('/storage/uploads/clients/'.$clientCompany->logo) }}" style="display:none;width:40%;" id="img-logo-client"> --}}
		<img src="{{ env('APP_URL').'/storage/app/public/uploads/clients/'.$clientCompany->logo }}" style="display:none;" id="img-logo-client">
		<button id="reset-logo-image" class="btn btn-xs btn-danger">Reset</button>
		<input type="text" id="logo-data" name="logo" style="display:none;" />
	</div>
</div>

<div class="form-group">
        {!! Form::label('criteria_id_cl', trans('quickadmin::admin.subcriteria-create-critid'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::select('criteria_id_cl', \App\Criteria::pluck('label_cri', 'id'), null, ['class'=>'form-control','id'=>'critria-select','multiple'=>'multiple']) !!}
        </div>
</div>

<div id="wrapper">
	<div class="form-group" id="newcriteria">
		{!! Form::label('newcriteria_lbl_cl', trans('quickadmin::admin.client-create-newcriteriacl'), ['class'=>'col-sm-2 control-label']) !!}
		<div class="col-sm-10">
			{!! Form::text('newcriteria_lbl_cl[]', "", ['class'=>'form-control','placeholder'=> trans('quickadmin::admin.client-create-newcriteriacl')]) !!}
		</div>
	</div>

	<button class="add_field_button">add</button>
</div>

{{-- <div class="form-group" id="newcriteria">
	{!! Form::label('newcriteria_lbl_cl', trans('quickadmin::admin.client-create-newcriteriacl'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('newcriteria_lbl_cl', "", ['class'=>'form-control','placeholder'=> trans('quickadmin::admin.client-create-newcriteriacl')]) !!}
	</div>
</div> --}}

{{-- \App\Subcriteria::pluck('label_subcr', 'id') --}}
<div class="form-group">
        {!! Form::label('subcriteria_id_cl', trans('quickadmin::admin.subcriteria-create-critid'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::select('subcriteria_id_cl', [], null, ['class'=>'form-control','id'=>'subcritria-select','multiple'=>'multiple']) !!}
        </div>
</div>

<div id="wrapper-sub">
	<div class="form-group" id="newsubcriteria">
		{!! Form::label('newsubcriteria_lbl_cl', trans('quickadmin::admin.client-create-newcriteriacl'), ['class'=>'col-sm-2 control-label']) !!}
		<div class="col-sm-10">
			{!! Form::text('newsubcriteria_lbl_cl[]', "", ['class'=>'form-control','placeholder'=> trans('quickadmin::admin.client-create-newcriteriacl')]) !!}
		</div>
	</div>

	<button class="sub_add_field_button">add</button>
</div>



{!! Form::hidden('criteriaitems','', ['id'=>'criteriaitems']) !!}
{!! Form::hidden('subcriteriaitems','', ['id'=>'subcriteriaitems']) !!}


<div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.criterias-create-btncreate'), ['class' => 'btn btn-primary','id'=>'submit-create-client']) !!}
        </div>
</div>


{!! Form::close() !!}

<!-- Modal -->
<div id="fileModal" class="modal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				{{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
				{{-- <h4 class="modal-title">Logo</h4> --}}
			</div>
			<div class="modal-body">
				<div style="height: 30em;width:auto;">
					<img id="crop-image" src="" style="">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="save-logo">Enregistrer</button>
				<button type="button" class="btn btn-default"  id="close-modal">Annuler</button>
			</div>
		</div>

	</div>
</div>


<script type="text/javascript">
    
    $(document).ready(function () {

        var clientGlobal = {!! json_encode($clientCompany) !!};
        console.log(clientGlobal);

        if(clientGlobal.logo_cl!=null && clientGlobal.logo_cl!=""){
            $("#img-logo-client").css('display', 'block');
        }

        $('#wrapper').hide();
		$('#wrapper-sub').hide();

        $('#client-id').val(clientGlobal.id);

		//getSubcriteriaByCriteriaId(8);

		var option = $("<option>").val("autre").text("autre");
		var suboption = $("<option>").val("autre").text("autre");

		$('#critria-select').prepend(option);
		$('#subcritria-select').prepend(suboption);

        $('#critria-select').chosen().change(function(event){
				console.log("values chosen >>",$(this).val());
				
				if($(this).val()!=null){

					if(event.target == this){
					//console.log($(this).val().join());
						console.log($(this).val().join());

						getSubcriteriaByCriteriaId($(this).val().join());

						$('#criteriaitems').val($(this).val());

					}
				
					if(typeof $('#criteriaitems') != "undefined" && $('#criteriaitems').val() =="autre"){
						console.log($('#criteriaitems').val());

						$('#wrapper').show();
					}

				}else{
					console.log("value null");

					//$("#critria-select").find('option').remove().end().append('<option value="autre">autre</option>');
					$("#subcritria-select").empty();
					$("#subcritria-select").val('');
					$("#subcritria-select").append('<option value="autre">autre</option>');
					$('#subcritria-select').trigger("chosen:updated");

					if(typeof $('#wrapper') != 'undefined' && $('#wrapper') != null)
						$('#wrapper').hide();
				}
			});

            var criteriaArray = [];
            var subcriteriaArray = [];

            $.each(clientGlobal.criteria,function(index,item){
                console.log(item);

                criteriaArray.push(item.id);
            });

            $.each(clientGlobal.subcriteria,function(index,item){
                console.log(item);

                subcriteriaArray.push(item.id);
            });

            console.log("sub criteria >>",subcriteriaArray);

            $('#critria-select').val(criteriaArray);
            
            $('#critria-select').trigger("chosen:updated").change();

            chosenSubCriteriaList();

            
			/**************************Add new criteria / subcriteria **********************/


			var max_fields = 20; //maximum input boxes allowed
			var wrapper = $("#wrapper"); //Fields wrapper
			var add_button = $(".add_field_button"); //Add button ID
 
			
			$(add_button).click(function(e){ //on add input button click
			e.preventDefault();
			console.log("click button");
				
				$(wrapper).append('<div class="form-group">{!! Form::label("newcriteria_lbl_cl", trans("quickadmin::admin.client-create-newcriteriacl"), ["class"=>"col-sm-2 control-label"]) !!}<div class="col-sm-8">{!! Form::text("newcriteria_lbl_cl[]", "", ["class"=>"form-control","placeholder"=> trans("quickadmin::admin.client-create-newcriteriacl")]) !!}</div><button class="remove_field col-sm-2">delete</button></div>'); //add input box
				
			});
			
			$(wrapper).on("click",".remove_field", function(e){ //user click on remove field
					e.preventDefault();
					 $(this).parent('div').remove(); 
			})

			var wrapper_sub = $("#wrapper-sub"); //Fields wrapper
			var add_button_sub = $(".sub_add_field_button"); //Add button ID
 
			
			$(add_button_sub).click(function(e){ //on add input button click
			e.preventDefault();
			console.log("click button");
				
				$(wrapper_sub).append('<div class="form-group">{!! Form::label("newsubcriteria_lbl_cl", trans("quickadmin::admin.client-create-newcriteriacl"), ["class"=>"col-sm-2 control-label"]) !!}<div class="col-sm-8">{!! Form::text("newsubcriteria_lbl_cl[]", "", ["class"=>"form-control","placeholder"=> trans("quickadmin::admin.client-create-newcriteriacl")]) !!}</div><button class="sub_remove_field col-sm-2">delete</button></div>'); //add input box
				
			});
			
			$(wrapper_sub).on("click",".sub_remove_field", function(e){ //user click on remove field
					e.preventDefault();
					 $(this).parent('div').remove(); 
			})

			/**************************************************/

			$('#reset-logo-image').click(function(event){

				event.preventDefault();

				$("#logo-file").wrap('<form>').closest('form').get(0).reset();
				$("#logo-file").unwrap();

				//$('#fileModal').modal('hide');

				$("#img-logo-client").css('display','none');
				$("#img-logo-client").attr('src', '');
				$("#logo-data").attr('value','');

				$('#crop-image').cropper('destroy');
			});
        
    	$('#logo-file').change(function(ev) {

            var tmppath = URL.createObjectURL(event.target.files[0]);
            console.log("tmp path"+tmppath);
            $('#crop-image').attr('src', tmppath);
            console.log($('#fileModal'));
            $('#fileModal').modal('show');

        });

        $('#fileModal').on('shown.bs.modal', function() {
            console.log("modal shown");

            $('#crop-image').cropper({
                autoCropArea: 0.5,
                crop: function(e) {
                    // Output the result data for cropping image.
                   // console.log("crop >>>",e);
                },
                ready: function() {
                    console.log("ready");

                    //$(this).cropper('setCanvasData', canvasData);
                    //$(this).cropper('setCropBoxData', cropBoxData);

                    //console.log(canvasData);
                }
            });

        }).on('hidden.bs.modal', function() {
            console.log("modal hidden");
            
			

        });

		$("#close-modal").click(function(){
			// reset input file
			$("#logo-file").wrap('<form>').closest('form').get(0).reset();
    		$("#logo-file").unwrap();

			$('#fileModal').modal('hide');
		});

		$("#save-logo").click(function(){
			console.log("click save btn");
			
			var imageData = $('#crop-image').cropper('getCroppedCanvas').toDataURL("image/png");

            $("#img-logo-client").css('display','block');
            $("#img-logo-client").attr('src', imageData);
            $("#logo-data").attr('value',imageData);

            $('#crop-image').cropper('destroy');

			$('#fileModal').modal('hide');

		})



		function getSubcriteriaByCriteriaId(criteriaId){

			$.ajax({
				headers: {
													'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
											},
											url: '{{URL::to("ajax/getsubcriteribycrid")}}',
											type: 'POST',
											datatType : 'json',
											data: {
												'criteriaid' :criteriaId 
											},
											success:function(data) {
												console.log(data);
												console.log(data[0]);
												if(typeof data != "undefined" && data.length>0){
													$("#subcritria-select").empty();
													$("#subcritria-select").append('<option value="autre">autre</option>');

													$.each(data,function(key,value){

														console.log(value.id);
														console.log($("#subcritria-select option[value="+value.id+"]").length > 0);
														// exist item
														if($("#subcritria-select option[value="+value.id+"]").length == 0){
															$('#subcritria-select').append('<option value='+value.id+'>'+value.label_subcr+'</option>');
														}
														
														// delete item problem
                                                        $('#subcritria-select').val(subcriteriaArray);
														$('#subcritria-select').trigger("chosen:updated").change();
													});
												}else{

													console.log("empty array sub criteria");
												}

												
											}
										})	
		}


		function chosenSubCriteriaList(){
			$("#subcritria-select").chosen().change(function(event){

				if($(this).val()!=null){

					if(event.target == this){
						//console.log($(this).val().join());
						$('#subcriteriaitems').val($(this).val());
					}

				    if(typeof $('#subcriteriaitems') != "undefined" && $('#subcriteriaitems').val() =="autre"){
						console.log($('#subcriteriaitems').val());

						$('#wrapper-sub').show();
					}

				}else{

					console.log("value null");
					if(typeof $('#wrapper-sub') != 'undefined' && $('#wrapper-sub') != null)
						$('#wrapper-sub').hide();
				}
				
			});
		}

    });
</script>

@endsection

