<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCriteria extends Model
{
    protected $table = "client_criterias";
    
    public $timestamps = false;

    protected $fillable = array("criteria_id","client_id");


    public function criteria(){
        return $this->belongsTo('App\Criteria','criteria_id');
    }


}
