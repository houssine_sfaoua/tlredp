<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImpactClientsTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('impact_clients', function (Blueprint $table) {
            $table->integer('impactid_imp_cl')->unsigned();
            $table->integer("clientid_imp_cl")->unsigned();
            $table->integer("criteriaid_imp_cl")->unsigned();
            $table->integer("subcriteriaid_imp_cl")->unsigned();

            $table->foreign('impactid_imp_cl')->references('id')
            ->on('impacts');
            $table->foreign('clientid_imp_cl')->references('id')
            ->on('client_companies');
            $table->foreign('criteriaid_imp_cl')->references('id')
            ->on('criterias');
            $table->foreign('subcriteriaid_imp_cl')->references('id')
            ->on('subcriterias');

            
        });
        
    }
    
    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('impact_clients');
    }
}