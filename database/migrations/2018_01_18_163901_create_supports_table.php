<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_sup');
            $table->string('origin_sup');
            $table->string('cat_sup');
            $table->string('edit_orie_sup');
            $table->string('period_sup');
            $table->string('form_edit_sup');// changed
            $table->string('lng_edit_sup');
            $table->string('postition_sup');
            $table->string('logo_sup')->nullable();
            
            //$table->foreign('crit_id_sup')->references('id')->on('criterias');
            //$table->foreign('subcrit_id_sup')->references('id')->on('subcriterias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supports');
    }
}
