<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientSubcriteriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_subcriterias', function (Blueprint $table) {
            $table->integer('subcriteria_id')->unsigned()->nullable();
            $table->foreign('subcriteria_id')->references('id')
                    ->on('subcriterias');

            $table->integer('client_id')->unsigned()->nullable();
            $table->foreign('client_id')->references('id')
                    ->on('client_companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_subcriterias');
    }
}
