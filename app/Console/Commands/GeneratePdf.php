<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Cronjob;
use App\ImpactClient;
use App\ClientCompany;
use App\Impact;
use File;
use PDF;
ini_set('memory_limit', '-1');
ini_set("pcre.backtrack_limit", "50000000");

class GeneratePDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate_pdf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate pdf from html';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->_generatePdf();
        // // get all cron that not yet complete
        $cronjob = Cronjob::whereNull('completed_at')->get();
        //dd($cronjob[0]);
        $impactIds = array();
            if(isset($cronjob[0]) && isset($cronjob[0]->data)){
                $impactIds=explode(",", $cronjob[0]->data);
            }
            //dd($impactIds);
            $aImpactArray = array();
            foreach ($impactIds as $key => $impactid) {
                if (isset($impactid) && $impactid != null && !is_null($impactid)){
                    $aImpact = Impact::find($impactid);
                    $clientImpact = ImpactClient::whereImpactid_imp_cl($aImpact->id)->first();
                    if  (isset($clientImpact) && $clientImpact != null && !is_null($clientImpact)) {
                        $aImpact->authorobj = $aImpact->author;
                        $aImpact->supportobj = $aImpact->support;
                        $aImpact->rubriqueobj = $aImpact->rubrique;
                        $aImpact->formatobj = $aImpact->format;
                        $aImpact->clientCampany = ClientCompany::find($clientImpact->clientid_imp_cl);
                        //dd(json_decode($aImpact->scan_imp));
                        $aImpact->images = json_decode($aImpact->scan_imp);
                        array_push($aImpactArray,$aImpact);
                    }
                }
            }
            $cronjob[0]->completed_at = date('Y-m-d H:i:s');
            $cronjob[0]->save();
            //dd('app/public/uploads/pdfs/'.$cronjob[0]->id.'_pdfname.pdf');
            $pdf = PDF::loadView('admin.pdf.impactspdf',compact('aImpactArray'))->save(storage_path('app/public/uploads/pdfs/'.$cronjob[0]->id.'_pdfname.pdf'));
        
        //print_r($pdf->stream('resumePdf.pdf'));die;
        //dd($pdf);
        //File::put('mytextdocument.txt',$aImpactArray);
        // foreach ($jobs as $job) {

        //     $job->executed_at = date('Y-m-d H:i:s');
        //     $job->save();

        //     $this->_generatePdf($job);

        //     // mark it as completed
        //     $job->completed_at = date('Y-m-d H:i:s');
        //     $job->save();
        // }
        dd("ok");
    }

    protected function _generatePdf()
    {
        //File::put('mytextdocument.txt','John Doe houssine shouosodou');
        // $data = json_decode($job->data, 1);

        // $path = storage_path('files/users/' . $job->user_id . '/pdf');
        // @mkdir($path, 0755, true);

        // $html_file = storage_path('files/users/' . $job->user_id . '/html/' . $data['html']);

        // $file = 'your-awesome-pdf-file.pdf';

        // $cmd_output = exec(base_path('bin/wkhtmltopdf') . ' ' . $html_file . ' ' . $path . '/' . $file);

        // unlink($html_file); // remove temporary html file
    }
}