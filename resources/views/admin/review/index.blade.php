@extends('admin.layouts.master')
<style>
    #prevMailModal-body > div {
        margin: 0 auto;
        width: 50%;
    }
    #submit-send-mail {
        position: relative;
        left: 80%;
    }
    #client_review_mail_chosen{
        width: 100% !important;
    }
</style>
@section('content')


<div class="content col-md-6">
    {{-- {!! Form::open(['route' => 'impact.store', 'class' => 'form-horizontal'])!!} --}}

    <div class="form-group col-md-12">
            {!! Form::label('impact_title_review', trans('quickadmin::admin.impact-create-title'), ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {{ Form::select('impact_title_review',\App\Impact::pluck('title_imp','title_imp'),'',['placeholder' => 'Titre','id'=>'impact-title-review','class'=>'form-control']) }}
            </div>
    </div>
    <div class="form-group col-md-12">
            {!! Form::label('impact_nature_review', trans('quickadmin::admin.impact-create-nature_imp'), ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {{ Form::select('impact_nature_review',['Article dédié'=>'Article dédié','Citation'=>'Citation'],'',['placeholder' => 'Nature','id'=>'impact-nature-review','class'=>'form-control']) }}
            </div>
    </div>
    <div class="form-group col-md-12">
        {!! Form::label('impact_language_review', trans('quickadmin::admin.impact-create-language'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {{ Form::select('impact_language_review', ['1'=>'Arabe','2'=>'Français','3'=>'Anglais'],'',['placeholder' => trans('quickadmin::admin.impact-create-language'),'id'=>'impact-language-review','class'=>'form-control']) }}
        </div>
    </div>
    <div class="form-group col-md-12">
            {!! Form::label('author_review', trans('quickadmin::admin.impact-create-author'), ['class'=>'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {{ Form::select('author_review',\App\Author::pluck('name_aut','id'),'',['placeholder' => 'Choisir un auteur','id'=>'author-review','class'=>'form-control']) }}
            </div>
        </div>

    <div class="form-group col-md-12">
        {!! Form::label('client_review', trans('quickadmin::admin.impact-create-clientcmp'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
			{{ Form::select('client_review',\App\ClientCompany::pluck('nom_cl','id'),'',['placeholder' => 'Choisir un client','id'=>'client-review','class'=>'form-control']) }}
		</div>
    </div>

    <div class="form-group col-md-12">
        {!! Form::label('date_begin', trans('quickadmin::admin.reviews-index-date_begin'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('date_begin', "", ['class'=>'form-control datepicker','id'=>'date-begin-review', 'placeholder'=> trans('quickadmin::admin.reviews-index-date_begin')])
            !!}
        </div>
    </div>

<div class="form-group col-md-12">
	{!! Form::label('date_end', trans('quickadmin::admin.reviews-index-date_end'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('date_end', "", ['class'=>'form-control datepicker','id'=>'date-end-review', 'placeholder'=> trans('quickadmin::admin.reviews-index-date_end')])
		!!}
	</div>
</div>
<div class="form-group col-md-12">
	{!! Form::label('period', trans('quickadmin::admin.impact-create-period-tag'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
        {{ Form::checkbox('period_tag',1,true, array('id'=>'first_period','class'=>'check_class')) }}
        {!! Form::label('period', "Matin", ['class'=>'']) !!}
        {{ Form::checkbox('period_tag',2,false, array('id'=>'second_period','class'=>'check_class')) }}
        {!! Form::label('period', "Après-midi", ['class'=>'']) !!}
	</div>
</div>

<div class="form-group col-md-12">
        {!! Form::label('pace_review', trans('quickadmin::admin.impact-create-pace'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
			{{ Form::select('pace_review',[''=>'','positif'=>'Positif', 'negatif'=>'Négatif', 'neutre'=>'Neutre'],'',['id'=>'pace-review','class'=>'form-control']) }}
		</div>
    </div>

<div class="form-group col-md-12">
	{!! Form::label('support_review', trans('quickadmin::admin.impact-create-support'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('support_review', \App\Support::pluck('name_sup', 'id'),'',['placeholder' => 'Choisir support','id'=>'support-review','class'=>'form-control']) }}
	</div>
</div>
<div class="form-group col-md-12">
        {!! Form::label('cat_sup_review', trans('quickadmin::admin.support-create-catsup'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
                {{ Form::select('cat_sup_review', [''=>'','Agence de presse'=>'Agence de presse','Portail du Maroc'=>'Portail du Maroc','Presse electronique'=>'Presse electronique','Presse Papier'=>'Presse Papier'],'',['id'=>'cat-sup-review','class'=>'form-control'])
            }}
        </div>
    </div>


</div>

<div class="col-md-6">
        <div class="form-group col-md-12">
                {!! Form::label('edit_orie_review', trans('quickadmin::admin.support-create-catsup'), ['class'=>'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                        {{ Form::select('edit_orie_review', [''=>'','Presse partisane'=>'Presse partisane', 'Presse independante'=>'Presse independante','Presse officielle'=>'Presse officielle'],'',['id'=>'edit-orie-review','class'=>'form-control'])
                    }}
                </div>
            </div>
    
            <div class="form-group col-md-12">
                    {!! Form::label('period_review', trans('quickadmin::admin.support-create-period'), ['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-sm-10">
                        {{ Form::select('period_review', [''=>'','Bi-mensuel'=>'Bi-mensuel','Quotidien'=>'Quotidien', 'Hebdomadaire'=>'Hebdomadaire','Mensuel'=>'Mensuel','Bimestriel'=>'Bimestriel','Trimestriel'=>'Trimestriel','En continu'=>'En continu'],'',['id'=>'period-review','class'=>'form-control'])
                        }}
                    </div>
            </div>
    
                <div class="form-group col-md-12">
                        {!! Form::label('form_edit_review', trans('quickadmin::admin.support-create-typeedit'), ['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {{ Form::select('form_edit_review', [''=>'','Journal'=>'Journal', 'Magazine'=>'Magazine','En ligne'=>'En ligne'],'',['id'=>'form-edit-review','class'=>'form-control'])
                            }}
                        </div>
                </div>
                <div class="form-group col-md-12">
                        {!! Form::label('lng_edit_review', trans('quickadmin::admin.support-create-lngedit'), ['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {{ Form::select('lng_edit_review', [''=>'','Arabophone'=>'Arabophone', 'Francophone'=>'Francophone','Espagnol'=>'Espagnol','Anglophone'=>'Anglophone','Bilingue'=>'Bilingue','Trilingue'=>'Trilingue','Multilingue'=>'Multilingue'],'',['id'=>'lng-edit-review','class'=>'form-control'])
                            }}
                        </div>
                    </div>
    
                    <div class="form-group col-md-12">
                            {!! Form::label('postition_review', trans('quickadmin::admin.support-create-lngedit'), ['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                    {{ Form::select('postition_review', [''=>'','Presse generaliste'=>'Presse generaliste','Presse MRE'=>'Presse MRE',
                                    'Presse officielle'=>'Presse officielle','Presse politique'=>'Presse politique','Presse politique (Sahara )'=>'Presse politique (Sahara )',
                                    'Presse RH'=>'Presse RH','Presse RSE'=>'Presse RSE','Presse satirique'=>'Presse satirique','Presse sportive'=>'Presse sportive',
                                    'Presse TIC'=>'Presse TIC','Presse Toursime'=>'Presse Toursime',
                                    'Presse medecine'=>'Presse medecine',
                                    'Presse masculine'=>'Presse masculine',
                                    'Presse maritime'=>'Presse maritime','Presse Management'=>'Presse Management',
                                    'Presse juridique'=>'Presse juridique','Presse Industrie'=>'Presse Industrie',
                                    'Presse Histoire'=>'Presse Histoire','Agregateur generaliste'=>'Agregateur generaliste',
                                    'presse financière'=>'presse financière','Presse feminine'=>'Presse feminine',
                                    'Presse economique'=>'Presse economique','Presse Ecologie & Environnement'=>'Presse Ecologie & Environnement','Presse eco & TIC'=>'Presse eco & TIC',
                                    'Presse Conso'=>'Presse Conso','Presse Cityguide'=>'Presse Cityguide',
                                    'Presse Art'=>'Presse Art','Presse auto/moto'=>'Presse auto/moto',
                                    'Presse BTP'=>'Presse BTP','Presse Art de vivre'=>'Presse Art de vivre',
                                    'Presse Agricole'=>'Presse Agricole','Presse Agroalimentaire'=>'Presse Agroalimentaire',
                                    'Généraliste'=>'Généraliste', 'économique'=>'économique','financier'=>'financier',
                                    'féminin'=>'féminin','satirique'=>'satirique','TIC'=>'TIC','Santé'=>'Santé','Industrie'=>'Industrie'],'',['id'=>'postition-review','class'=>'form-control'])
                                    }}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('criteria_review', trans('quickadmin::admin.criteria-create-labelcr'), ['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                {{ Form::select('criteria_review', \App\Criteria::pluck('label_cri', 'id'),'',['placeholder' => 'Choisir critère','id'=>'criteria-review','class'=>'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('impact_print_review', trans('quickadmin::admin.impact-create-print'), ['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                    {{ Form::select('impact_print_review', [''=>'','couleur'=>'couleur','noirblanc'=>'noirblanc'],'',['id'=>'impact-print-review','class'=>'form-control'])
                                }}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('impact_pubval_review', trans('quickadmin::admin.impact-create-pub_value'), ['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                    {{ Form::text('impact_pubval_review', '',['id'=>'impact-pubval-review','class'=>'form-control']) }}
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                                {!! Form::label('impact_appear_review', trans('quickadmin::admin.impact-create-pub_value'), ['class'=>'col-sm-2 control-label']) !!}
                            <div class="col-sm-10 appear-select-imp">
                                {!! Form::select('appear_select_imp',\App\Appear::pluck('label_appe', 'id'),'', ['class'=>'form-control','id'=>'impact-appear-review', 'placeholder'=> trans('quickadmin::admin.impact-create-appear')])
                                !!}
                            </div>
                        </div>
    
</div>
<div class="col-sm-10">
        <a href="#" id="review-press-search" class="btn btn-primary">Search</a>
        <a href="#" id="review-press-empty-search" class="btn btn-primary">Mail aucune retombée</a>
    </div>

<div class="portlet box">
    <div id="print-impacts-form">
        {!! Form::open(['route' => 'reviews.impacts.tcpdf', 'id'=>'pdf-submit','class' => 'form-horizontal','style'=>'float: right;'])!!}
        {!! Form::hidden('impactid','', ['id'=>'impact-array-id']) !!}
        {!! Form::submit('LIST PDF', array('class' => 'btn btn-xs btn-danger')) !!}
        {!! Form::close() !!}
        <a href="#" id="mailModalBtn" class="btn btn-xs btn-danger" style="width: 51%;height: 33px;padding-top: 6px;">mail</a>
        <a href="#" id="fileModelBtn" class="btn btn-xs btn-danger" style="width: 51%;height: 33px;padding-top: 6px;">show list file</a>
        <!-- {!! Form::open(['route' => 'reviews.impacts.send','id'=>'send-mail-submit','class' => 'form-horizontal','style'=>'float: right;'])!!}
        {!! Form::hidden('impacts_email','', ['id'=>'impact-emails-id']) !!}
        {!! Form::submit('Envoyer', array('class' => 'btn btn-xs btn-danger')) !!}
        {!! Form::close() !!} -->
    </div>
            {{-- <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.impact-index-impact_list') }}</div>
            </div> --}}
            <div class="portlet-body">
                <table id="review-dt" class="table table-striped table-hover table-responsive">
                    <thead>
                        <tr>
                            <th></th>
                            <th>id</th>
                            <th>{{ trans('quickadmin::admin.impact-index-name')  }}</th>
                            <th>{{ trans('quickadmin::admin.impact-index-title') }}</th>
                            <th>{{ trans('quickadmin::admin.impact-index-date-parution') }}</th>
                            <th>{{ trans('quickadmin::admin.impact-index-date-integration') }}</th>
                            <th>{{ trans('quickadmin::admin.impact-index-user-add-impact') }}</th>
                            <th>&nbsp;</th>
                            {{-- <th>{{ trans('quickadmin::admin.impact-index-title') }}</th>
                            
                            <th>&nbsp;</th> --}}
                        </tr>
                    </thead>

                    <tbody id="review-tbody">
                        
                    </tbody>
                </table>
            </div>
        </div>

<!-- Modal -->
<div id="mailModal" class="modal fade" role="dialog">
	<div class="modal-dialog" style="width: 100%;">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				{{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
				<h4 class="modal-title">Modal Header</h4>
			</div>
			<div class="modal-body">
                <div class="form-group col-md-12">
                        {!! Form::label('client_review', trans('quickadmin::admin.impact-create-clientcmp'), ['class'=>'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {{ Form::select('client_review',\App\ClientCompany::pluck('nom_cl','id'),'',['placeholder' => 'Choisir un client','id'=>'client-review-mail','class'=>'form-control']) }}
                        </div>
                </div>
                <div id="submit-empty-mail">
                    {!! Form::open(['route' => 'reviews.impactsempty.sendempty','id'=>'send-emptymail-submit','class' => 'form-horizontal','style'=>''])!!}
                    {!! Form::hidden('client_review','', ['id'=>'selected-client-id']) !!}
                    {!! Form::submit('Envoyer', array('class' => 'btn btn-xs btn-danger','id'=>'submit-send-mail')) !!}
                    {!! Form::close() !!}
                </div>
                    
                
			</div>
		</div>

	</div>
</div>
<!-- Prev Email  -->
<div id="prevMailModal" class="modal fade" role="dialog">
	<div class="modal-dialog" style="width: 100%;">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<div id="modal-title-prev">
                    elfklekflkeflkeelfkelfk
                </div>
            </div>
                <div class="modal-body" >
                    <!-- @include('emails.impactsmail',array('aImpacts' => [])) -->
                    <div id="prevMailModal-body">
                    </div>
                    <div id="select-files-container">

                    </div>
                    {!! Form::open(['route' => 'reviews.impacts.send','id'=>'send-mail-submit','class' => 'form-horizontal','style'=>''])!!}
                    {!! Form::hidden('impacts_email','', ['id'=>'impact-emails-id']) !!}
                    {!! Form::hidden('client_review','', ['id'=>'selected-client-id']) !!}
                    {!! Form::hidden('pdf_name','', ['id'=>'pdf-name-impact']) !!}
                    {!! Form::submit('Envoyer', array('class' => 'btn btn-xs btn-danger','id'=>'submit-send-mail')) !!}
                    {!! Form::close() !!}
                </div>
			
		</div>

	</div>
</div>
<!-- list pdf -->
<div id="pdfListModal" class="modal fade" role="dialog">
	<div class="modal-dialog" style="width: 100%;">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
			</div>
			<div class="modal-body">
                <div id="fileListModal-body">
                </div>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){

            $('.check_class').bind('click',function() {
                $('.check_class').not(this).prop("checked", false);
            });


            $('#review-dt').hide();
            $('#print-impacts-form').hide();
            $('#review-press-empty-search').hide();
            $('#submit-empty-mail').hide();
            var reviewdatatableReview = null;
            var selectedClient = null;
            

            $('#support-review').chosen();
            $('#client-review').chosen();
            $('#author-review').chosen();
            $('#cat-sup-review').chosen();
            $('#edit-orie-review').chosen();
            $('#period-review').chosen();
            $('#postition-review').chosen();
            $('#criteria-review').chosen();
            $('#impact-title-review').chosen();
            $('#impact-nature-review').chosen();
            $('#impact-language-review').chosen();
            $('#impact-print-review').chosen();
            $('#impact-appear-review').chosen();

            $('#client-review-mail').chosen().change(function() {
                //console.log("client-review-mail");
                //console.log($(this).val());
                
                selectedClient = $(this).val();
                $("#selected-client-id").val(selectedClient);
                var checkedInputDT = $('.select-checkbox input[type=checkbox]:checked');
                console.log("checkedInputDT >>> ",checkedInputDT);
                var arrayids = [];
                reviewdatatableReview.column(0).nodes().to$().each(function(index) {
                    ////console.log($(this).find(">:first-child").is(':checked'));
                    if($(this).find(">:first-child").is(':checked')){
                        arrayids[index] = $(this).find(">:first-child").attr('value');
                        $('#impact-emails-id').val(arrayids.toString()); 
                        $('#impact-array-id').val(arrayids.toString()); 
                        ////console.log(arrayids);
                    } else {
                        console.log("empty checked item");
                    }
                })
                console.log("arrayids >> ",arrayids);
                if (arrayids.length > 0) {
                    $('#mailModal').modal('hide');
                    generateMailsImpact(arrayids.toString());
                    $('#prevMailModal').modal('show');
                    $(this).val('');
                } else {
                  console.log("empty send")  
                }
                
            });

    // date range
            $("#date-begin-review").datetimepicker({
                dateFormat: "dd-mm-yy",
                timeFormat:  "hh:mm:ss",
                 onSelect: function () {
                    var endDate = $('#date-end-review');
                    var startDate = $(this).datepicker('getDate');
                    //add 30 days to selected date
                    startDate.setDate(startDate.getDate() + 120);
                    var minDate = $(this).datepicker('getDate');
                    //minDate of dt2 datepicker = dt1 selected day
                    endDate.datepicker('setDate', minDate);
                    //sets endDate maxDate to the last day of 30 days window
                    endDate.datepicker('option', 'maxDate', startDate);
                    //first day which can be selected in endDate is selected date in dt1
                    endDate.datepicker('option', 'minDate', minDate);
                    //same for dt1
                    //$(this).datepicker('option', 'minDate', minDate);
                }
            });

            $("#date-end-review").datetimepicker({
                dateFormat: "dd-mm-yy",
                timeFormat:  "hh:mm:ss",
            });

            $('#review-press-empty-search').click(function(){
                console.log("empty search click");
                $('#mailModal').modal('show');
                $('#submit-empty-mail').show();
            });

            $('#review-press-search').click(function(){
                ////console.log($(this));

                var clientid = $('#client-review').val();
                var datebegin = $('#date-begin-review').val();
                var dateend = $('#date-end-review').val();
                var supportid = $('#support-review').val();
                var authorid = $('#author-review').val();
                console.log($('input:checkbox[name=period_tag]:checked').val());
                var periodTag = $('input:checkbox[name=period_tag]:checked').val();
                //var authorid = $('#author-review').val();
                var paceid = $('#pace-review').val();
                var paceid = $('#pace-review').val();
                var catSupport = $('#cat-sup-review').val();
                var editOrientation = $('#edit-orie-review').val();
                var periodReview = $('#period-review').val();
                var formatEdit = $('#form-edit-review').val();
                var languageEdit  = $('#lng-edit-review').val();
                var positionReview = $('#postition-review').val();
                var criteriaReview = $('#criteria-review').val();

                var impactTitleReview = $('#impact-title-review').val();
                var impactNatureReview = $('#impact-nature-review').val();
                var impactLanguageReview = $('#impact-language-review').val();
                var impactPrintReview = $('#impact-print-review').val();
                var impactPubvalReview = $('#impact-pubval-review').val();
                var impactAppearReview = $('#impact-appear-review').val();
                
                //console.log(authorid)
                //console.log(paceid);
                //console.log(catSupport);
                //console.log(editOrientation);
                //console.log(periodReview);
                //console.log(formatEdit);
                //console.log(languageEdit);
                //console.log("title impact ",impactTitleReview);
                //console.log("nature impact ",impactNatureReview);
                //console.log("language ",impactLanguageReview);
                //console.log("impact appear",impactAppearReview);

                //console.log(datebegin);

                //console.log(supportid);
                console.log(datebegin);


                searchImpact(clientid,datebegin,dateend,supportid,authorid,paceid,catSupport,editOrientation,periodReview,
                    formatEdit,languageEdit,criteriaReview,impactTitleReview,impactNatureReview,
                    impactLanguageReview,impactPrintReview,impactPubvalReview,impactAppearReview,periodTag)
                
            });

            
            $("#pdf-submit").submit(function(event){
                //event.preventDefault();
                ////console.log("sumit send pdf");

                var checkedInputDT = $('.select-checkbox input[type=checkbox]:checked');
                //////console.log(checkedInputDT[0]);
                var arrayids = [];
                reviewdatatableReview.column(0).nodes().to$().each(function(index) {
                    ////console.log($(this).find(">:first-child").is(':checked'));
                    if($(this).find(">:first-child").is(':checked')){
                        arrayids[index] = $(this).find(">:first-child").attr('value');
                        $('#impact-array-id').val(arrayids.toString()); 
                    }
                })
            })
            
                // checked inputs 
                $("#send-mail-submit").submit(function(event){
                //event.preventDefault();
                ////console.log("sumit send pdf");

                var checkedInputDT = $('td.select-checkbox input.selectedCheckbox');
                //////console.log(checkedInputDT[0]);
                var arrayids = [];
                reviewdatatableReview.column(0).nodes().to$().each(function(index) {
                    ////console.log($(this).find(">:first-child").is(':checked'));
                    if($(this).find(">:first-child").is(':checked')){
                        arrayids[index] = $(this).find(">:first-child").attr('value');
                        $('#impact-emails-id').val(arrayids.toString()); 
                        $('#impact-array-id').val(arrayids.toString()); 
                        ////console.log(arrayids);
                    }
                })
            })
            

            function searchImpact(clientid,datebegin,dateend,supportid,authorid,paceid,catSupport,editOrientation,periodReview,
            formatEdit,languageEdit,criteria,impactTitle,nature,language,print,pubValue,appear,periodTag){
                //console.log(supportid);

                $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                            },
                            url: '{{URL::to("ajax/searchimpact")}}',
                            type: 'POST',
                            datatType: 'json',
                            data: {
                                'clientid': clientid,
                                'datebegin': datebegin,
                                'dateend': dateend,
                                'supportid': supportid,
                                'authorid': authorid,
                                'paceid': paceid,
                                'catSupport': catSupport,
                                'editOrientation': editOrientation,
                                'periodReview': periodReview,
                                'formatEdit': formatEdit,
                                'languageEdit': languageEdit,
                                'criteria': criteria,
                                'title': impactTitle,
                                'nature': nature,
                                'language': language,
                                'print': print,
                                'pubValue': pubValue,
                                'appear': appear,
                                'period_tag_imp': periodTag
                            },
                            error: function (e) {
                                //console.log("error >>>", e);

                            },
                            success: function (res) {
                                console.log("success >>>", res);
                                //console.log($.fn.dataTable.isDataTable( '#review-dt' ));

                                if(res.length >0){
                                    //$('#impacts-json').val(JSON.stringify(res));
                                    $('#print-impacts-form').show();

                                    var arrayids = [];

                                    $.each(res,function(index,item){
                                        //console.log(item);
                                        arrayids[index] = item.impctid;
                                    })

                                    //console.log(arrayids.toString());

                                    $('#impact-array-id').val(arrayids.toString());
                                    $('#impact-emails-id').val(arrayids.toString());

                                }else {
                                    // empty impact
                                    console.log("empty impact")
                                    $('#print-impacts-form').hide();
                                    $('#review-press-empty-search').show();

                                }

                                if($.fn.dataTable.isDataTable( '#review-dt' )){
                                    reviewdatatableReview= $('#review-dt').DataTable();
                                    reviewdatatableReview.destroy();
                                }
                                

                                    var tbodyReview = $("#review-tbody");
                                    //console.log(res);

                                    // buttons select all / select none
                                    reviewdatatableReview = $('#review-dt').DataTable({
                                            "aaData": res,
                                            'select': true,
                                            'dom': 'Bfrtip',
                                            'buttons': [
                                                {
                                                    'text': 'Select all',
                                                    'action': function (e, dt, node, config ) {
                                                        //console.log("select all");
                                                        //console.log(dt);
                                                        dt.rows().select();
                                                        ////console.log($("td.select-checkbox input.selectedCheckbox"));
                                                        //$("td.select-checkbox input.selectedCheckbox").prop('checked', true);
                                                        ////console.log(dt.column(0).nodes());
                                                        dt.column(0).nodes().to$().each(function(index) {  
                                                            //console.log($(this));  
                                                                $(this).find(">:first-child").prop('checked', true);
                                                        }); 
                                                    }
                                                },
                                                {
                                                    'text': 'Select none',
                                                    'action': function (e, dt, node, config ) {
                                                        //console.log("select none")
                                                        dt.rows().deselect();
                                                        dt.column(0).nodes().to$().each(function(index) {  
                                                            //console.log($(this));  
                                                                $(this).find(">:first-child").prop("checked",false);
                                                        }); 
                                                    }
                                                }
                                            ],
                                            select: {
                                                style: 'multi',
                                                selector: 'td.select-checkbox input.selectedCheckbox'
                                            },
                                            "columns": [
                                                {
                                                    "data":"id",
                                                    'targets': 0,
                                                    "className": 'select-checkbox',
                                                    "visible": true,
                                                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                                        //console.log("data ",oData);
                                                        $(nTd).html("<input type='checkbox' class='selectedCheckbox' value="+oData.impctid+" />");
                                                    }
                                                },
                                                {
                                                    "data":"id",
                                                    "visible": false
                                                },
                                                {"data":"support_name"},
                                                { "data": "title_imp" ,
                                                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {

                                                        var routeImpactDetail ='{{ URL::to("admin/impacts-detail/") }}/'+oData.impctid;
                                                        //console.log(routeImpactDetail);
                                                        
                                                        $(nTd).html("<a href="+routeImpactDetail+">"+oData.title_imp+"</a>");
                                                    }
                                                },
                                                {"data":"date_imp",
                                                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {

                                                        var formattedDate = new Date(oData.date_imp);
                                                        console.log("formattedDate "+formattedDate);
                                                        var d = formattedDate.getDate();
                                                        var m =  formattedDate.getMonth();
                                                        m += 1;  // JavaScript months are 0-11
                                                        var y = formattedDate.getFullYear();
                                                        if (d < 10) {
                                                            d = "0" + d;
                                                        }
                                                        if (m < 10) {
                                                            m = "0" + m;
                                                        }

                                                        console.log("hour "+formattedDate.getHours());

                                                        $(nTd).html(d + "-" + m + "-" + y);
                                                    }
                                                },
                                                {"data":"date_integ_imp",
                                                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {

                                                        var formattedDate = new Date(oData.date_integ_imp);
                                                        console.log("formattedDate "+formattedDate);
                                                        var d = formattedDate.getDate();
                                                        var m =  formattedDate.getMonth();
                                                        m += 1;  // JavaScript months are 0-11
                                                        var y = formattedDate.getFullYear();
                                                        if (d < 10) {
                                                            d = "0" + d;
                                                        }
                                                        if (m < 10) {
                                                            m = "0" + m;
                                                        }

                                                        console.log("hour "+formattedDate.getHours());

                                                        $(nTd).html(d + "-" + m + "-" + y+" "+formattedDate.getHours()+":"+formattedDate.getMinutes());
                                                    }
                                                },
                                                {"data":"name",
                                                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                                        console.log("sData >>> "+oData.name);
                                                        $(nTd).html(oData.name);
                                                    }
                                                },
                                                {"data":"date_imp",
                                                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {

                                                        var urlprintPost = '{{ URL::to("admin/print/impact") }}';
                                                        var certificatForm = $('input[name="_token"]').attr('value');
                                                        var routeImpactDetail = '<form method="POST" action="'+urlprintPost+'" class="form-horizontal" style="float: right;"><input name="_token" type="hidden" value="'+certificatForm+'">';
                                                        routeImpactDetail +='<input name="impactid" type="hidden" value="'+oData.impctid+'">';
                                                        routeImpactDetail +='<input class="btn btn-xs btn-danger" type="submit" value="PDF" formtarget="_blank"></form>';
                                                        ////console.log(routeImpactDetail);

                                                        $(nTd).html(routeImpactDetail);
                                                    }
                                                },
                                            ]
                                        })

                                        // reviewdatatableReview.on("click", "th.select-checkbox", function() {
                                        //     if ($("th.select-checkbox").hasClass("selected")) {
                                        //         reviewdatatableReview.rows().deselect();
                                        //         $("th.select-checkbox").removeClass("selected");
                                        //     } else {
                                        //         reviewdatatableReview.rows().select();
                                        //         $("th.select-checkbox").addClass("selected");
                                        //     }
                                        // }).on("select deselect", function() {
                                        //     ("Some selection or deselection going on")
                                        //     if (reviewdatatableReview.rows({
                                        //             selected: true
                                        //         }).count() !== reviewdatatableReview.rows().count()) {
                                        //         $("th.select-checkbox").removeClass("selected");
                                        //     } else {
                                        //         $("th.select-checkbox").addClass("selected");
                                        //     }
                                        // });

                                        ////console.log(reviewdatatableReview.columns());

                                        $('#review-dt').show();

                                    

                            }
                        });
            }

            $("#mailModalBtn").click(function(){
                //console.log("click");
                $('#mailModal').modal('show');
            })
            $("#mailPrevBtn").click(function(){
                //console.log("click prev");
                var checkedInputDT = $('.select-checkbox input[type=checkbox]:checked');
                var arrayids = [];
                reviewdatatableReview.column(0).nodes().to$().each(function(index) {
                    ////console.log($(this).find(">:first-child").is(':checked'));
                    if($(this).find(">:first-child").is(':checked')){
                        arrayids[index] = $(this).find(">:first-child").attr('value');
                        $('#impact-emails-id').val(arrayids.toString()); 
                        $('#impact-array-id').val(arrayids.toString()); 
                        ////console.log(arrayids);
                    }
                })
                generateMailsImpact(arrayids.toString());
                $('#prevMailModal').modal('show');
            })
            $("#fileModelBtn").click(function(){
                console.log("file model show")
                generatePdfList();
                $('#pdfListModal').modal('show');
            });
            // return view mail
            function generateMailsImpact(ids){
                //console.log(ids);
                generatePdfListSelect();
                //loading 
                $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                            },
                            url: '{{URL::to("ajax/getimpacts")}}',
                            type: 'POST',
                            datatType: 'json',
                            data: {
                                impactId: ids
                            },
                            error: function (e) {
                                //console.log("error >>>", e);
                            },
                            success: function (res) {
                                ////console.log("generateMailsImpact",res);
                                $("#prevMailModal-body").html(res);
                            }
                });
            }
            // get list PDF file
            function generatePdfList(){
                $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                            },
                            url: '{{URL::to("ajax/getPdfList")}}',
                            type: 'GET',
                            error: function (e) {
                                //console.log("error >>>", e);
                            },
                            success: function (res) {
                                console.log("generatePdfList",res);
                                $("#fileListModal-body").html(res);
                            }
                });
            }
            // generate select list pdf
            function generatePdfListSelect(){
                $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                            },
                            url: '{{URL::to("ajax/getPdfListSelect")}}',
                            type: 'GET',
                            error: function (e) {
                                //console.log("error >>>", e);
                            },
                            success: function (res) {
                                console.log("generatePdfList",res);
                                var sel = $("<select id='select-file'>")
                                sel.append($("<option >").attr('value','').text(''));
                                $.each(res,function(index,item){
                                        console.log(item);
                                        sel.append($("<option >").attr('value',item).text(item));
                                })
                                $("#select-files-container").html(sel);
                                $('#select-file').on("change", function(){
                                    console.log($(this).val());
                                    $('#pdf-name-impact').val($(this).val());
                                });
                                //console.log("select ",sel);
                            }
                });
            }
});
</script>

@endsection