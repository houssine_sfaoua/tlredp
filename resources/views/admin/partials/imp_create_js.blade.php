<script type="text/javascript">
    $(document).ready(function() {
        //$(document).ready(function () {

            var aClientsGlobal = {!! json_encode(\App\ClientCompany::pluck('nom_cl','id')) !!};
            
            //console.log(aClientsGlobal);
            $('.check_class').bind('click',function() {
                $('.check_class').not(this).prop("checked", false);
            });

            $('#wrapper-author').hide();
            $('#wrapper-rubrique').hide();

            $('#wrapper-format').hide();
            $('div.appear-wrapper').hide();
            $('div.appear-select-imp').hide();

            // date
            $("#date-impact").datepicker({
                dateFormat: "dd-mm-yy"
            });

            $('#date-integ-impact').datepicker({
                dateFormat: "dd-mm-yy"
            });

            /*$("#date-integ-impact").datepicker({
                dateFormat: "dd-mm-yy"
            });*/
            

            $("#author-select").append($("<option>").attr('value', 'autre').text("Autre"));
            $("#rubrique-select").append($("<option>").attr('value', 'autre').text("Autre"));
            //$("#format-select").append($("<option>").attr('value', 'autre').text("Autre"));

            /********************************Select Record*************************************************************/
            $("div.record-scan-container").hide();
            $("#type-impact").change(function(){
                console.log($(this).val());
                if ($(this).val() == "audiovisuel"){
                    $("div.record-scan-container").show();
                } else {
                    $("div.record-scan-container").hide();
                }
            })

            /**********************************************************************************************/
            

                /********************************Select Appear****************************************/

                $("#appear-select").append($("<option>").attr('value', 'autre').text("Autre"));

                    $('#appear-select').chosen().change(function () {
                        //console.log("appear change");
        
                        if ($(this).val() != null) {
        
                            if ($(this).val() == "autre") {
                                //console.log("autre");
                                //$('#wrapper-format').show();.
                                $('div.appear-wrapper').show();
                            } else {
                                //$('#wrapper-format').hide();
                                $('div.appear-wrapper').hide();
                            }
        
                        } else {
        
                            //console.log("value null");
                        }
                    });


            /********************************Select client****************************************/

            var count = 0;
            $("#add-new-client").click(function(event)
            {
                
                event.preventDefault();
                count++;

                //console.log("count add ",count);
                
                ////console.log($(this));
                ////console.log($('div.client-add-wrapper'));

                var selectArray = ["client_imp[]","criteria_imp[]","subcriteria_imp[]"];

                var htmlWrap = "<div style='clear:both;'></div>";
                    htmlWrap+="<div class='client-add-wrapper-"+count+"'><label for='client_imp' class='col-sm-2 control-label'>Client</label><div class='col-sm-4'>";

                    htmlWrap+="<select class='form-control' id='client-select-"+count+"' name='client_imp[]'>";

                    var optionsCl = "<option selected='selected'>Choisir un client</option>";
                    $.each(aClientsGlobal,function(index,element){
                        //console.log(element);

                        optionsCl+="<option value="+index+">"+element+"</option>";
                    });

                    ////console.log(optionsCl);

                    htmlWrap+= optionsCl;
                    htmlWrap+= "</select></div>";


                    htmlWrap+="<div class='col-sm-3'>";
                    htmlWrap+="<select class='form-control' id='criteria-select-"+count+"' name='criteria_imp[]'></select></div>";

                    htmlWrap+="<div class='col-sm-3'>";
                    htmlWrap+="<select class='form-control' id='subcriteria-select-"+count+"' name='subcriteria_imp[]'></select></div>";

                    htmlWrap+="<button id='delete-client-wrapper' class='btn btn-xs btn-danger client-add-wrapper-"+count+"'>delete</button>";

                    ////console.log(htmlWrap);

                    $('div.client-imp-wrapper').append(htmlWrap);


                    $('#criteria-select-'+count).chosen().change(function(){
                        //console.log("criteria select ++",$(this));

                        // id item ==> last item array ; count
                        var aIdItem = $(this).attr("id").split("-");

                        var indexHtml = aIdItem[2];
                        ////console.log(aIdItem[2]);

                        if ($(this).val() != null) {

                            var criteriaid = $(this).val();

                            getsubcriteriabycriteriaid(indexHtml,criteriaid);
                        
                        }
                    });

                    $('#subcriteria-select-'+count).chosen().change(function(){
                        //console.log("subcriteria select ++",$(this));
                    });

                    $('#client-select-'+count).chosen().change(function(){
                        //console.log("client select ++",$(this).attr("id").split("-"));

                        // id item ==> last item array ; count
                        var aIdItem = $(this).attr("id").split("-");

                        var indexHtml = aIdItem[2];
                        //console.log(aIdItem[2]);

                        if ($(this).val() != null) {

                            var clientid = $(this).val();
                            getCriteriaByClientid(indexHtml,clientid);
                        }
                        
                    });

                    $('button.client-add-wrapper-'+count).click(function(event){
                        event.preventDefault();
                        //console.log("delete item",$(this).parent('div'));

                        $(this).parent('div').remove();

                        if(count>0){
                            count--;
                        }

                        //console.log("count remove ",count);
                    });
            })

            $("#subcriteria-select-0").chosen().change(function(){
                //console.log("#subcriteria-select-0",$(this));
            });

            $("#criteria-select-0").chosen().change(function(){
                //console.log($(this).val());

                if ($(this).val() != null) {

                        var criteriaid = $(this).val();

                        getsubcriteriabycriteriaid(0,criteriaid);
                        
                }
            });

            
            

            $("#client-select-0").chosen().change(function () {
                //console.log("change function ");

                //console.log($(this).val());

                if ($(this).val() != null) {

                        var clientid = $(this).val();

                        getCriteriaByClientid(0,clientid);

                }

            });

            function getsubcriteriabycriteriaid(indexHtml,criteriaid){

                $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                            },
                            url: '{{URL::to("ajax/getsubcriteriabycriteriaid")}}',
                            type: 'POST',
                            datatType: 'json',
                            data: {
                                'criteriaid': criteriaid
                            },
                            error: function (e) {
                                //console.log("error >>>", e);

                            },
                            success: function (res) {
                                //console.log("success >>>", res);

                                $("#subcriteria-select-"+indexHtml).empty();
                                //console.log($("#subcriteria-select-"+indexHtml));

                                if (res.length > 0) {
                                    $.each(res, function (index, item) {
                                        //console.log(item);
                                        
                                        $("#subcriteria-select-"+indexHtml).append($("<option>").attr('value', item.id).text(item.label_subcr));
                                    });
                                }
                                //$("#criteria-select").chosen();
                                $("#subcriteria-select-"+indexHtml).trigger("chosen:updated").change();
                            }
                        });

            }


            // index html ==> index item added
            function getCriteriaByClientid(indexHtml, clientid){

                                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                            },
                            url: '{{URL::to("ajax/getcriteriabyclientid")}}',
                            type: 'POST',
                            datatType: 'json',
                            data: {
                                'clientid': clientid
                            },
                            error: function (e) {
                                //console.log("error >>>", e);

                            },
                            success: function (res) {
                                //console.log("success >>>", res);

                                $("#criteria-select-"+indexHtml).empty();
                                //console.log($("#criteria-select-"+indexHtml));

                                if (res.length > 0) {
                                    $.each(res, function (index, item) {
                                        ////console.log(item);
                                        
                                        $("#criteria-select-"+indexHtml).append($("<option>").attr('value', item.id).text(item.label_cri));
                                    });
                                }
                                //$("#criteria-select").chosen();
                                
                                $("#criteria-select-"+indexHtml).trigger("chosen:updated").change();
                            }
                        });

            }



            /************************************************************************************/


            $("#rubrique-select").chosen().change(function () {
                //console.log("change rubrique");

                if ($(this).val() != null) {

                    if ($(this).val() == "autre") {
                        //console.log("autre");
                        $('#wrapper-rubrique').show();

                    } else {
                        $('#wrapper-rubrique').hide();
                    }

                } else {

                    //console.log("value null");
                }
            })

            // get all author
            getAllAuthor();

            getAllRubrique();


            $('#author-select').chosen().change(function () {
                //console.log("author change");

                if ($(this).val() != null) {

                    if ($(this).val() == "autre") {
                        //console.log("autre");
                        $('#wrapper-author').show();
                        add_button.show();
                    } else {
                        $('#wrapper-author').hide();
                        add_button.hide();
                    }

                } else {

                    //console.log("value null");
                }

            });

            $('#format-select').chosen().change(function () {
                //console.log("format change");

                if ($(this).val() != null) {

                    if ($(this).val() == "autre") {
                        //console.log("autre");
                        $('#wrapper-format').show();
                    } else {
                        $('#wrapper-format').hide();
                        ////console.log($("#print"));
                        ////console.log($("input:radio[name='print']:checked").val());
                        if ($(this).val() != null && $(this).val() != "")
                            getFormatById($(this).val(), $("input:radio[name='print']:checked").val());
                    }

                } else {

                    //console.log("value null");
                }

            });
            /**********************************ADD NEW FORMAT***************************** */

            var max_fields = 20; //maximum input boxes allowed
            var wrapper_format = $("#wrapper-format"); //Fields wrapper
            /*var add_button_format = $(".sub_add_format_button");*/ //Add button ID


            /*$(add_button_format).click(function (e) { //on add input button click
                e.preventDefault();
                //console.log("click button");

                $(wrapper_format).append('<div class="form-group">{!! Form::label("new_format_lbl", trans("quickadmin::admin.impact-create-author"), ["class"=>"col-sm-2 control-label"]) !!}<div class="col-sm-8">{!! Form::text("new_format_lbl[]", "", ["class"=>"form-control","placeholder"=> trans("quickadmin::admin.impact-create-author")]) !!}</div><button class="remove_field col-sm-2">delete</button></div>'); //add input box

            });*/

            $(wrapper_format).on("click", ".remove_field", function (e) { //user click on remove field
                e.preventDefault();
                $(this).parent('div').remove();
            })
            /*********************************************************************** */



            /**********************************ADD NEW RUBRIQUE***************************** */

            var max_fields = 20; //maximum input boxes allowed
            var wrapper_rub = $("#wrapper-rubrique"); //Fields wrapper
            var add_button_rub = $(".sub_add_rub_button"); //Add button ID


            $(add_button_rub).click(function (e) { //on add input button click
                e.preventDefault();
                //console.log("click button");

                $(wrapper_rub).append('<div class="form-group">{!! Form::label("new_rub_lbl", trans("quickadmin::admin.impact-create-author"), ["class"=>"col-sm-2 control-label"]) !!}<div class="col-sm-8">{!! Form::text("new_rub_lbl[]", "", ["class"=>"form-control","placeholder"=> trans("quickadmin::admin.impact-create-author")]) !!}</div><button class="remove_field col-sm-2">delete</button></div>'); //add input box

            });

            $(wrapper_rub).on("click", ".remove_field", function (e) { //user click on remove field
                e.preventDefault();
                $(this).parent('div').remove();
            })
            /*********************************************************************** */

            /**********************************ADD NEW AUTHOR***************************** */

            var max_fields = 20; //maximum input boxes allowed
            var wrapper = $("#wrapper-author"); //Fields wrapper
            var add_button = $("#sub-add-author"); //Add button ID
            add_button.hide();

            $(add_button).click(function (e) { //on add input button click
                e.preventDefault();
                //console.log("click button");

                $(wrapper).append('<div class="form-group">{!! Form::label("new_author_lbl", trans("quickadmin::admin.impact-create-author"), ["class"=>"col-sm-2 control-label"]) !!}<div class="col-sm-8">{!! Form::text("new_author_lbl[]", "", ["class"=>"form-control","placeholder"=> trans("quickadmin::admin.impact-create-author")]) !!}</div><button class="remove_field col-sm-2">delete</button></div>'); //add input box

            });

            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove field
                e.preventDefault();
                $(this).parent('div').remove();
            })
            /*********************************************************************** */

            function getAllRubrique() {

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                    },
                    url: '{{URL::to("ajax/getallrubrique")}}',
                    type: 'GET',
                    error: function (e) {
                        //console.log("error >>>", e);

                    },
                    success: function (res) {
                        //console.log("success >>>", res);

                        if (res.length > 0) {
                            $.each(res, function (index, item) {
                                //console.log(item);
                                $("#rubrique-select").append($("<option>").attr('value', item.id).text(item.nom_rub));
                            });
                        }
                        $('#rubrique-select').trigger("chosen:updated");
                        //$('').chosen();
                    }
                });

            }

            function getAllAuthor() {

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                    },
                    url: '{{URL::to("ajax/getallauthor")}}',
                    type: 'GET',
                    error: function (e) {
                        //console.log("error >>>", e);

                    },
                    success: function (res) {
                        //console.log("success >>>", res);

                        if (res.length > 0) {
                            $.each(res, function (index, item) {
                                ////console.log(item);
                                $("#author-select").append($("<option>").attr('value', item.id).text(item.name_aut));
                            });
                        }
                        $('#author-select').trigger("chosen:updated");
                        //$('').chosen();
                    }
                });
            }


            /********************CROP IAMGE*******************/

            var camanContainer = $("#caman-container");
            camanContainer.hide();

            var copperContainer = $("#copper-container");
            copperContainer.show();

            var imageData = null;

            var caman = null;
            var filterglobal  = null;
            
            var brightnessglobal = null;
            var contrastglobal = null;

            var imageCaman = null;

            var countCropimg = 0;

            var logowrapperdiv = $("#logo-wrapper");

            $('#reset-logo-image').hide();
            

            $('#openFileDialog').click(function(){
                //console.log('openfiledialog');
                $('#scan-file').trigger('click');
            });
            
            
            $('#scan-file').change(function (ev) {
                //var tmppath = null;
                ////console.log(event.target.files[0]);
                ////console.log(this.files);

                var tmppath = URL.createObjectURL(event.target.files[0]);
                ////console.log(tmppath);

                $('#crop-image').attr('src', tmppath);

                $('#fileModal').modal('show');

            });

            function startCropper() {

                // Destroy if already initialized
                if ($('#crop-image').data('cropper')) {
                    $('#crop-image').cropper('destroy');
                }

                // Initialize a new cropper
                $('#crop-image').cropper({
                    autoCropArea: 0.5,
                    minContainerHeight: 250,
                    minContainerWidth: 250,
                    crop: function (e) {
                        ////console.log(e);
                    },
                    built: function () {
                        //console.log("built");
                        $('#crop-image').cropper("setCropBoxData", { width: "100", height: "50" });
                    }
                });
            }

            

            function startCaman(url) {
                caman = Caman('#caman-img', function () {                  
                    this.newLayer(function () {
                        filterglobal = this.filter;
                        $('#filter-brightness-val').text(0);
                        $('#filter-contrast-val').text(0);
                        
                        $("#contrast-range").val(0);
                        $("#brightness-range").val(0);

                        filterglobal.brightness(0);
                        filterglobal.contrast(0);

                        //console.log("filter new layer");
                        caman.render();
                });
                //console.log("outside ",this);
           });

            }


            $('#contrast-range').on('change',function(){
                    //console.log($(this).val());
                     caman.revert();

                    contrastglobal = $(this).val();

                    $('#filter-contrast-val').text(contrastglobal);
                    console.log(contrastglobal);
                    console.log(brightnessglobal);
                    if(brightnessglobal!=null) filterglobal.brightness(parseInt(brightnessglobal));
                    if(contrastglobal!=null) filterglobal.contrast(parseInt(contrastglobal));

                    caman.render(function(){
                        imageCaman = null;
                        imageCaman = this.toBase64();
                        ////console.log("image render ",image);
                    });
                
            });

            $('#brightness-range').on('change',function(){
                //console.log($(this).val());
                caman.revert();
                ////console.log(caman);
                 brightnessglobal= $(this).val();

                 $('#filter-brightness-val').text(brightnessglobal);
                 //filterglobal.contrast(contrastglobal);
                 console.log(contrastglobal);
                console.log(brightnessglobal);
                if(brightnessglobal!=null) filterglobal.brightness(parseInt(brightnessglobal));
                if(contrastglobal!=null) filterglobal.contrast(parseInt(contrastglobal));
                
                caman.render(function(){
                    imageCaman = null;
                    imageCaman = this.toBase64();
                    ////console.log("image render ",image);
                });
            });


            var cropBoxData;
            var canvasData;
           $('#fileModal').on('shown.bs.modal', function () {
                //console.log("modal shown");

                $("#crop-save").hide();
                $("#crop-next").show();

                $('.modal .modal-body').css('overflow-y', 'auto'); 
                $('.modal .modal-body').css('max-height', $(window).height() * 0.7);

                $('#crop-image').cropper({
                    autoCropArea: 0.5,
                    crop: function(e) {
                        ////console.log(e);
                    }
                });

            }).on('hidden.bs.modal', function () {
                //console.log("modal hidden");

            });


            // reset image data ==> remove input value image
            $('#reset-logo-image').click(function(event){

				event.preventDefault();

                // logo container count
                countCropimg= 0;

                copperContainer.show();
                camanContainer.hide();

				$("#scan-file").wrap('<form>').closest('form').get(0).reset();
                $("#scan-file").unwrap();

                //console.log($('.logo-container'));

                $('.logo-container').remove();

                //console.log("count logo",countCropimg);

				$('#crop-image').cropper('destroy');

                $('#reset-logo-image').hide();
			});

            /*$('#scan-file').change(function (ev) {

                var tmppath = URL.createObjectURL(event.target.files[0]);
                //console.log(tmppath);
                $('#crop-image').attr('src', tmppath);
                //console.log($('#fileModal'));
                $('#fileModal').modal('show');

            });*/

            $("#close-modal").click(function () {
                // reset input file
                $("#scan-file").wrap('<form>').closest('form').get(0).reset();
                $("#scan-file").unwrap();

                $('#fileModal').modal('hide');

                copperContainer.show();
                camanContainer.hide();

                $('#caman-img').remove();

                $('#crop-image').cropper('destroy');
            });

            $("#crop-save").click(function(){

                var htmlcontent = "<div class='col-sm-10 col-sm-offset-2 logo-container logo-container-"+countCropimg+"'><img src='#' style='display:none;width: 17%;' id='img-logo-client-"+countCropimg+"'>";
                htmlcontent+= "<input type='text' id='position-"+countCropimg+"' name='position[]' style='display:none;' value='"+(countCropimg+1)+"' />"    
                htmlcontent+= "<input type='text' id='logo-data-"+countCropimg+"' name='logo[]' style='display:none;' />"
	            htmlcontent+= "</div>";

                //console.log(htmlcontent);

                $(".logo-wrapper").append(htmlcontent);

                ////console.log("caman image",imageCaman);

                if(imageCaman !=null){
                }else{
                    imageCaman= imageData;
                }

                ////console.log("image caman",imageCaman);
                

                $("#scan-file").wrap('<form>').closest('form').get(0).reset();
                $("#scan-file").unwrap();

                $("#img-logo-client-"+countCropimg).css('display','block');
                $("#position-"+countCropimg).css('display','block');
				$("#img-logo-client-"+countCropimg).attr('src', imageCaman);
				$("#logo-data-"+countCropimg).attr('value',imageCaman);

                copperContainer.show();
                camanContainer.hide();

                // reset image caman
                imageCaman = null;
                ////console.log("image data",imageData);

                //$("#logo-data-"+count).attr('value', );

                $('#caman-img').remove();

                $('#crop-image').cropper('destroy');
                $('#reset-logo-image').show();

                // logo container count
                countCropimg++;
                ////console.log("count logo",countCropimg);

                $('#fileModal').modal('hide');
            });


            $("#crop-next").click(function () {
                //console.log("click save btn");

                //console.log($('#crop-image'));

                $("#crop-save").show();
                $("#crop-next").hide();

                imageData = null;

                imageData = $('#crop-image').cropper('getCroppedCanvas').toDataURL("image/png");

                ////console.log("crop image ",imageData);

                $("#caman-container").prepend('<img id="caman-img" data-caman="resize({width: 100, height: 100})"></img>');

                copperContainer.hide();
                camanContainer.show();
                
                //$("#img-logo-client").css('display', 'block');
                $("#caman-img").attr('src', imageData);
                //$("#logo-data").attr('value', imageData);

                 startCaman(imageData);

                $('#crop-image').cropper('destroy');

            });


            $('#support-impt').chosen().change(function (event) {
                //console.log("support chosen change");

                if ($(this).val() != null) {

                    if (event.target == this) {
                        //console.log($(this).val());

                        if ($(this).val() == "autre") {

                            $('#wrapper-author').show();
                            /*add_button.show();*/

                        } else {
                            getSupportById($(this).val());
                        }
                    }
                } else {

                    //console.log("value null");
                }
            });


            // on change radio button print
            $('input.print-radio').on('change', function () {
                //console.log($(this).val());

                //console.log($('#format-select').val());
                getFormatById($('#format-select').val(), $(this).val());
            });

            // on change appear
            $('input.appear-radio').on('change', function () {
                //console.log("appear radio ",$(this).val());

                if ($(this).val() == '1') {
                    //$('div.appear-wrapper').show();
                    $("div.appear-select-imp").show();
                    $("#appear_select_chosen").css('width','623px');
                    
                } else {
                    //$('div.appear-wrapper').hide();
                    $("div.appear-select-imp").hide();
                    $("#appear_select_chosen").css('display:none;');
                }

                ////console.log($('#format-select').val());
                //getFormatById($('#format-select').val(),$(this).val());
            });



            /********************************GET DATA FORMAT BY ID****************************** */

            function getFormatById(formatId, print) {
                //console.log(print);
                //console.log(formatId);


                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                    },
                    url: '{{URL::to("ajax/getformatbyid")}}',
                    type: 'POST',
                    datatType: 'json',
                    data: {
                        'formatid': formatId,
                        'print': print
                    },
                    success: function (data) {
                        //console.log(data);
                        $('#pub_value').val("");

                        ////console.log($('input[value=couleur]'));

                        if (typeof data != "undefined" && data.length > 0) {

                            $('#pub_value').val(data[0].value_ad_frm);

                            if(data[0].print_frm == "couleur"){
                                $('input[value=couleur]').prop("checked", true);
                                $('input[value=noirblanc]').prop("checked", false);
                            }else{
                                $('input[value=noirblanc]').prop("checked", true);
                                $('input[value=couleur]').prop("checked", false);
                            };

                        } else {

                            //console.log("empty array format");
                        }
                    }
                })


            }
            /********************************************************************************** */

            function getSupportById(supportid) {


                //console.log(supportid);

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                    },
                    url: '{{URL::to("ajax/getsupportbyid")}}',
                    type: 'POST',
                    datatType: 'json',
                    data: {
                        'supportid': supportid
                    },
                    success: function (data) {
                        console.log('get support ',data);

                        //$('#cat_sup').val("Presse Papier");

                        //$("#format-select").empty().append($("<option>").attr('value', 'autre').text("Autre"));

                            if(typeof data != "undefined"){
                                if(data.cat_sup == "Presse électronique"){
                                    //console.log("cat electronique")
                                    $('.num-press').css('display','none');
                                    $('.link-press').css('display','block');
    
                                }else{
                                    //console.log("cat presse papier")
                                    $('.link-press').css('display','none');
                                    $('.num-press').css('display','block');
                                }
                            }

                        if (typeof data != "undefined" && typeof data.formatItems != "undefined") {
                            /*$('#cat_sup').val(data.cat_sup);
                            $('#orient_sup').val(data.edit_orie_sup);
                            $('#perd_sup').val(data.period_sup);
                            $('#type_sup').val(data.type_edit_sup);
                            $('#lng_sup').val(data.lng_edit_sup);
                            $('#posit_sup').val(data.postition_sup);*/

                            console.log(data.formatItems);

                            

                            $.each(data.formatItems, function (index, item) {
                                console.log(item);
                                $("#format-select").append($("<option>").attr('value', item.id).text(item.pseudo_frm));
                            });

                            //$('#format-select').val();
							$('#format-select').trigger("chosen:updated").change();

                        } else {

                            //console.log("empty array sub criteria");

                            //$("#format-select").empty().append($("<option>").attr('value', 'autre').text("Autre"));
                            $('#format-select').trigger("chosen:updated").change();

                        }
                    }
                })
            }

            /************************************************/

        //});
    });
</script>
