{{-- {!! dd($impact) !!} --}}
<script type="text/javascript">
        $(document).ready(function () {

            $('.check_class').bind('click',function() {
                $('.check_class').not(this).prop("checked", false);
            });

            var impacts = {!! json_encode($impact) !!};
            var aClientsGlobal = {!! json_encode(\App\ClientCompany::pluck('nom_cl','id')) !!};

    
            var urlStoragedir = '{!! env("APP_URL").'/storage/app/public/uploads/impacts' !!}';

            //console.log("url storage",urlStoragedir);

            //console.log("impact ",impacts);


            $('#impact-id').val(impacts.id);
            

            //console.log("impact edits");

            $('#wrapper-author').hide();
            $('#wrapper-rubrique').hide();

            $('#wrapper-format').hide();
            $('div.appear-wrapper').hide();

            // date
            $("#date-impact").datepicker({
                dateFormat: "dd-mm-yy"
            });
            
            $('#date-integ-impact').datetimepicker({
                dateFormat: "dd-mm-yy",
                timeFormat:  "hh:mm:ss"
            });

            var formattedDate = new Date(impacts.date_imp);
            var d = formattedDate.getDate();
            var m =  formattedDate.getMonth();
            m += 1;  // JavaScript months are 0-11
            var y = formattedDate.getFullYear();

            $("#date-impact").datepicker('setDate', d + "-" + m + "-" + y);

            //date integration
            var formattedDateInteg = new Date(impacts.date_integ_imp);
            var dInteg = formattedDateInteg.getDate();
            var mInteg =  formattedDateInteg.getMonth();
            mInteg += 1;  // JavaScript months are 0-11
            var yInteg = formattedDateInteg.getFullYear();

            $("#date-integ-impact").datepicker('setDate', dInteg + "-" + mInteg + "-" + yInteg);

            $('#pace-id').val(impacts.pace_imp);

            $("#num_edit").val(impacts.num_edit_imp);
            $("#num_page").val(impacts.num_page_imp);
            /********************************Select Record*************************************************************/
            $("div.record-scan-container").hide();
            ////console.log("type imap",impacts);
            if (impacts.type_imp != null) {
                $("div.record-scan-container").show();
            } else {
                $("div.record-scan-container").hide();
            }
            $("#type-impact").change(function(){
                //console.log($(this).val());
                if ($(this).val() == "audiovisuel"){
                    $("div.record-scan-container").show();
                } else {
                    $("div.record-scan-container").hide();
                }
            })
            /*********************************************************************************/

            $("#author-select").append($("<option>").attr('value', 'autre').text("Autre"));
            $("#rubrique-select").append($("<option>").attr('value', 'autre').text("Autre"));
            $("#format-select").append($("<option>").attr('value', 'autre').text("Autre"));

                /********************************Select Appear****************************************/

                $("#appear-select").append($("<option>").attr('value', 'autre').text("Autre"));

                        $('#appear-select').chosen().change(function () {
                            //console.log("appear change");
            
                            if ($(this).val() != null) {
            
                                if ($(this).val() == "autre") {
                                    //console.log("autre");
                                    //$('#wrapper-format').show();.
                                    $('div.appear-wrapper').show();
                                } else {
                                    //$('#wrapper-format').hide();
                                    $('div.appear-wrapper').hide();
                                }
            
                            } else {
            
                                //console.log("value null");
                            }
                        });
    
/************************************************************************/

            $("#rubrique-select").chosen().change(function () {
                //console.log("change rubrique");

                if ($(this).val() != null) {

                    if ($(this).val() == "autre") {
                        //console.log("autre");
                        $('#wrapper-rubrique').show();
                        add_button_rub.show();
                    } else {
                        $('#wrapper-rubrique').hide();
                        add_button_rub.hide();
                    }

                } else {

                    //console.log("value null");
                }
            })

            // get all author
            getAllAuthor();

            getAllRubrique();


            $('#author-select').chosen().change(function () {
                //console.log("author change");

                if ($(this).val() != null) {

                    if ($(this).val() == "autre") {
                        //console.log("autre");
                        $('#wrapper-author').show();
                        add_button.show();
                    } else {
                        $('#wrapper-author').hide();
                        add_button.hide();
                    }

                } else {

                    //console.log("value null");
                }

            });

            



            /**********************************ADD NEW FORMAT***************************** */

            var max_fields = 20; //maximum input boxes allowed
            var wrapper_format = $("#wrapper-format"); //Fields wrapper
            var add_button_format = $("#sub-add-format-button"); //Add button ID
            //console.log("selector format ",add_button_format);
            add_button_format.hide();

            $(add_button_format).click(function (e) { //on add input button click
                e.preventDefault();
                //console.log("click button");

                $(wrapper_format).append('<div class="form-group">{!! Form::label("new_format_lbl", trans("quickadmin::admin.impact-create-author"), ["class"=>"col-sm-2 control-label"]) !!}<div class="col-sm-8">{!! Form::text("new_format_lbl[]", "", ["class"=>"form-control","placeholder"=> trans("quickadmin::admin.impact-create-author")]) !!}</div><button class="remove_field col-sm-2">delete</button></div>'); //add input box

            });

            $(wrapper_format).on("click", ".remove_field", function (e) { //user click on remove field
                e.preventDefault();
                $(this).parent('div').remove();
            })
            /*********************************************************************** */

            $('#format-select').chosen().change(function () {
                //console.log("format change");

                if ($(this).val() != null) {

                    if ($(this).val() == "autre") {
                        //console.log("autre");
                        $('#wrapper-format').show();
                        add_button_format.show();
                    } else {
                        $('#wrapper-format').hide();
                        //console.log(add_button_format);
                        add_button_format.hide();
                        ////console.log($("#print"));
                        ////console.log($("input:radio[name='print']:checked").val());
                        if($(this).val()!=null && $(this).val()!="")
                        getFormatById($(this).val(),$("input:radio[name='print']:checked").val());
                    }

                } else {

                    //console.log("value null");
                }

            });

            



            /**********************************ADD NEW RUBRIQUE***************************** */

            var max_fields = 20; //maximum input boxes allowed
            var wrapper_rub = $("#wrapper-rubrique"); //Fields wrapper
            var add_button_rub = $("#sub-add-rub-button"); //Add button ID
            add_button_rub.hide();

            $(add_button_rub).click(function (e) { //on add input button click
                e.preventDefault();
                //console.log("click button");

                $(wrapper_rub).append('<div class="form-group">{!! Form::label("new_rub_lbl", trans("quickadmin::admin.impact-create-author"), ["class"=>"col-sm-2 control-label"]) !!}<div class="col-sm-8">{!! Form::text("new_rub_lbl[]", "", ["class"=>"form-control","placeholder"=> trans("quickadmin::admin.impact-create-author")]) !!}</div><button class="remove_field col-sm-2">delete</button></div>'); //add input box

            });

            $(wrapper_rub).on("click", ".remove_field", function (e) { //user click on remove field
                e.preventDefault();
                $(this).parent('div').remove();
            })
            /*********************************************************************** */

            /**********************************ADD NEW AUTHOR***************************** */

            var max_fields = 20; //maximum input boxes allowed
            var wrapper = $("#wrapper-author"); //Fields wrapper
            var add_button = $("#sub-add-author"); //Add button ID
            add_button.hide();


            $(add_button).click(function (e) { //on add input button click
                e.preventDefault();
                //console.log("click button");

                $(wrapper).append('<div class="form-group">{!! Form::label("new_author_lbl", trans("quickadmin::admin.impact-create-author"), ["class"=>"col-sm-2 control-label"]) !!}<div class="col-sm-8">{!! Form::text("new_author_lbl[]", "", ["class"=>"form-control","placeholder"=> trans("quickadmin::admin.impact-create-author")]) !!}</div><button class="remove_field col-sm-2">delete</button></div>'); //add input box

            });

            $(wrapper).on("click", ".remove_field", function (e) { //user click on remove field
                e.preventDefault();
                $(this).parent('div').remove();
            })
            /*********************************************************************** */

            function getAllRubrique() {

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                    },
                    url: '{{URL::to("ajax/getallrubrique")}}',
                    type: 'GET',
                    error: function (e) {
                        //console.log("error >>>", e);

                    },
                    success: function (res) {
                        //console.log("success >>>", res);

                        if (res.length > 0) {
                            $.each(res, function (index, item) {
                                //console.log(item);
                                $("#rubrique-select").append($("<option>").attr('value', item.id).text(item.nom_rub));
                            });
                        }

                        $('#rubrique-select').val(impacts.rubid_imp);
                        $('#rubrique-select').trigger("chosen:updated");
                        //$('').chosen();
                    }
                });

            }

            function getAllAuthor() {

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                    },
                    url: '{{URL::to("ajax/getallauthor")}}',
                    type: 'GET',
                    error: function (e) {
                        //console.log("error >>>", e);

                    },
                    success: function (res) {
                        //console.log("success >>>", res);

                        if (res.length > 0) {
                            $.each(res, function (index, item) {
                                //console.log(item);
                                $("#author-select").append($("<option>").attr('value', item.id).text(item.name_aut));
                            });
                        }

                        // edit value
                        $('#author-select').val(impacts.authorid_imp);
                        $('#author-select').trigger("chosen:updated");
                        //$('').chosen();
                    }
                });
            }


            /********************CROP IAMGE*******************/

            var countCropimg = 0;
            // show image
            if(impacts.img != "" && impacts.img != null){

                var imgArray = JSON.parse(impacts.img);

                countCropimg = imgArray.length;
                //console.log("count image array "+countCropimg);

                

                $.each(imgArray,function(index,element){
                     //console.log("image",element);
                    var htmlcontent = "<div class='col-sm-10 col-sm-offset-2 logo-container logo-container-"+index+"'><a class='pop'><img src='"+urlStoragedir+"/"+impacts.id+"/"+element.scan+"' style='width: 17%;' id='img-logo-client-"+index+"'></a>";
                    htmlcontent+= "<input type='text' id='position-"+index+"' name='position[]' style='' value='"+(element.position)+"' />"    
                    htmlcontent+= "<input type='text' id='logo-data-"+index+"' name='logo[]' style='display:none;' />"
                    htmlcontent+= "<a id='deletescan-"+index+"' data-last-value='"+element.position+"' data-index-item='"+index+"' class='remove-scan'>X</a>";
	                htmlcontent+= "</div>";

                    var text = "a.deletescan-"+index;
                    console.log("logo data ",$($(text)))
                    console.log("url dir ",urlStoragedir+"/"+impacts.id+"/"+element.scan);

                    $(".logo-wrapper").append(htmlcontent);

                    toDataUrl(urlStoragedir+"/"+impacts.id+"/"+element.scan,function(myBase64){
                        console.log("image base 64 >>>",index);
                        $("#logo-data-"+index).attr('value',myBase64);
                    });
                });

                ////console.log(imgArray);
                $( "a.remove-scan").each(function(index) {
                    ////console.log("remove scan");
                    ////console.log($(this));
                    $(this).on("click", function(event){
                        event.preventDefault();
                        //console.log($(this))
                        //console.log("lastvalue ",$(this).data("lastValue"));
                        //console.log("index item ",$(this).data("indexItem"));
                        var positionScan = $(this).data("lastValue");
                        var indexItemScan = $(this).data("indexItem");
                        $("#removed-scan").append("<input type='text' id='removed-id-"+indexItemScan+"' name='ids[]' />")
                        $("#removed-id-"+indexItemScan).attr('value',positionScan);
                        $(this).parents('.logo-container').remove();
                    });
                });
            }


            

            function toDataUrl(url, callback) {
                var xhr = new XMLHttpRequest();
                xhr.onload = function() {
                    var reader = new FileReader();
                    reader.onloadend = function() {
                        callback(reader.result);
                    }
                    reader.readAsDataURL(xhr.response);
                };
                xhr.open('GET', url);
                xhr.responseType = 'blob';
                xhr.send();
            }

            var camanContainer = $("#caman-container");
            camanContainer.hide();

            var copperContainer = $("#copper-container");
            copperContainer.show();

            var imageData = null;

            var caman = null;
            var filterglobal  = null;
            
            var brightnessglobal = null;
            var contrastglobal = null;

            var imageCaman = null;

            
            var logowrapperdiv = $("#logo-wrapper");


            $('#openFileDialog').click(function(){
                //console.log('openfiledialog');
                $('#scan-file').trigger('click');
            })

            $('#scan-file').change(function (ev) {

                var tmppath = URL.createObjectURL(event.target.files[0]);
                //console.log(tmppath);
                $('#crop-image').attr('src', tmppath);
                $('#fileModal').modal('show');

            });


            function startCaman(url) {
                caman = Caman('#caman-img', function () {

                    this.newLayer(function () {
                        //console.log("filter new layer",this.filter);

                        filterglobal = this.filter;
                        //$('#filter-contrast-val').text(0);
                        $('#filter-brightness-val').text(0);

                        //$("#contrast-range").val(0);
                        $("#brightness-range").val(0);

                        filterglobal.brightness(0);
                        //filterglobal.contrast(0);

                        caman.render();
                        
                });
            });
        }
        
        /*$('#contrast-range').on('change',function(){
                //console.log($(this).val());
                contrastglobal = $(this).val();

                $('#filter-contrast-val').text(contrastglobal);
                caman.revert();
                //console.log(caman);
                
                filterglobal.brightness(brightnessglobal);
                filterglobal.contrast(contrastglobal);
                caman.render(function(){
                    imageCaman = null;

                    imageCaman = this.toBase64();
                    ////console.log("image render ",image);
                });
                
            });*/

            $('#brightness-range').on('change',function(){
                //console.log($(this).val());
                caman.revert();
                ////console.log(caman);
                 brightnessglobal= $(this).val();

                 $('#filter-brightness-val').text(brightnessglobal);
                 //filterglobal.contrast(contrastglobal);
                filterglobal.brightness(brightnessglobal);
                caman.render(function(){
                    imageCaman = null;
                    imageCaman = this.toBase64();
                    ////console.log("image render ",image);
                });
            });

            var cropBoxData;
            var canvasData;
            $('#fileModal').on('shown.bs.modal', function () {
                //console.log("modal shown");

                $("#crop-save").hide();
                $("#crop-next").show();

                $('.modal .modal-body').css('overflow-y', 'auto'); 
                $('.modal .modal-body').css('max-height', $(window).height() * 0.7);

                $('#crop-image').cropper({
                    autoCropArea: 0.5,
                    crop: function(e) {
                        ////console.log(e);
                    }

                });

            }).on('hidden.bs.modal', function () {
                //console.log("modal hidden");

                /*var imageData = $('#crop-image').cropper('getCroppedCanvas').toDataURL("image/png");
                //var croppng = cropcanvas.toDataURL("image/png");
    
                //console.log(imageData);
    
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                    },
                    url: '{{URL::to("admin/impact/getFile")}}',
                    data: {
                        pngimageData: imageData,
                        filename: 'test.png'
                    },
                    success: function(output) {
                        //console.log(output);
    
                    }
                })
    
                $("#test-crop").attr('src', imageData);
    
                $('#crop-image').cropper('destroy');*/

            });

            $('#scan-file').change(function (ev) {

                var tmppath = URL.createObjectURL(event.target.files[0]);
                //console.log(tmppath);
                $('#crop-image').attr('src', tmppath);
                //console.log($('#fileModal'));
                $('#fileModal').modal('show');

            });

            // reset image data ==> remove input value image
            $('#reset-logo-image').click(function(event){

                event.preventDefault();

                if (confirm('Voulez vous effacer toutes les photos?')) {

                    // logo container count
                    countCropimg= 0;

                    copperContainer.show();
                    camanContainer.hide();

                    $("#scan-file").wrap('<form>').closest('form').get(0).reset();
                    $("#scan-file").unwrap();

                    //console.log($('.logo-container'));

                    $('.logo-container').remove();

                    //console.log("count logo",countCropimg);

                    $('#crop-image').cropper('destroy');
                    
                } else {
                    
                }

			});

            $("#close-modal").click(function () {
                $("#scan-file").wrap('<form>').closest('form').get(0).reset();
                $("#scan-file").unwrap();

                $('#fileModal').modal('hide');

                copperContainer.show();
                camanContainer.hide();

                $('#caman-img').remove();

                $('#crop-image').cropper('destroy');
            });


            
            $("#crop-next").click(function () {
                //console.log("click save btn");

                //console.log($('#crop-image'));

                $("#crop-save").show();
                $("#crop-next").hide();

                imageData = null;

                imageData = $('#crop-image').cropper('getCroppedCanvas').toDataURL("image/png");

                ////console.log("crop image ",imageData);

                $("#caman-container").prepend('<img id="caman-img" ></img>');

                copperContainer.hide();
                camanContainer.show();
                
                //$("#img-logo-client").css('display', 'block');
                $("#caman-img").attr('src', imageData);
                //$("#logo-data").attr('value', imageData);

                 startCaman(imageData);

                $('#crop-image').cropper('destroy');

            });


            $("#crop-save").click(function () {

                //console.log("count on save ",countCropimg);
                
                var htmlcontent = "<div class='col-sm-10 col-sm-offset-2 logo-container logo-container-"+countCropimg+"'><a class='pop'><img src='#' style='width: 17%;' id='img-logo-client-"+countCropimg+"'></a>";
                htmlcontent+= "<input type='text' id='position-"+countCropimg+"' name='position[]' style='' value='"+(countCropimg+1)+"' />"    
                htmlcontent+= "<input type='text' id='logo-data-"+countCropimg+"' name='logo[]' style='display:none;' />"
	            htmlcontent+= "</div>";

                //console.log(htmlcontent);

                $(".logo-wrapper").append(htmlcontent);

               if(imageCaman !=null){
                }else{
                    imageCaman= imageData;
                }
                

                $("#scan-file").wrap('<form>').closest('form').get(0).reset();
                $("#scan-file").unwrap();

                $("#img-logo-client-"+countCropimg).css('display','block');
                $("#position-"+countCropimg).css('display','block');
				$("#img-logo-client-"+countCropimg).attr('src', imageCaman);
				$("#logo-data-"+countCropimg).attr('value',imageCaman);

                copperContainer.show();
                camanContainer.hide();

                // reset image caman
                imageCaman = null;

                $('#caman-img').remove();

                $('#crop-image').cropper('destroy');

                // logo container count
                countCropimg++;
                //console.log("count logo",countCropimg);

                $('#fileModal').modal('hide');


            });


            $('#support-impt').chosen().change(function (event) {
                //console.log("chosen change support");

                if ($(this).val() != null) {

                    if (event.target == this) {
                        //console.log($(this).val());

                        if ($(this).val() == "autre") {

                            $('#wrapper-author').show();

                        } else {

                            getSupportById($(this).val());

                        }

                    }

                } else {

                    //console.log("value null");
                }

            });

            /********************************SHOW IMAGE****************************************/
            $('.pop').click(function (e) { //on add input button click
                //e.preventDefault();
                //console.log($(this).find('img').attr('src'));
                $('.imagepreview').attr('src', $(this).find('img').attr('src'));
                //console.log($('#imageModal'));
                $('#imageModal').modal('show');
		    });
            /**********************************************************************************/

            // edit support / trigger change
            $('#support-impt').val(impacts.supportid_imp);
            $('#support-impt').trigger("chosen:updated").change();


            // on change radio button print
            $('input.print-radio').on('change', function() {
                //console.log($(this).val());

                //console.log($('#format-select').val());
                getFormatById($('#format-select').val(),$(this).val());
            });

            $('#oui-appear').on('change',function(){
                //console.log("oui appear");
                //console.log($(this).val());

                $('#oui-appear').attr("checked",true);
                $('#non-appear').attr("checked",false);

                //$('div.appear-wrapper').show();
                $("div.appear-select-imp").show();
                $("#appear_select_chosen").css('width','623px');

                //$('#appear-input').val(impacts.appear_txt_imp);

                $('#appear-select').val(impacts.appearid_imp);
                $('#appear-select').trigger("chosen:updated").change();
                
            });

            $('#non-appear').on('change',function(){
                //console.log("non appear");
                //console.log($(this).val());

                $('#oui-appear').attr("checked",false);
                $('#non-appear').attr("checked",true);

                //$('div.appear-wrapper').hide();

                $("div.appear-select-imp").hide();
                $("#appear_select_chosen").css('display:none;');
                
            });

            // on change appear
            /*$('input.appear-radio').on('change', function() {
                //console.log($(this).val());

                if($(this).val() == '1'){
                    $('div.appear-wrapper').show();
                }else{
                    $('div.appear-wrapper').hide();
                }

                ////console.log($('#format-select').val());
                //getFormatById($('#format-select').val(),$(this).val());
            });*/
            
           //console.log((impacts.appear_imp==1));
            
            //$('input.appear-radio').prop('checked',true);
            //$('input.appear-radio').trigger("change");

            if(impacts.appear_imp == 1){
                $('#oui-appear').trigger("change");
            }else{
                $('#non-appear').trigger("change");
            }
            

            /********************************GET DATA FORMAT BY ID****************************** */

            function getFormatById(formatId,print){
                //console.log(print);
                //console.log(formatId);


                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                    },
                    url: '{{URL::to("ajax/getformatbyid")}}',
                    type: 'POST',
                    datatType: 'json',
                    data: {
                        'formatid': formatId,
                        'print' : print
                    },
                    success: function (data) {
                        //console.log(data);
                        $('#pub_value').val("");

                        if (typeof data != "undefined" && data.length>0) {
                        
                            $('#pub_value').val(data[0].value_ad_frm);

                            if(data[0].print_frm == "couleur"){
                                $('input[value=couleur]').prop("checked", true);
                                $('input[value=noirblanc]').prop("checked", false);
                            }else{
                                $('input[value=noirblanc]').prop("checked", true);
                                $('input[value=couleur]').prop("checked", false);
                            };

                        } else {

                            //console.log("empty array format");
                        }
                    }
                })


            }
            /********************************************************************************** */

            function getSupportById(supportid) {


                //$('#support-impt').chosen()

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                    },
                    url: '{{URL::to("ajax/getsupportbyid")}}',
                    type: 'POST',
                    datatType: 'json',
                    data: {
                        'supportid': supportid
                    },
                    success: function (data) {
                        //console.log(data);

                        //$('#cat_sup').val("Presse Papier");

                        if (typeof data != "undefined") {
                            /*$('#cat_sup').val(data.cat_sup);
                            $('#orient_sup').val(data.edit_orie_sup);
                            $('#perd_sup').val(data.period_sup);
                            $('#type_sup').val(data.type_edit_sup);
                            $('#lng_sup').val(data.lng_edit_sup);
                            $('#posit_sup').val(data.postition_sup);*/

                            if(typeof data != "undefined"){
                                if(data.cat_sup == "Presse électronique"){
                                    //console.log("cat electronique")
                                    $('.num-press').css('display','none');
                                    $('.link-press').css('display','block');
    
                                }else{
                                    //console.log("cat presse papier")
                                    $('.link-press').css('display','none');
                                    $('.num-press').css('display','block');
                                }
                            }

                            $("#format-select").empty().append($("<option>").attr('value', 'autre').text("Autre"));

                            //console.log(data.formatItems);
                            if(typeof data.formatItems != "undefined"){
                                $.each(data.formatItems, function (index, item) {
                                    //console.log(item);
                                    $("#format-select").append($("<option>").attr('value', item.id).text(item.nom_frm));
                                });

                                //console.log("impacts funct",impacts);
                                $("#format-select").val(impacts.formatid_imp);
                            }else{
                                $("#format-select").empty().append($("<option>").attr('value', 'autre').text("Autre"));
                            }
                            
							$('#format-select').trigger("chosen:updated").change();

                        } else {

                            //console.log("empty array sub criteria");
                        }
                    }
                })
            }

            /************************************************/

            
            /********************************Select client****************************************/

            //console.log("impact client >> ",impacts.impctclient);
            var count = 0;

            // populate client select and sub
            $.each(impacts.impctclient,function(index,item){

                 var htmlWrap = "<div style='clear:both;'></div>";

                    count++;
                    //console.log("count edit>>",count);

                    htmlWrap+="<div class='client-add-wrapper-"+index+"'><label for='client_imp' class='col-sm-2 control-label'>Client</label><div class='col-sm-4'>";

                    htmlWrap+="<select class='form-control' id='client-select-"+index+"' name='client_imp[]'>";

                    var optionsCl = "<option selected='selected'>Choisir un client</option>";
                    $.each(aClientsGlobal,function(index,element){
                        //console.log(element);

                        optionsCl+="<option value="+index+">"+element+"</option>";
                    });

                    ////console.log(optionsCl);

                    htmlWrap+= optionsCl;
                    htmlWrap+= "</select></div>";


                    htmlWrap+="<div class='col-sm-3'>";
                    htmlWrap+="<select class='form-control' id='criteria-select-"+index+"' name='criteria_imp[]'></select></div>";

                    htmlWrap+="<div class='col-sm-3'>";
                    htmlWrap+="<select class='form-control' id='subcriteria-select-"+index+"' name='subcriteria_imp[]'></select></div>";

                    // un client au min
                    //console.log(index > 0);
                    if(index>0){
                        htmlWrap+="<div class='col-sm-2'><button id='delete-client-wrapper' class='btn btn-xs btn-danger client-delete-wrapper-"+index+"'>delete</button></div>";
                    }

                    $('div.client-imp-wrapper').append(htmlWrap);

                    $('#subcriteria-select-'+index).chosen().change(function(){
                        //console.log("subcriteria select ++",$(this));
                    });

                    $('#criteria-select-'+index).chosen().change(function(){
                        //console.log("criteria select ++",$(this).val());

                        // id item ==> last item array ; count
                        var aIdItem = $(this).attr("id").split("-");

                        var indexHtml = aIdItem[2];
                        ////console.log(aIdItem[2]);

                        if ($(this).val() != null) {

                            var criteriaid = $(this).val();

                            getsubcriteriabycriteriaid(indexHtml,criteriaid,item.subcriteriaid_imp_cl);
                        
                        }
                    });

                    //console.log("criteria id ",item.criteriaid_imp_cl);

                   

                

                    $('#client-select-'+index).chosen().change(function(){
                        //console.log("client select ++",$(this).attr("id").split("-"));

                        // id item ==> last item array ; count
                        var aIdItem = $(this).attr("id").split("-");

                        var indexHtml = aIdItem[2];
                        //console.log(aIdItem[2]);

                        if ($(this).val() != null) {

                            var clientid = $(this).val();
                            getCriteriaByClientid(indexHtml,clientid,item.criteriaid_imp_cl);
                        }
                        
                    });

                    $('#client-select-'+index).val(item.clientid_imp_cl);
                    $('#client-select-'+index).trigger("chosen:updated").change();

                    $('button.client-delete-wrapper-'+index).click(function(event){
                        event.preventDefault();
                        //console.log($('div.client-add-wrapper-'+index));
                        //console.log("delete item",$('div.client-add-wrapper-'+index));

                        $('div.client-add-wrapper-'+index).remove();

                        if(count>0){
                            count--;
                        }

                        //console.log("count remove ",count);
                    });
            })

            





            
            $("#add-new-client").click(function(event)
            {
                
                event.preventDefault();
                count++;

                //console.log("count add ",count);
                
                ////console.log($(this));
                ////console.log($('div.client-add-wrapper'));

                var selectArray = ["client_imp[]","criteria_imp[]","subcriteria_imp[]"];

                var htmlWrap = "<div style='clear:both;'></div>";
                    htmlWrap+="<div class='client-add-wrapper-"+count+"'><label for='client_imp' class='col-sm-2 control-label'>Client</label><div class='col-sm-4'>";

                    htmlWrap+="<select class='form-control' id='client-select-"+count+"' name='client_imp[]'>";

                    var optionsCl = "<option selected='selected'>Choisir un client</option>";
                    $.each(aClientsGlobal,function(index,element){
                        //console.log(element);

                        optionsCl+="<option value="+index+">"+element+"</option>";
                    });

                    ////console.log(optionsCl);

                    htmlWrap+= optionsCl;
                    htmlWrap+= "</select></div>";


                    htmlWrap+="<div class='col-sm-3'>";
                    htmlWrap+="<select class='form-control' id='criteria-select-"+count+"' name='criteria_imp[]'></select></div>";

                    htmlWrap+="<div class='col-sm-3'>";
                    htmlWrap+="<select class='form-control' id='subcriteria-select-"+count+"' name='subcriteria_imp[]'></select></div>";

                    htmlWrap+="<button id='delete-client-wrapper' class='btn btn-xs btn-danger client-delete-wrapper-"+count+"'>delete</button>";

                    ////console.log(htmlWrap);

                    $('div.client-imp-wrapper').append(htmlWrap);


                    $('#criteria-select-'+count).chosen().change(function(){
                        //console.log("criteria select ++",$(this));

                        // id item ==> last item array ; count
                        var aIdItem = $(this).attr("id").split("-");

                        var indexHtml = aIdItem[2];
                        ////console.log(aIdItem[2]);

                        if ($(this).val() != null) {

                            var criteriaid = $(this).val();

                            getsubcriteriabycriteriaid(indexHtml,criteriaid);
                        
                        }
                    });

                    $('#subcriteria-select-'+count).chosen().change(function(){
                        //console.log("subcriteria select ++",$(this));
                    });

                    $('#client-select-'+count).chosen().change(function(){
                        //console.log("client select ++",$(this).attr("id").split("-"));

                        // id item ==> last item array ; count
                        var aIdItem = $(this).attr("id").split("-");

                        var indexHtml = aIdItem[2];
                        //console.log(aIdItem[2]);

                        if ($(this).val() != null) {

                            var clientid = $(this).val();
                            getCriteriaByClientid(indexHtml,clientid);
                        }
                        
                    });

                    $('button.client-delete-wrapper-'+count).click(function(event){
                        event.preventDefault();
                        //console.log("delete item",$(this).parent('div'));

                        $(this).parent('div').remove();

                        if(count>0){
                            count--;
                        }

                        //console.log("count remove ",count);
                    });
            })


            function getsubcriteriabycriteriaid(indexHtml,criteriaid,subcriteriaimp){
                //console.log("sub criteria id imp>>> ",subcriteriaimp);

                $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                            },
                            url: '{{URL::to("ajax/getsubcriteriabycriteriaid")}}',
                            type: 'POST',
                            datatType: 'json',
                            data: {
                                'criteriaid': criteriaid
                            },
                            error: function (e) {
                                //console.log("error >>>", e);

                            },
                            success: function (res) {
                                //console.log("success get sub >>>", res);

                                $("#subcriteria-select-"+indexHtml).empty();
                                //console.log($("#subcriteria-select-"+indexHtml));

                                if (res.length > 0) {
                                    $.each(res, function (index, item) {
                                        //console.log(item);
                                        
                                        $("#subcriteria-select-"+indexHtml).append($("<option>").attr('value', item.id).text(item.label_subcr));
                                    });
                                }
                                
                                $('#subcriteria-select-'+indexHtml).val(subcriteriaimp);
                                $("#subcriteria-select-"+indexHtml).trigger("chosen:updated").change();
                            }
                        });

            }


            // index html ==> index item added
            function getCriteriaByClientid(indexHtml, clientid,criteriaidimp){
                //console.log(criteriaidimp);

                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                            },
                            url: '{{URL::to("ajax/getcriteriabyclientid")}}',
                            type: 'POST',
                            datatType: 'json',
                            data: {
                                'clientid': clientid
                            },
                            error: function (e) {
                                //console.log("error >>>", e);

                            },
                            success: function (res) {
                                //console.log("success >>>", res);

                                $("#criteria-select-"+indexHtml).empty();
                                //console.log($("#criteria-select-"+indexHtml));

                                if (res.length > 0) {
                                    $.each(res, function (index, item) {
                                        ////console.log(item);
                                        
                                        $("#criteria-select-"+indexHtml).append($("<option>").attr('value', item.id).text(item.label_cri));
                                    });
                                }
                                //$("#criteria-select").chosen();
                                $('#criteria-select-'+indexHtml).val(criteriaidimp);
                                $("#criteria-select-"+indexHtml).trigger("chosen:updated").change();
                            }
                        });

            }


            // trigger select client




            /************************************************************************************/

        });
</script>
