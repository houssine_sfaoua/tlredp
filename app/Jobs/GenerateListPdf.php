<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Cronjob;
use App\ImpactClient;
use App\ClientCompany;
use App\Impact;
use File;
use PDF;

class GenerateListPdf implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $cronjob = Cronjob::whereNull('completed_at')->get();
        $impactIds = array();
        if(isset($cronjob[0]->data)){
            $impactIds=explode(",", $cronjob[0]->data);
        }
        //dd($impactIds);
        if (!empty($impactIds)) {
            $aImpactArray = array();
            foreach ($impactIds as $key => $impactid) {
                if (isset($impactid) && $impactid != null && !is_null($impactid)){
                    $aImpact = Impact::find($impactid);
                    $clientImpact = ImpactClient::whereImpactid_imp_cl($aImpact->id)->first();
                    $aImpact->authorobj = $aImpact->author;
                    $aImpact->supportobj = $aImpact->support;
                    $aImpact->rubriqueobj = $aImpact->rubrique;
                    $aImpact->formatobj = $aImpact->format;
                    $aImpact->clientCampany = ClientCompany::find($clientImpact->clientid_imp_cl);
                    $aImpact->images = json_decode($aImpact->scan_imp);
                    array_push($aImpactArray,$aImpact);
                }
            }
            //dd($aImpactArray);
            $pdf = PDF::loadView('admin.pdf.impactspdf',compact('aImpactArray'))->save('pdfnameTest.pdf');
            $cronjob[0]->completed_at = date('Y-m-d H:i:s');
            $cronjob[0]->save();
        }
    }
}
