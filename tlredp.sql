-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 01 juin 2018 à 14:23
-- Version du serveur :  5.7.21
-- Version de PHP :  7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tlredp`
--

-- --------------------------------------------------------

--
-- Structure de la table `appears`
--

DROP TABLE IF EXISTS `appears`;
CREATE TABLE IF NOT EXISTS `appears` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label_appe` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `appears`
--

INSERT INTO `appears` (`id`, `label_appe`) VALUES
(8, 'PArution akhbar lyawm'),
(7, 'parution abc24'),
(6, 'Parution update'),
(5, 'parution');

-- --------------------------------------------------------

--
-- Structure de la table `authors`
--

DROP TABLE IF EXISTS `authors`;
CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_aut` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `authors`
--

INSERT INTO `authors` (`id`, `name_aut`) VALUES
(1, 'houssine');

-- --------------------------------------------------------

--
-- Structure de la table `client_companies`
--

DROP TABLE IF EXISTS `client_companies`;
CREATE TABLE IF NOT EXISTS `client_companies` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom_cl` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email_cl` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `tel_cl` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo_cl` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `client_companies`
--

INSERT INTO `client_companies` (`id`, `nom_cl`, `email_cl`, `tel_cl`, `logo_cl`) VALUES
(1, 'societe', 'houssine@test.com', NULL, '37_LOGO SAZ AVEC LOGO GROUPE CDG.png');

-- --------------------------------------------------------

--
-- Structure de la table `client_criterias`
--

DROP TABLE IF EXISTS `client_criterias`;
CREATE TABLE IF NOT EXISTS `client_criterias` (
  `criteria_id` int(10) UNSIGNED DEFAULT NULL,
  `client_id` int(10) UNSIGNED DEFAULT NULL,
  KEY `client_criterias_criteria_id_foreign` (`criteria_id`),
  KEY `client_criterias_client_id_foreign` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `client_criterias`
--

INSERT INTO `client_criterias` (`criteria_id`, `client_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `client_subcriterias`
--

DROP TABLE IF EXISTS `client_subcriterias`;
CREATE TABLE IF NOT EXISTS `client_subcriterias` (
  `subcriteria_id` int(10) UNSIGNED DEFAULT NULL,
  `client_id` int(10) UNSIGNED DEFAULT NULL,
  KEY `client_subcriterias_subcriteria_id_foreign` (`subcriteria_id`),
  KEY `client_subcriterias_client_id_foreign` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `client_subcriterias`
--

INSERT INTO `client_subcriterias` (`subcriteria_id`, `client_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `criterias`
--

DROP TABLE IF EXISTS `criterias`;
CREATE TABLE IF NOT EXISTS `criterias` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label_cri` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `criterias`
--

INSERT INTO `criterias` (`id`, `label_cri`) VALUES
(1, 'Aménagement territorial et urbanisme');

-- --------------------------------------------------------

--
-- Structure de la table `formats`
--

DROP TABLE IF EXISTS `formats`;
CREATE TABLE IF NOT EXISTS `formats` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom_frm` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `print_frm` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value_ad_frm` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `formats`
--

INSERT INTO `formats` (`id`, `nom_frm`, `print_frm`, `value_ad_frm`) VALUES
(1, 'format 2m', 'couleur', '34'),
(2, 'format 2m', 'couleur', '34'),
(3, 'format jdida', 'couleur', '98'),
(4, 'format JDIDA', 'couleur', '34'),
(5, 'format hespress', 'couleur', '98'),
(6, 'Format abc24', 'couleur', '67'),
(7, 'FORMAT FORMAT', 'noirblanc', '98'),
(8, 'agora format', 'noirblanc', '98');

-- --------------------------------------------------------

--
-- Structure de la table `impacts`
--

DROP TABLE IF EXISTS `impacts`;
CREATE TABLE IF NOT EXISTS `impacts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title_imp` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `language_imp` enum('ar','fr','en') COLLATE utf8_unicode_ci NOT NULL,
  `resume_imp` longtext COLLATE utf8_unicode_ci,
  `scan_imp` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_imp` date NOT NULL,
  `date_integ_imp` date NOT NULL,
  `selection_imp` int(11) NOT NULL,
  `authorid_imp` int(10) UNSIGNED NOT NULL,
  `pace_imp` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `supportid_imp` int(10) UNSIGNED NOT NULL,
  `num_edit_imp` int(11) DEFAULT NULL,
  `num_page_imp` int(11) DEFAULT NULL,
  `link_imp` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rubid_imp` int(10) UNSIGNED NOT NULL,
  `formatid_imp` int(10) UNSIGNED NOT NULL,
  `appear_imp` int(11) NOT NULL,
  `appearid_imp` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `impacts_authorid_imp_foreign` (`authorid_imp`),
  KEY `impacts_supportid_imp_foreign` (`supportid_imp`),
  KEY `impacts_rubid_imp_foreign` (`rubid_imp`),
  KEY `impacts_formatid_imp_foreign` (`formatid_imp`),
  KEY `impacts_appearid_imp_foreign` (`appearid_imp`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `impacts`
--

INSERT INTO `impacts` (`id`, `title_imp`, `language_imp`, `resume_imp`, `scan_imp`, `date_imp`, `date_integ_imp`, `selection_imp`, `authorid_imp`, `pace_imp`, `supportid_imp`, `num_edit_imp`, `num_page_imp`, `link_imp`, `rubid_imp`, `formatid_imp`, `appear_imp`, `appearid_imp`) VALUES
(1, 'Iam retombé update', 'ar', NULL, '[{\"position\":\"1\",\"scan\":\"11527096102.png\"}]', '2018-05-23', '2018-05-23', 1, 1, 'positif', 41, 45, 45, 'https://www.hespress.com/', 1, 7, 1, 6),
(2, 'شمال إستعمل يتم أي. لمّ عن دارت الأرض. بحق بأيدي وبالتحديد', 'ar', NULL, '[{\"position\":\"1\",\"scan\":\"11527420679.png\"}]', '2018-05-27', '2018-05-27', 1, 1, 'positif', 12, NULL, NULL, 'http://abc24.ma/', 1, 6, 1, 8),
(3, 'Iam retombé update', 'ar', NULL, '[{\"position\":\"1\",\"scan\":\"11527420752.png\"}]', '2018-05-27', '2018-05-27', 1, 1, 'positif', 33, 87, 3, 'https://www.agora.ma/', 1, 8, 1, 8);

-- --------------------------------------------------------

--
-- Structure de la table `impact_clients`
--

DROP TABLE IF EXISTS `impact_clients`;
CREATE TABLE IF NOT EXISTS `impact_clients` (
  `impactid_imp_cl` int(10) UNSIGNED NOT NULL,
  `clientid_imp_cl` int(10) UNSIGNED NOT NULL,
  `criteriaid_imp_cl` int(10) UNSIGNED NOT NULL,
  `subcriteriaid_imp_cl` int(10) UNSIGNED NOT NULL,
  KEY `impact_clients_impactid_imp_cl_foreign` (`impactid_imp_cl`),
  KEY `impact_clients_clientid_imp_cl_foreign` (`clientid_imp_cl`),
  KEY `impact_clients_criteriaid_imp_cl_foreign` (`criteriaid_imp_cl`),
  KEY `impact_clients_subcriteriaid_imp_cl_foreign` (`subcriteriaid_imp_cl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `impact_clients`
--

INSERT INTO `impact_clients` (`impactid_imp_cl`, `clientid_imp_cl`, `criteriaid_imp_cl`, `subcriteriaid_imp_cl`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 1),
(3, 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `position` int(11) DEFAULT NULL,
  `menu_type` int(11) NOT NULL DEFAULT '1',
  `icon` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `menu_role`
--

DROP TABLE IF EXISTS `menu_role`;
CREATE TABLE IF NOT EXISTS `menu_role` (
  `menu_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  UNIQUE KEY `menu_role_menu_id_role_id_unique` (`menu_id`,`role_id`),
  KEY `menu_role_menu_id_index` (`menu_id`),
  KEY `menu_role_role_id_index` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2015_10_10_000000_create_menus_table', 1),
(4, '2015_10_10_000000_create_roles_table', 1),
(5, '2015_10_10_000000_update_users_table', 1),
(6, '2015_12_11_000000_create_users_logs_table', 1),
(7, '2016_03_14_000000_update_menus_table', 1),
(8, '2018_01_12_230738_create_authors_table', 1),
(9, '2018_01_18_163901_create_supports_table', 1),
(10, '2018_01_18_170244_create_criterias_table', 1),
(11, '2018_01_18_170303_create_subcriterias_table', 1),
(21, '2018_01_20_235814_create_client_companies_table', 2),
(13, '2018_01_22_235223_create_client_criterias_table', 1),
(14, '2018_01_28_135854_create_client_subcriterias_table', 1),
(15, '2018_02_18_132108_create_rubriques_table', 1),
(16, '2018_02_18_134616_create_formats_table', 1),
(26, '2018_02_18_224505_create_impacts_table', 5),
(18, '2018_03_04_124603_create_impact_clients_table', 1),
(19, '2018_05_10_144246_create_support_formats_table', 1),
(23, '2018_05_20_165036_create_appears_table', 4);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `rubriques`
--

DROP TABLE IF EXISTS `rubriques`;
CREATE TABLE IF NOT EXISTS `rubriques` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom_rub` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `rubriques`
--

INSERT INTO `rubriques` (`id`, `nom_rub`) VALUES
(1, 'rubrique khona');

-- --------------------------------------------------------

--
-- Structure de la table `subcriterias`
--

DROP TABLE IF EXISTS `subcriterias`;
CREATE TABLE IF NOT EXISTS `subcriterias` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label_subcr` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `criteria_id_subcr` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `subcriterias_criteria_id_subcr_foreign` (`criteria_id_subcr`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `subcriterias`
--

INSERT INTO `subcriterias` (`id`, `label_subcr`, `criteria_id_subcr`) VALUES
(1, 'Victoria City / Garan', 1);

-- --------------------------------------------------------

--
-- Structure de la table `supports`
--

DROP TABLE IF EXISTS `supports`;
CREATE TABLE IF NOT EXISTS `supports` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_sup` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `origin_sup` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `cat_sup` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `edit_orie_sup` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `period_sup` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `form_edit_sup` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `lng_edit_sup` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `postition_sup` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `logo_sup` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=499 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `supports`
--

INSERT INTO `supports` (`id`, `name_sup`, `origin_sup`, `cat_sup`, `edit_orie_sup`, `period_sup`, `form_edit_sup`, `lng_edit_sup`, `postition_sup`, `logo_sup`) VALUES
(1, '04.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', 'logo-2016.jpg'),
(2, '1001infos.net', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(3, '24ala24.info', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(4, '24saa.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(5, '24sur24.news', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(6, '2m.ma', 'Presse nationale', 'Presse electronique', 'Presse officielle', 'En continu', 'En ligne', 'Bilingue', 'Presse officielle', '37_LOGO SAZ AVEC LOGO GROUPE CDG.png'),
(7, '3ayen.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(8, '60min.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(9, '90minutes.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(10, '9anat.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(11, 'Aabbir.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(12, 'Abc24.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(13, 'Achamal', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(14, 'Achamalpress.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(15, 'Acharq24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(16, 'Achnoo.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(17, 'Achpress.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(18, 'Achwa9e3.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(19, 'Actuelles.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(20, 'Actu-maroc.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(21, 'Adahira24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(22, 'Adare.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(23, 'Adawliya.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(24, 'AFRIMAG', 'Presse nationale', 'Presse Papier', 'Presse independante', '', 'Magazine', 'Francophone', 'Presse generaliste', NULL),
(25, 'Afrimag.net', 'Presse internationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'presse africaine', NULL),
(26, 'Afrique-presse.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(27, 'Agadir.news', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(28, 'Agadir1.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(29, 'Agadir24.info', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(30, 'Agadir360.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(31, 'Agadirinfo.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(32, 'Agenceafrique.com', 'Presse internationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(33, 'Agora.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(34, 'Agrimaroc.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse Agricole', NULL),
(35, 'Ahdath.info', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(36, 'Ahdatnews.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(37, 'Ahdatsouss.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(38, 'Ajil.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(39, 'Ajilpress.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(40, 'Akhbaralaan.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(41, 'AKHBAR AL YAOUM', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(42, 'Akhbaralfajr.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(43, 'Akhbarchamal.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(44, 'Akhbarlyoum.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(45, 'Akhbarmeknes24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(46, 'Akhbarona.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(47, 'Akhbarsouss.net', 'Presse nationale', 'Presse electronique', 'Presse independante', 'Quotodien', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(48, 'AKHIR SAA', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(49, 'AL AHDATH AL MAGHRIBIA', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(50, 'AL AKHBAR', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', '842ebca9ce4213e7de2950530ec0d15c-microsoft.png'),
(51, 'AL ALAM', 'Presse nationale', 'Presse Papier', 'Presse partisane', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(52, 'AL AMAL AL MAGHRIBIA', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(53, 'AL ANBAA AL MAGHRIBIA', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(54, 'AL AYAM', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(55, 'AL BAYANE', 'Presse nationale', 'Presse Papier', 'Presse partisane', 'Quotidien', 'Journal', 'Francophone', 'Presse generaliste', NULL),
(56, 'Al3omk.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(57, 'AL BILAD AL OUKHRA', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(58, 'Alaan.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(59, 'AL HADATH ACHARKI', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(60, 'AL HARAKA', 'Presse nationale', 'Presse Papier', 'Presse partisane', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(61, 'AL ISSLAH WA ATTANMIA', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(62, 'AL ITTIHAD AL ICHTIRAKI', 'Presse nationale', 'Presse Papier', 'Presse partisane', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(63, 'Alakhbaralmaghribia.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(64, 'Alakhbartv.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(65, 'Alalam.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(66, 'AL MASSAE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(67, 'Al Massar Assahafi', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(68, 'AL MICHAAL', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(69, 'Alanbaa.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(70, 'Alanbaapost.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(71, 'AL MOUNAATAF', 'Presse nationale', 'Presse Papier', 'Presse independante ', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(72, 'Alaoual.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(73, 'AL MOUNTADA', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(74, 'AL OUSBOUE ASSAHAFI', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(75, 'Alassima24.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(76, 'AL WASAT', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(77, 'AL WATAN AL ANE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(78, 'Alaw9at.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(79, 'Alayam24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(80, 'Albayane.press.ma', 'Presse nationale', 'Presse electronique', 'Presse partisane', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(81, 'Alboughaznews.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(82, 'Alhadath.net', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(83, 'ALAM RIYADI', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(84, 'Alhadath24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(85, 'Alhadet.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(86, 'Alhakika24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(87, 'Alhayatalyaoumia.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(88, 'Alhodhode.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(89, 'Alifyae.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(90, 'Al-intifada.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(91, 'Alittihad.info', 'Presse nationale', 'Presse electronique', 'Presse partisane', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(92, 'Aljadida24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(93, 'Aljalia24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(94, 'Aljamaa.net', 'Presse nationale', 'Presse electronique', 'Presse partisane', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(95, 'Aljanoub24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(96, 'Aljarida24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(97, 'Aljassour.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(98, 'Aljihatalmaghribia.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(99, 'Alkhabar.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(100, 'Alkhabar.press.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(101, 'Alkhabar.tv', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(102, 'Alkhabar24.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(103, 'Alkhabar24.net', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(104, 'Alkhabarpress.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(105, 'Almadina24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(106, 'Almaghreb24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(107, 'Almaghrib.alkhabar.info', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(108, 'Almaghribia.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(109, 'Almaghribia24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(110, 'Almaghribialyoum.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(111, 'Almaghribpress.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(112, 'Almaghribtoday.net', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(113, 'Almal.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse economique', NULL),
(114, 'Almarrakchia.net', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(115, 'Almasdare.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(116, 'Almoharir.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(117, 'Almolahid.net', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(118, 'Almouhitalfilahi.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse agricole', NULL),
(119, 'Almounaataf.com', 'Presse nationale', 'Presse electronique', 'Presse partisane', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(120, 'Almoustakil.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(121, 'Almouwatine.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(122, 'Alobor.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(123, 'Alousboue.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(124, 'Alraiy.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(125, 'Alsafiranews.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(126, 'Altpresse.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(127, 'Al-wajiha.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(128, 'Alwassait.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(129, 'Alwataniya24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(130, 'Alyaoum24.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(131, 'Amirati.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse feminine', NULL),
(132, 'Anahda.info', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(133, 'Anakamaghribia.blogspot.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(134, 'Analkhabar.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(135, 'Anawinek.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(136, 'ANDALUSPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Presse generaliste', NULL),
(137, 'ANFASPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(138, 'ANNAHAR AL MAGHRIBIA', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(139, 'ANNAHAR.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(140, 'ANNASS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(141, 'ANWARPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(142, 'ANZIPRESS.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(143, 'ARAB TRAVELINVEST.FR.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Presse generaliste', NULL),
(144, 'ARAI24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(145, 'ARAY.INFO', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(146, 'ARIFFINO.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(147, 'AROUIT24.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(148, 'ARRABITA.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(149, 'ARTICLE19.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(150, 'ARTPRESS.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(151, 'ASDAAALWATAN.INFO', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(152, 'ASDAAMAZAGAN.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(153, 'ASKISAHARA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(154, 'ASRARONA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(155, 'ASRARPRES.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(156, 'ASSABAH', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', 'sabahlogo.png'),
(157, 'Assabah.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(158, 'ASSABIL', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Bi-mensuel', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(159, 'ASSAFIR24.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(160, 'ASSAHRAA AL MAGHRIBIA', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(161, 'ASSAHRAA.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(162, 'ASSARAG24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(163, 'ASSDAE.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(164, 'ASWATCITY.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(165, 'ASWATNEWS.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(166, 'ATIGMEDIA.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(167, 'ATLAS24.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(168, 'ATLASSCOOP.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', '', '', '', 'Presse generaliste', NULL),
(169, 'ATTACMAROC.ORG', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(170, 'ATTAHARI.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(171, 'Aufemenin.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse feminine', NULL),
(172, 'AUJOURD\'HUI LE MAROC', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Francophone', 'Presse generaliste', NULL),
(173, 'AUJOURDHUI.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(174, 'AWASSIM.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(175, 'AZEMMOURINFO24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(176, 'AZILAL24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(177, 'AZILALALHORA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(178, 'AZILALNEWS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(179, 'AZILAL-ONLINE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(180, 'AZILAL-PRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(181, 'AZPRESSE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(182, 'BABERKANE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(183, 'BABNADOR.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(184, 'BABOUBI.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse satirique', NULL),
(185, 'BADIL.INFO', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(186, 'BADILPRESSE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(187, 'BARID.TV', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(188, 'BARLAMANE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(189, 'BASMAMAG.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(190, 'BAYANE AL YAOUME', 'Presse nationale', 'Presse Papier', 'Presse partisane', 'Quotidien', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(191, 'Bayanealyaoume.press.ma', 'Presse nationale', 'Presse electronique', 'Presse partisane', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(192, 'BAYANEMARRAKECH.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(193, 'BAYTTE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(194, 'BENIMELALPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(195, 'BENIMELLAL ONLINE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(196, 'BENIMELLALNEWS.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(197, 'BENIMELLALONLINE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(198, 'BERHIL.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(199, 'BERKANEZOOM.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(200, 'BERRECHIDNEWS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(201, 'BIDARIJA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(202, 'BILAKOYOUD.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(203, 'BILWADEH.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(204, 'BLADI.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(205, 'BLADIONLINE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(206, 'BLADIPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(207, 'BLANCAPRESS.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(208, 'BLAZWA9.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(209, 'BLED.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(210, 'BLEDNA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(211, 'BLEDPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(212, 'BM', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', 'Presse MRE', NULL),
(213, 'BNINSARCITY.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(214, 'BOUYAFAR.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(215, 'BREFINFO.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(216, 'BUSINESSKECH-MARRAKECH.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(217, 'CANALTETOUAN.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(218, 'CAPTOURISME.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse Toursime', NULL),
(219, 'CASA24.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(220, 'CASACITY.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(221, 'CASANEWS.TV', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(222, 'CASAOUI.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(223, 'CASAPRESS.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(224, 'CAWALISSE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(225, 'CENTREPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(226, 'CHAABPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(227, 'ChadaFM.net', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(228, 'CHAFAFIA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(229, 'CHALLENGE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Magazine', 'Francophone', 'Presse economique', NULL),
(230, 'Challenge.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse economique', NULL),
(231, 'CHAMALPOST.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(232, 'CHAMALY.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(233, 'CHANTIERS DU MAROC', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', 'Presse BTP', NULL),
(234, 'CHANTIERSDUMAROC.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse Industrie', NULL),
(235, 'CHICHAOUAALAAN.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(236, 'CHICHAOUAPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(237, 'CHOALAPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(238, 'CHOUFTV.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(239, 'CHTOUKA24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(240, 'CHTOUKAPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(241, 'CLUBDEPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(242, 'COMMUNIQUEDEPRESSE.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(243, 'CONJONCTURE', 'Presse nationale', 'Presse Papier', 'Presse independante', '', 'Magazine', 'Francophone', 'Presse economique', NULL),
(244, 'CONJONCTURE.INFO', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse economique', NULL),
(245, 'CONSONEWS', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Bilingue', 'Presse Conso', NULL),
(246, 'CONSONEWS.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Presse generaliste', NULL),
(247, 'COUSCOOP.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Presse generaliste', NULL),
(248, 'DADES24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(249, 'DAKHLA24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(250, 'DAKHLAALRAI.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(251, 'DAKHLAPRESS.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(252, 'DAKIRATALMADINA.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Presse generaliste', NULL),
(253, 'DIALOGUERIF.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(254, 'DIMABLADNA.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(255, 'DIMARIF.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(256, 'DIPTYK', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', 'Presse Art', NULL),
(257, 'DIRECTPRESSE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(258, 'DOUKKALAMEDIA24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(259, 'DOUNIAPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(260, 'DRAAPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(261, 'DRARGAPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(262, 'DRIOUCHCITY.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(263, 'DROMABUZZ.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(264, 'ECONOMICA.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(265, 'ECONOMIE & ENTREPRISES', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', 'Presse economique', NULL),
(266, 'Economie-entreprises.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse economique ', NULL),
(267, 'Econostrum.info', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse economique ', NULL),
(268, 'ELAIOUN.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(269, 'ELBOTOLA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse sportive', NULL),
(270, 'ELHADAT24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(271, 'ELHAYATDAILY.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(272, 'ELHIWARPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(273, 'ELJADIDA36.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse economique', NULL),
(274, 'ELJADIDAINFO.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(275, 'ELJADIDANEWS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(276, 'ELJADIDAPRESSE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(277, 'ELJADIDATODAY.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(278, 'ELJAREDA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(279, 'ELJAREEDAH.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(280, 'ELKHADRA.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse sportive', NULL),
(281, 'ELWATANMEDIA.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(282, 'ELWATANNOW.INFO', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(283, 'EMBAJADA-MARRUECOS.ES', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Espagnol', 'Presse generaliste', NULL),
(284, 'EMPLOYER.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse RH', NULL),
(285, 'Energie & Mines', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', '', NULL),
(286, 'ENERGIENVIRONNEMENT.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(287, 'Entreprendre.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Agregateur generaliste', NULL),
(288, 'ETIZNIT.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(289, 'FAAPA.INFO', 'Presse internationale', 'Agence de presse ', 'Presse officielle', 'En continu', 'En ligne', 'Multilingue', 'Presse officielle', NULL),
(290, 'FACEPRESS.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(291, 'FADAOKOM.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(292, 'FALLESTEMRABAT.WORDPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse economique ', NULL),
(293, 'FAMILLE ACTUELLE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', 'Presse feminine', NULL),
(294, 'FDALATV.INFO', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(295, 'FEBRAYER.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(296, 'FEMMES DU MAROC', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', 'Presse feminine', NULL),
(297, 'Femmesdumaroc.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse feminine', NULL),
(298, 'FES24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(299, 'FESNEWS.MEDIA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(300, 'FESNEWS.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(301, 'FILDACTU.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(302, 'FINANCENEWS.PRESS.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse economique', NULL),
(303, 'FINANCES NEWS HEBDO', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Francophone', 'Presse economique', NULL),
(304, 'FLASHPRESSE.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(305, 'FLM.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse economique', NULL),
(306, 'FNIWNA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse economique', NULL),
(307, 'FOOD MAGAZINE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', 'Presse Agroalimentaire', NULL),
(308, 'MAROCPRESS.INFO', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Agregateur generaliste', NULL),
(309, 'FREERIF.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(310, 'Golf du Maroc', 'Presse nationale', 'Presse Papier', 'Presse independante', '', 'Magazine', 'Francophone', 'Presse sportive', NULL),
(311, 'GOUD.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(312, 'GRAZIA MAROC', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', 'Presse feminine', NULL),
(313, 'GUELMIMPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(314, 'GUERCIF24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(315, 'H24INFO.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(316, 'HADAT.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(317, 'HADATCOM.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(318, 'HADATH.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(319, 'HAKAIK24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(320, 'HAKAIKPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(321, 'HALAPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse sportive', NULL);
INSERT INTO `supports` (`id`, `name_sup`, `origin_sup`, `cat_sup`, `edit_orie_sup`, `period_sup`, `form_edit_sup`, `lng_edit_sup`, `postition_sup`, `logo_sup`) VALUES
(322, 'HAMSNEWS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(323, 'HAMSSANEWS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(324, 'HAPPYNADOR.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(325, 'HASPRESSE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(326, 'HAYATOKY.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse feminine', NULL),
(327, 'HESPORT.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse sportive', NULL),
(328, 'Hespress.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(329, 'HESSOUSS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(330, 'HEUREDUJOURNAL.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Presse generaliste', NULL),
(331, 'HEUREPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(332, 'HIBA.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse feminine', NULL),
(333, 'Hibapress.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(334, 'HIBAZOOM.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(335, 'HITRADIO.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Presse generaliste', NULL),
(336, 'HIYAPLUS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse feminine', NULL),
(337, 'HOCEIMACITY.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(338, 'HODHOD.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(339, 'HOLA MAROC', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', 'Presse feminine', NULL),
(340, 'HOMMES D\'AFRIQUE MAGAZINE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', '', NULL),
(341, 'HONALHADAT.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(342, 'HORIZONTV.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(343, 'HOSPIHUB.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse medecine', NULL),
(344, 'HOUARANEWS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(345, 'HTARI24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(346, 'HUFFPOSTMAGHREB.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(347, 'ICHRAKANEWS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(348, 'IFNIDIRECT.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(349, 'IFNINEWS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(350, 'IHATA.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(351, 'IKHBARIYA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(352, 'ILAIKI.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse feminine', NULL),
(353, 'INDUSTRIE DU MAROC', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', 'Presse Industrie', NULL),
(354, 'INDUSTRIES.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse Industrie', NULL),
(355, 'INFO24.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(356, 'INFOMEDIAIRE.net', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(357, 'INFOSOUSS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(358, 'ISLYNEWS.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(359, 'ISTIQLAL.INFO', 'Presse nationale', 'Presse electronique', 'Presse partisane', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(360, 'ISTITMAR.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(361, 'JADIDALYAWM.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(362, 'JADIDINFO.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(363, 'JANOUBPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(364, 'JARIDA-TARBAWIYA.BLOGSPOT.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(365, 'JARIDATIPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(366, 'JEUNEPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(367, 'JIHAOUIAPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(368, 'JIHATNEWS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(369, 'JINIGOUD.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(370, 'JOURNALASWAT.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(371, 'JOURNANE.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(372, 'JOURNAUX.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Agregateur generaliste', NULL),
(373, 'KAFAPRESSE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(374, 'KAFAPRESSE.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(375, 'KANASSALATLASS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(376, 'KASPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(377, 'KECH24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(378, 'KHABARMAROC.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(379, 'KHABARPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(380, 'KHBARCOM.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(381, 'KHBARKOM.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(382, 'KHBARNA.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(383, 'KHBIRATE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(384, 'KHEMISSETCOURRIER.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(385, 'KHENIFRA-ONLINE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(386, 'KHOURIBGAMEDIA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(387, 'KHOURIBGANEWS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(388, 'KIFACHE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(389, 'KIFACHE.TV', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(390, 'KLAMKOM.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(391, 'KOLOCHAYAE.WORDPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(392, 'KSARFORUM.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(393, 'LA NOUVELLE TRIBUNE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Francophone', 'Presse generaliste', NULL),
(394, 'LA REVUE DE LA GENDARMERIE ROYALE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Trimestriel', 'Magazine', 'Francophone', 'Presse generaliste', NULL),
(395, 'LA VERITE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Francophone', 'Presse generaliste', NULL),
(396, 'LA VIE ECO', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Francophone', 'Presse economique', NULL),
(397, 'labass.net', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(398, 'LA VOIX DU CENTRE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Francophone', 'Presse generaliste', NULL),
(399, 'LAKOME2.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(400, 'LALAMOULATI.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse feminine', NULL),
(401, 'LALAZINA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse feminine', NULL),
(402, 'LALETTRE.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(403, 'LALLAFATIMA.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse feminine', NULL),
(404, 'LALLA FATEMA', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Arabophone', 'Presse feminine', NULL),
(405, 'LAQUOTIDIENNE.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(406, 'LARACHE24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(407, 'LARACHECITY.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(408, 'LARACHENEWS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(409, 'Lareleve.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(410, 'LAVIEECO.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse economique', NULL),
(411, 'LAVIGIE.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(412, 'LE1.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(413, 'LE212.INFO', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Agregateur generaliste', NULL),
(414, 'LE JOURNAL DES ANNONCES LEGALES & APPELS D\'OFFRES', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', '', 'Francophone', '', NULL),
(415, 'LE360.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Presse generaliste', NULL),
(416, 'LE7TV.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(417, 'LE MATIN DU SAHARA ET DU MAGHREB', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Francophone', 'Presse generaliste', NULL),
(418, 'LE MONDE AMAZIGH', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Journal', 'Arabophone', 'Presse generaliste', NULL),
(419, 'LE REPORTER', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Magazine', 'Francophone', 'Presse generaliste', NULL),
(420, 'LE TEMPS', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Magazine', 'Francophone', 'Presse generaliste', NULL),
(421, 'Lecanardlibere.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(422, 'LECOTIDIEN.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(423, 'LEDAILY.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(424, 'LEDESK.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Presse generaliste', NULL),
(425, 'L\'ECONOMISTE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Francophone', 'Presse economique', NULL),
(426, 'LEGALFLASH.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse juridique', NULL),
(427, 'LEGUIDE.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(428, 'Lejournaldetanger.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(429, 'LEMAG.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(430, 'LEMAGAZINEDUMANAGER.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse Management', NULL),
(431, 'LEMATIN.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(432, 'LEMONDE24.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(433, 'LENQUETE.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(434, 'LEPETITJOURNALMAROCAIN.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(435, 'Lerelais.org', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(436, 'LEREPORTER.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(437, 'LEREPORTEREXPRESS.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(438, 'LES CAHIERS MAP', 'Presse nationale', 'Presse Papier', 'Presse officielle', 'Mensuel', 'Magazine', 'Bilingue', 'Presse officielle', NULL),
(439, 'LES DOSSIERS DU TADLA', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Francophone', '', NULL),
(440, 'LES INSPIRATIONS ECO', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Quotidien', 'Journal', 'Francophone', 'Presse economique', NULL),
(441, 'LESLEADERS.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(442, 'LESECO.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse economique', NULL),
(443, 'LESINFOS.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(444, 'LESITEINFO.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Presse generaliste', NULL),
(445, 'LEVERT.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse Ecologie & Environnement', NULL),
(446, 'LIBE.MA', 'Presse nationale', 'Presse electronique', 'Presse partisane', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(447, 'LIBERATION', 'Presse nationale', 'Presse Papier', 'Presse partisane', 'Quotidien', 'Journal', 'Francophone', 'Presse generaliste', NULL),
(448, 'LIBRE ENTREPRISE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Magazine', 'Francophone', 'Presse economique', NULL),
(449, 'L\'INTERMEDIAIRE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Journal', 'Francophone', '', NULL),
(450, 'LIONSDELATLAS.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse sportive', NULL),
(451, 'LNT.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(452, 'L\'OBSERVATEUR DU MAROC ET D\'AFRIQUE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Magazine', 'Francophone', 'Presse generaliste', NULL),
(453, 'Lobservateur.info', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(454, 'L\'OPINION', 'Presse nationale', 'Presse Papier', 'Presse partisane', 'Quotidien', 'Journal', 'Francophone', 'Presse generaliste', NULL),
(455, 'Lopinion.ma', 'Presse nationale', 'Presse electronique', 'Presse partisane', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(456, 'LUXERADIO.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(457, 'MAARIFPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(458, 'MACHAHID.INFO', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(459, 'MACHAHID24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(460, 'MACHAHIDPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(461, 'MADAJARA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(462, 'Madame Lifeguide Maroc', 'Presse nationale', 'Presse Papier', 'Presse independante', '', 'Journal', 'Arabophone', '', NULL),
(463, 'MADAMECASA.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse feminine', NULL),
(464, 'MADEINMEDINA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse Cityguide', NULL),
(465, 'MAGAZINEINNOVANT.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(466, 'MAGHAREBNEWS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(467, 'MAGHREB ARABE PRESSE', 'Presse nationale', 'Agence de presse', 'Presse officielle', 'En continu', 'En ligne', 'Multilingue', 'Presse officielle', NULL),
(468, 'MAGHREBEMERGENT.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse economique', NULL),
(469, 'MAGHREBINTELLIGENCE.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(470, 'MAGHREBVOICES.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(471, 'MAGHRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Agregateur generaliste', NULL),
(472, 'MAHATA24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(473, 'MAJALA24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(474, 'MAJALIS', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Magazine', 'Arabophone', 'Presse generaliste', NULL),
(475, 'MAMLAKAPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(476, 'MAMLAKATONA.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(477, 'Manabirpress.com', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(478, 'MANARPLUS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(479, 'MAPRESS.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(480, 'Maritimenews.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse maritime', NULL),
(481, 'MAROC DIPLOMATIQUE', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Mensuel', 'Journal', 'Francophone', 'Presse generaliste', NULL),
(482, 'MAROC HEBDO INTERNATIONAL', 'Presse nationale', 'Presse Papier', 'Presse independante', 'Hebdomadaire', 'Magazine', 'Francophone', 'Presse generaliste', NULL),
(483, 'MAROC.MA', 'Presse nationale', 'Portail du Maroc', 'Presse officielle', 'En continu', 'En ligne', 'Multilingue', 'Presse officielle', NULL),
(484, 'MAROC24.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Multilingue', 'Presse generaliste', NULL),
(485, 'MAROCAINSPARTOUT.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(486, 'MAROCBLEU.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse maritime', NULL),
(487, 'Maroc-diplomatique.net', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(488, 'Maroc-hebdo.press.ma', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(489, 'MAROCLAW.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(490, 'MAROC-LEAKS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Francophone', 'Presse generaliste', NULL),
(491, 'MAROCLIBRETV.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(492, 'MAROCNEWS.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Bilingue', 'Presse generaliste', NULL),
(493, 'MAROCNEWS24.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(494, 'MAROCPLUS.INFO', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(495, 'MAROCPOLIS.NET', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(496, 'MAROCPRESS.COM', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL),
(497, 'MAROCREGIONS.MA', 'Presse nationale', 'Presse electronique', 'Presse independante', 'En continu', 'En ligne', 'Arabophone', 'Presse generaliste', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `support_formats`
--

DROP TABLE IF EXISTS `support_formats`;
CREATE TABLE IF NOT EXISTS `support_formats` (
  `support_id` int(10) UNSIGNED NOT NULL,
  `format_id` int(10) UNSIGNED NOT NULL,
  KEY `support_formats_support_id_foreign` (`support_id`),
  KEY `support_formats_format_id_foreign` (`format_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `support_formats`
--

INSERT INTO `support_formats` (`support_id`, `format_id`) VALUES
(6, 1),
(1, 2),
(6, 3),
(1, 4),
(328, 5),
(12, 6),
(41, 7),
(33, 8);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@admin.com', '$2y$10$saGZ8nEiyh0VoiiBkKE0Wuj9m0OntVwyoHr3E6noS1dYZhyXnZXlO', 'dyY3Fksh9n923T0epEOyoSZYPJFdzrQvLmhOjOb1xClGN77ynKiwHy8rkOVg', '2017-12-18 20:30:42', '2017-12-18 20:30:42');

-- --------------------------------------------------------

--
-- Structure de la table `users_logs`
--

DROP TABLE IF EXISTS `users_logs`;
CREATE TABLE IF NOT EXISTS `users_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `action` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `action_model` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
