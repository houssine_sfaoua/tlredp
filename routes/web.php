<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', function () {
    //dd(Auth::guest());
     if(!Auth::guest()){
        return redirect()->action('\Laraveldaily\Quickadmin\Controllers\QuickadminController@index');
     }else{
        
     }
});

Route::get('/pass', function () {
    //dd(bcrypt("")); 
    return view('login');
});


Route::get('/login', function () {
    //dd("login"); 
    return view('login');
});

Route::get('extranet/{id}','ExtranetController@index');
Route::get('extranet/resume/{id}','ExtranetController@resumeExtranet');
Route::get('extranet/joint/{downloadPdfFile}','ExtranetController@downloadPdfFile');
Route::post('extranet/print',['as' => 'extranet.print', 'uses' => 'ExtranetController@printPdf']);
Route::post('extranet/printimpact',['as' => 'extranet.printtcpdf', 'uses' => 'ExtranetController@printtcpdf']);
Route::post('extranet/resume/print',['as' => 'extranet.print.resume', 'uses' => 'ExtranetController@printResumePdf']);

Route::group(['middleware' => ['auth']], function() {
    

//impacts
Route::get('admin/impacts',['as' => 'impacts.index', 'uses' => 'ImpactController@index']);
Route::get('admin/impacts/create',['as' => 'impacts.create', 'uses' => 'ImpactController@create']);
Route::post('admin/impacts/store',['as' => 'impact.store', 'uses' => 'ImpactController@store']);
Route::get('admin/impacts/edit/{id}',['as' => 'impact.edit', 'uses' => 'ImpactController@edit']);
Route::delete('admin/impacts/destroy/{id}',['as' => 'impact.destroy', 'uses' => 'ImpactController@destroy']);

//reviews
Route::get('admin/reviews',['as' => 'reviews.index', 'uses' => 'ReviewController@index']);
Route::post('admin/reviews/pdf',['as' => 'reviews.impacts.pdf', 'uses' => 'ReviewController@printAllImpactPdf']);
Route::post('admin/reviews/primpact',['as' => 'reviews.impacts.tcpdf', 'uses' => 'ReviewController@generateTcpdf']);
Route::post('admin/reviews/send',['as' => 'reviews.impacts.send', 'uses' => 'ReviewController@sendImpactsByMail']);
Route::post('admin/reviews/sendempty',['as' => 'reviews.impactsempty.sendempty', 'uses' => 'ReviewController@sendEmptyMail']);

// show images
Route::get('admin/images/{impactid}', ['as' => 'images.show','uses'=> 'ReviewController@showImage']);

//impact detail
Route::get('admin/impacts-detail/{id}',['as' => 'impacts.detail.index', 'uses' => 'ImpactDetailController@index']);
Route::post('admin/print/pdf',['as' => 'impacts.print.pdf', 'uses' => 'ImpactDetailController@printPdf']);
Route::post('admin/print/impact',['as' => 'impacts.print.tcpdf', 'uses' => 'ImpactDetailController@printtcpdf']);
Route::post('admin/print/resumepdf',['as' => 'impacts.print.resumepdf', 'uses' => 'ImpactDetailController@printResumePdf']);
Route::post('admin/print/word',['as' => 'impacts.print.word', 'uses' => 'ImpactDetailController@printWord']);


//criteria
Route::get('admin/criteria',['as' => 'criteria.index', 'uses' => 'CriteriaController@index']);
Route::get('admin/criteria/create',['as' => 'criteria.create', 'uses' => 'CriteriaController@create']);
Route::post('admin/criteria/store',['as' => 'criteria.store', 'uses' => 'CriteriaController@store']);
Route::get('admin/criteria/edit/{id}',['as' => 'criteria.edit', 'uses' => 'CriteriaController@edit']);

//subcriteria
Route::get('admin/subcriteria',['as' => 'subcriteria.index', 'uses' => 'SubCriteriaController@index']);
Route::get('admin/subcriteria/create',['as' => 'subcriteria.create', 'uses' => 'SubCriteriaController@create']);
Route::post('admin/subcriteria/store',['as' => 'subcriteria.store', 'uses' => 'SubCriteriaController@store']);
Route::get('admin/subcriteria/edit/{id}',['as' => 'subcriteria.edit', 'uses' => 'SubCriteriaController@edit']);

//support
Route::get('admin/support',['as' => 'support.index', 'uses' => 'SupportController@index']);
Route::get('admin/support/create',['as' => 'support.create', 'uses' => 'SupportController@create']);
Route::post('admin/support/store',['as' => 'support.store', 'uses' => 'SupportController@store']);
Route::get('admin/support/edit/{id}',['as' => 'support.edit', 'uses' => 'SupportController@edit']);


//client
Route::get('admin/client',['as' => 'client.index', 'uses' => 'ClientController@index']);
Route::get('admin/client/edit/{id}',['as' => 'client.edit', 'uses' => 'ClientController@edit']);
Route::get('admin/client/create',['as' => 'client.create', 'uses' => 'ClientController@create']);
Route::post('admin/client/store',['as' => 'client.store', 'uses' => 'ClientController@store']);


// ajax call
Route::post('admin/impact/getFile',['as' => 'impact.ajax.getFile', 'uses' => 'ImpactController@getFile']);
//Route::get('ajax/getallauthor/{query}',['as' => 'ajax.getallauthor', 'uses' => 'AjaxReqController@getAllAuthor']);
Route::get('ajax/getallauthor',['as' => 'ajax.getallauthor', 'uses' => 'AjaxReqController@getAllAuthor']);
Route::post('ajax/ajaxdt',['as' => 'ajax.ajaxdt', 'uses' => 'AjaxReqController@ajaxTestDatatable']);
Route::post('ajax/getsubcriteribycrid',['as' => 'ajax.getsubcriteribycrid', 'uses' => 'AjaxReqController@getSubcriteriaByCriteriaId']);
Route::get('ajax/getallsupport',['as' => 'ajax.getallsupport', 'uses' => 'AjaxReqController@getAllSupports']);
Route::post('ajax/getsupportbyid',['as' => 'ajax.getsupportbyid', 'uses' => 'AjaxReqController@getSupportById']);
Route::get('ajax/getallrubrique',['as' => 'ajax.getallrubrique', 'uses' => 'AjaxReqController@getAllRubrique']);
Route::post('ajax/getformatbyid',['as' => 'ajax.getformatbyid', 'uses' => 'AjaxReqController@getFormatById']);
Route::post('ajax/getcriteriabyclientid',['as' => 'ajax.getcriteriabyclientid', 'uses' => 'AjaxReqController@getCriteriaByClientid']);
Route::post('ajax/getsubcriteriabycriteriaid',['as' => 'ajax.getsubcriteriabycriteriaid', 'uses' => 'AjaxReqController@getCubcriteriaByCriteriaId']);
Route::post('ajax/searchimpact',['as' => 'ajax.searchimpact', 'uses' => 'AjaxReqController@searchImpact']);
Route::post('ajax/getimpacts',['as' => 'ajax.getimpacts', 'uses' => 'AjaxReqController@getImpactByIds']);
Route::get('ajax/getPdfList',['as' => 'ajax.getpdflist', 'uses' => 'AjaxReqController@getListPdf']);
Route::get('ajax/getPdfListSelect',['as' => 'ajax.getpdflistselect', 'uses' => 'AjaxReqController@getListPdfSelect']);

//Format
Route::get('admin/formats',['as' => 'formats.index', 'uses' => 'FormatController@index']);
Route::get('admin/formats/create',['as' => 'formats.create', 'uses' => 'FormatController@create']);
Route::get('admin/formats/edit/{id}',['as' => 'formats.edit', 'uses' => 'FormatController@edit']);
Route::post('admin/formats/store',['as' => 'formats.store', 'uses' => 'FormatController@store']);
});