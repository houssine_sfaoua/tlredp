@include('admin.partials.header')
@include('admin.partials.topbar')
<div class="clearfix"></div>
<div class="page-container">

    <div class="page-content-wrapper">
        <div class="page-content">

            <!-- <h3 class="page-title">
                {{ preg_replace('/([a-z0-9])?([A-Z])/','$1 $2',str_replace('Controller','',explode("@",class_basename(app('request')->route()->getAction()['controller']))[0])) }}
                {{-- {{ trans('quickadmin::admin.partials-topbar-title') }} --}}
            </h3> -->

            <div class="row">
                <div class="col-md-12">

                    @if (Session::has('message'))
                        <div class="note note-info">
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif

<style>
    .page-content-wrapper .page-content{
        margin-left: 0 !important;
    }
    #adv-custom-pager span{
        font-size: 40px;
    }
    div.support-section{
        float: left;
        width: 25%;
    }

    div.support-section h2{
        margin: 0;
    }

    p.title-impact{
        text-align: center;
        line-height: 1.5;
        width: 100%;
        font-size: 21px;
        font-weight: bold;
    }

    p.info-impact{
        margin-top:15px;
    }    

    div.scan-container{
        margin-top: 0.5%;
    }

    #itemContainer li{
        list-style:none !important;
    }

    #itemContainer li img{
        margin: 0 auto;
        display: block;
    }
    #btn-impact-pdf {
        background: url('/tlredp/public/image/PDF-LOGO.png') no-repeat;
        display: inline-block;
        height: 50px;
        width: 50px;
        background-size: 50px 50px;
        border: none;
    }
    

    /******************************/

/*----*/
<style>
    #adv-custom-pager span{
        font-size: 40px;
    }
    div.support-section{
        float: left;
        width: 30%;
    }

    div.support-section h2{
        margin: 0;
    }
    
    div.detail-header div.title-container{
        /* margin: 6% 0 6% 0; */
    }

    div.detail-header p.title-impact{
        line-height: 2.5;
        margin: 38px 0 0 0 !important;
        width: 100%;
        font-size: 21px;
        font-weight: bold;
    }

    div.detail-header p.resume-impact{
        margin: 0 0 0 0 !important;
        width: 100%;
    }

    div.action-impact{
        float: right;
    }

    div.action-impact input.btn-action-impact{
        padding: 10% 35%;
        width: 8em;
    }
    
    

    p.info-impact{
        margin-top:15px;
    }    

    #itemContainer li{
        list-style:none !important;
    }

    #itemContainer li img{
        margin: 0 auto;
        display: block;
    }
    

    /******************************/

     .holder {
    margin:15px 0;
}
.holder a {
    font-size:12px;
    cursor:pointer;
    margin:0 5px;
    color:#333;
}
.holder a:hover {
    background-color:#222;
    color:#fff;
}
.holder a.jp-previous {
    margin-right:15px;
}
.holder a.jp-next {
    margin-left:15px;
}
.holder a.jp-current,a.jp-current:hover {
    color:#FF4242;
    font-weight:bold;
}
.holder a.jp-disabled,a.jp-disabled:hover {
    color:#bbb;
}
.holder a.jp-current,a.jp-current:hover,.holder a.jp-disabled,a.jp-disabled:hover {
    cursor:default;
    background:none;
}
.holder span {
    margin:0 5px;
}
form {
    /* float:right;
    margin-right:10px; */
}
form label {
    /* margin-right: 5px; */
}

    #img-support{
        width: 40%;
    }

    #support-info{
        width: 50em;
        margin-top: 22px;
    }
    .support-name{
        font-size:16px;
        margin: 0;
    }
    .support-subname{
        margin: 0;
        font-size: 12px;
    }    
    .img-scan{
        margin-top: 10%;
        width: 50%;
        display: block;
        margin-left: 25%;
    }

    label.lbl-fiche, label.lbl-link{
        display: block;
        font-weight: 600;
    }
    /* span.sheet-impact {
        font-size: 13px;
        font-weight: normal;
    } */
    .title-row {
            margin-top: 3%;
        }
        #impact-fiche h2{
            font-weight: bold;
        }
        p.sheet-impact {
            display: inline-block;
            font-style: normal;
            font-weight: 400;
            margin: 0;
        }
        p.sheet-impact::first-letter{
            text-transform:uppercase;
        }
    @media only screen and (max-width: 768px) {
        span.sheet-impact {
            font-size: 12px;
        }
        #impact-fiche h2{
            font-weight: 600;
            font-size: 14px;
        }
        #impact-fiche label{
            font-size: 10px;
        }
        #impact-fiche {
            margin: 0 auto;
            width: 44%;
        }
        #itemContainer li img {
            width: 100% !important;
        }
        p.title-impact{
        line-height: 1.5;
        }
        .title-row {
            margin-top: 8%;
        }
        #support-info p {
            font-size: 11px;
        }
    }

</style>
<!-- <div class="detail-header col-md-8">
    <div class="support-section">
        @if($aImpact->supportobj->logo_sup!= null)
            <img src="{{ env('APP_URL').'/storage/app/public/uploads/support/'.$aImpact->supportobj->id.'/'.$aImpact->supportobj->logo_sup }}" alt="img support" id="img-support" style="">
        @else
            <img src="{{ env('APP_URL').'/storage/app/public/newspapericon.jpg' }}" alt="img support" id="img-support" style="">
        @endif
        <div id="support-info" style="">
        <p style="">{{$aImpact->supportobj->name_sup}} <br/> {{ $aImpact->supportobj->form_edit_sup }} . {{$aImpact->supportobj->period_sup }} . {{$aImpact->supportobj->lng_edit_sup }} . {{$aImpact->supportobj->postition_sup }}</p>
        </div>
    </div>
    <div class="action-impact">
    @if(isset($aImpact->images))
        {!! Form::open(['route' => 'extranet.print.resume', 'class' => 'form-horizontal','style'=>'float:left;'])!!}
        {!! Form::hidden('impactid',$aImpact->id, []) !!}
        {!! Form::submit('PDF', array('class' => 'btn btn-primary btn-action-resumeimpact','formtarget' => '_blank')) !!}
        {!! Form::close() !!}
    @endif
    </div>

      
</div>
<div class="col-md-4" style="float: right;">
    <h2>Fiche de la retombée</h2>
    <label class="lbl-fiche" for="">Nom de la rubrique : <span class="sheet-impact">{{ $aImpact->rubriqueobj->nom_rub }}</span> </label>
    @if(isset($aImpact->num_page_imp))
        <label class="lbl-fiche" for="">Numéro de page : <span class="sheet-impact" >{{ $aImpact->num_page_imp }}</span> </label>
    @endif
    <label class="lbl-fiche" for="">Auteur : <span class="sheet-impact" >{{ $aImpact->authorobj->name_aut }}</span></label>
    <label class="lbl-fiche" for="">Date de parution : <span class="sheet-impact" >{{ Carbon\Carbon::parse($aImpact->date_imp)->format('d-m-Y') }}</span></label>
    <label class="lbl-fiche" for="">Ton de la retombée : <span class="sheet-impact" >{{ $aImpact->pace_imp }}</span></label>
    <label class="lbl-fiche" for="">Format : <span class="sheet-impact" >{{ $aImpact->formatobj->nom_frm }}</span></label>
    <label class="lbl-fiche" for="">Impression : <span class="sheet-impact" >{{ $aImpact->formatobj->print_frm }}</span></label>
    <label class="lbl-fiche" for="">EP:</label>
    <label for="lbl-link" class="lbl-link">Lien : <a class="sheet-impact" href="{{ $aImpact->link_imp }}" target="blank">{{ $aImpact->link_imp }}</a></label>
</div>
<div id="main-content" class="col-md-12">
    <div class="title-container">
        <p class="title-impact" style="">{{$aImpact->title_imp}}</p>
        <p class="resume-impact" style="">{{$aImpact->resume_imp}}</p>
    </div>  
</div>



                </div>
            </div>

        </div>
    </div>

</div> -->

<div class="row">
    <div class="col-xs-8 col-sm-4">
        @if($aImpact->supportobj->logo_sup!= null)
            <img src="{{ env('APP_URL').'/storage/app/public/uploads/support/'.$aImpact->supportobj->id.'/'.$aImpact->supportobj->logo_sup }}" alt="img support" id="img-support" style="">
        @else
            <img src="{{ env('APP_URL').'/storage/app/public/newspapericon.jpg' }}" alt="img support" id="img-support" style="">
        @endif
        <div id="support-info" style="">
        <p style="">{{$aImpact->supportobj->name_sup}} <br/> {{ $aImpact->supportobj->form_edit_sup }} . {{$aImpact->supportobj->period_sup }} . {{$aImpact->supportobj->lng_edit_sup }} . {{$aImpact->supportobj->postition_sup }}</p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4">
        @if(isset($aImpact->images))
            {!! Form::open(['route' => 'extranet.print', 'class' => 'form-horizontal','style'=>'text-align: right;padding-top: 3%;'])!!}
            {!! Form::hidden('impactid',$aImpact->id, []) !!}
            {!! Form::submit('', array('class' => '','id'=>'btn-impact-pdf','formtarget' => '_blank')) !!}
            {!! Form::close() !!}
        @endif
    </div>
    <div class="col-sm-4 col-xs-12">
    <!-- col-xs-offset-2 -->
        <div class="" id="impact-fiche">
            <h2>Fiche de la retombée</h2>
            <label class="lbl-fiche" for="">Nom de la rubrique : <p class="sheet-impact">{{ $aImpact->rubriqueobj->nom_rub }}</p> </label>
            @if(isset($aImpact->num_page_imp))
                <label class="lbl-fiche" for="">Numéro de page : <p class="sheet-impact" >{{ $aImpact->num_page_imp }}</p> </label>
            @endif
            <label class="lbl-fiche" for="">Auteur : <p class="sheet-impact" >{{ $aImpact->authorobj->name_aut }}</p></label>
            <label class="lbl-fiche" for="">Date de parution : <p class="sheet-impact" >{{ Carbon\Carbon::parse($aImpact->date_imp)->format('d-m-Y') }}</p></label>
            <label class="lbl-fiche" for="">Ton de la retombée : <p class="sheet-impact" >{{ $aImpact->pace_imp }}</p></label>
            <label class="lbl-fiche" for="">Format : <p class="sheet-impact" >{{ $aImpact->formatobj->nom_frm }}</p></label>
            <label class="lbl-fiche" for="">Impression : @if($aImpact->formatobj->print_frm == "noirblanc")<p class="sheet-impact" >Noir & Blanc</p>@else<p class="sheet-impact" >Couleur</p> @endif</label>
            <label class="lbl-fiche" for="">EP :</label>
            @if(isset($aImpact->link_imp))
                <label for="lbl-link" class="lbl-link">Lien : <a class="sheet-impact" href="{{ $aImpact->link_imp }}" target="blank">{{ $aImpact->link_imp }}</a></label>
            @endif
        </div>
    </div>
</div>
<div class="row title-row">
    <div class="col-sm-12">
        <p class="title-impact" style="">{{$aImpact->title_imp}}</p>
        <p class="resume-impact" style="">{{$aImpact->resume_imp}}</p>
    </div>
</div>

@include('admin.partials.footer')
