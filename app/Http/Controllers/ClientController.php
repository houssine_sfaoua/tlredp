<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\ClientCriteria;
use App\Criteria;
use App\Subcriteria;
use App\ClientSubcriteria;
use App\ClientCompany;

class ClientController extends Controller
{
    public function index(){

        $clientCompany  = ClientCompany::all();

        return view('admin.clients.index',compact('clientCompany'));
    }

    public function create(){
        
        return view('admin.clients.create');
    }

    public function edit($id){

        $clientCompany  = ClientCompany::findOrFail($id);

        $clientCompany->logo = $clientCompany->id."_".$clientCompany->logo_cl;

        $aClientCriteria = ClientCriteria::whereClient_id($clientCompany->id)->get();
        $aClientSubCriteria = ClientSubcriteria::whereClient_id($clientCompany->id)->get();

        //dd($aClientSubCriteria);

        $aCriteriaClient = array();
        $aSubCriteriaClient = array();

        foreach ($aClientCriteria as $key => $clientCriteria) {

            array_push($aCriteriaClient,$clientCriteria->criteria);

        }

        foreach ($aClientSubCriteria as $key => $clientSubCriteria) {

            array_push($aSubCriteriaClient,$clientSubCriteria->subcriteria);
        }

        $clientCompany->criteria = $aCriteriaClient;
        $clientCompany->subcriteria = $aSubCriteriaClient;

        //dd($aSubCriteriaClient);

        return view('admin.clients.edit', compact('clientCompany'));

    }

    // edit and store items
    public function store(Request $request){

        $data = $request->all();

        //dd($data);
        if(isset($data['edit'])){
            $logoRequired = "";
        }else{
            $logoRequired = "required";
        }

        $validatedData = $request->validate([
            'nom_cl' => 'required',
            'email_cl' => 'required',
            'logo_cl' => $logoRequired,
            'criteria_id_cl'=>'required',
            'subcriteria_id_cl'=>''
        ]);

             $aClient = array(
            'nom_cl'=>$data['nom_cl'],
            'email_cl'=>$data['email_cl'],
            'tel_cl'=>$data['tel_cl']
        );
       
        //dd($data);
        if(isset($data['edit'])){
            
            ClientCompany::where('id', $data['edit'])->update($aClient);
            $client = ClientCompany::find($data['edit']);
        }else{
            $client = ClientCompany::create($aClient);
        }
        
        //dd($client);
        // store criteria

        if(isset($data['criteria_id_cl'])){

                if($data['criteria_id_cl'] != "autre"){

                $aCriteria =explode(",",$data['criteriaitems']);

                if(isset($data['edit'])){
                    ClientCriteria::whereClient_id($data['edit'])->delete();
                }
                
                // store criteria
                foreach ($aCriteria as $key => $criteria) {
                    $temp = array("criteria_id"=>intval($criteria),"client_id"=>(isset($data['edit'])? $data['edit']: $client->id));
                    //print_r($temp);
                    ClientCriteria::create($temp);
                }

            }else{

                // create criteria

                //store client criteria
                foreach ($data['newcriteria_lbl_cl'] as $key => $criteria) {

                    if(isset($criteria) && $criteria != null){
                        $criteria=Criteria::create(array('label_cri'=>$criteria));

                        $temp = array("criteria_id"=>intval($criteria->id),"client_id"=>(isset($data['edit'])? $data['edit']: $client->id));
                        //print_r($temp);
                        ClientCriteria::create($temp);
                    }
                    
                }
            }
        }
        
        if(isset($data['subcriteria_id_cl'])){

            if($data['subcriteria_id_cl'] != "autre"){

             $aSubCriteria =explode(",",$data['subcriteriaitems']);

                if(isset($data['edit'])){
                    ClientSubcriteria::whereClient_id($data['edit'])->delete();
                }

                // store subcriteria
                foreach ($aSubCriteria as $key => $subcriteria) {
                    $subtemp = array("subcriteria_id"=>intval($subcriteria),"client_id"=>(isset($data['edit'])? $data['edit']: $client->id));
                    
                    ClientSubcriteria::create($subtemp);
                }
            }else{
                
                //store client criteria
                foreach ($data['newsubcriteria_lbl_cl'] as $key => $subcriteria) {

                    if(isset($subcriteria) && $subcriteria != null){
                        $oSubcriteria=Subcriteria::create(array('label_subcr'=>$subcriteria,'criteria_id_subcr'=>null));

                        $subtemp = array("subcriteria_id"=>intval($oSubcriteria->id),"client_id"=>(isset($data['edit'])? $data['edit']: $client->id));
                        //print_r($subtemp);
                        ClientSubcriteria::create($subtemp);
                    }
                    
                }
            }
        }
        
        
        if(isset($request->logo_cl)){
            $image = $request->logo;
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);

            if(isset($client)){
                 $result = Storage::disk('public')->put('clients/'.$client->id.'_'.$request->logo_cl,base64_decode($image));
                 //dd($result);
                 if($result){
                    $client->logo_cl = $request->logo_cl;
                    $client->save();
                 }
            }
        }
        return redirect()->route('client.index')->withMessage(trans('quickadmin::admin.item-controller-successfully_created'));
    }


}
