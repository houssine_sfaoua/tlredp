<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Criteria;

class CriteriaController extends Controller
{
    
    public function index(){

        $aCriteria =Criteria::all();
        //dd($aCriteria);

        return view("admin.criterias.index",compact('aCriteria'));
    }


    public function create(){
        return view("admin.criterias.create");
    }

    public function edit($id){

        $criteria  = Criteria::findOrFail($id);
        
        return view("admin.criterias.edit",compact('criteria'));
    }

    public function store(Request $request){

        $data = $request->all();

        if(isset($data['edit'])){
            $aCriteria = array("label_cri"=>$data['label_cri']);

            Criteria::where('id', $data['edit'])->update($aCriteria);
        }else{
            $criteria=Criteria::create($data);
        }
        
        return redirect()->route('criteria.index')->withMessage(trans('quickadmin::admin.item-controller-successfully_created'));
    }
}
