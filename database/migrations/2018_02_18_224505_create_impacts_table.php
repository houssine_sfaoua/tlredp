<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImpactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('impacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_imp');
            $table->enum('language_imp', ['ar', 'fr','en']);
            $table->longText('resume_imp')->nullable();
            $table->string('source_imp')->nullable();
            $table->string('scan_imp')->nullable();
            $table->date('date_imp');
            $table->date('date_integ_imp');
            $table->integer('selection_imp');
            $table->integer('authorid_imp')->unsigned();
            $table->string('pace_imp');
            $table->integer('supportid_imp')->unsigned();
            $table->integer('num_edit_imp')->nullable();
            $table->integer('num_page_imp')->nullable();
            $table->string('link_imp')->nullable();
            $table->integer('rubid_imp')->unsigned();
            $table->integer('formatid_imp')->unsigned();
            $table->integer('appear_imp');
            $table->integer('appearid_imp')->unsigned()->nullable();
            $table->string('nature_imp');

            $table->foreign('authorid_imp')->references('id')->on('authors');
            $table->foreign('supportid_imp')->references('id')->on('supports'); 
            $table->foreign('rubid_imp')->references('id')->on('rubriques'); 
            $table->foreign('formatid_imp')->references('id')->on('formats');
            $table->foreign('appearid_imp')->references('id')->on('appears');             
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('impacts');
    }
}
