<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rubrique extends Model
{
    protected $table = "rubriques";
    
    public $timestamps = false;

    protected $fillable = array("nom_rub");
}
