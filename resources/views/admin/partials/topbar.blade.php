<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner">
        <div class="page-header-inner">
            <div class="navbar-header">
                <a href="{{ url(config('quickadmin.homeRoute')) }}" class="navbar-brand">
                    {{ trans('quickadmin::admin.partials-topbar-title') }}
                </a>
            </div>
            <a href="javascript:;"
               class="menu-toggler responsive-toggler"
               data-toggle="collapse"
               data-target=".navbar-collapse">
            </a>

            <div class="top-menu">
                @if (Auth::check())
                <ul class="nav navbar-nav pull-right" style="margin-top: 14px;">
                    <p style="color:white;">{{ Auth::user()->name }}</p>
                </ul>
                @endif
            </div>
        </div>
    </div>
</div>