<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Author;
use App\Subcriteria;
use App\Support;
use App\Rubrique;
use App\Format;
use App\Impact;
use App\ClientCompany;
use App\ClientCriteria;
use Yajra\Datatables\Facades\Datatables;
use App\ImpactClient;
use App\SupportFormat;
use App\Criteria;
use App\Appear;
use Image;
use App\User;
use Auth;


class AjaxReqController extends Controller
{
    //


    /**
    * get all author
    */
    public function getAllAuthor_($query){
        //print_r($query);die;
        /*$query = $request->all();
        print_r($query);die;*/

        $aAuthor = Author::where('name_aut', 'like', '%' . $query . '%')->get();
        //print_r($aAuthor);
        return response()->json($aAuthor);
    }
    /**
    * get all author
    */
    public function getAllAuthor(){

        $aAuthor = Author::all();
        //print_r($aAuthor);die;
        return response()->json($aAuthor, 200);
    }

    public function getListPdfSelect() {
        $aFiles = Storage::disk('public')->files('pdfs/');

        $downloadedFiles = [];
        if(isset($aFiles) && !empty($aFiles)) {
            foreach ($aFiles as $key => $filename) {
                    $partFile = explode("/", $filename);
                if ($partFile[1] != ".DS_Store") {
                    $filename = $partFile[1];
                    $downloadedFiles[$key] = $filename;
                }
            }
        }
        return response()->json($downloadedFiles, 200);
    }
      /**
    * get all support
    */
    public function getListPdf() {
        $aFiles = Storage::disk('public')->files('pdfs/');

        $downloadedFiles = [];
        if(isset($aFiles) && !empty($aFiles)) {
            foreach ($aFiles as $key => $filename) {
                    $partFile = explode("/", $filename);
                if ($partFile[1] != ".DS_Store") {
                    $filename = $partFile[1];
                    $downloadedFiles[$key] = $filename;
                }
            }
        }
        //dd($downloadedFiles);
        return view('admin.partials.fileList',compact('downloadedFiles'));
    }

    /**
    * get all support
    */

    public function getAllSupports(){

        $aSupport = Support::all();
        //print_r($aSupport);die;
        return response()->json($aSupport, 200);

    }

    public function getCriteriaByClientid(Request $request){
        $data = $request->all();

        $aClientCriteria = ClientCriteria::whereClient_id($data['clientid'])->get();

        //print_r($aCriteria[0]->criteria);die;

        $aCriteria = array();

        foreach ($aClientCriteria as $key => $clientCriteria) {
            //print_r($clientCriteria->criteria);
            array_push($aCriteria,$clientCriteria->criteria);
        }

        return response()->json($aCriteria, 200);
    }

    public function getCubcriteriaByCriteriaId(Request $request){
        $data = $request->all();

        $aSubcriteriaCriteria = Subcriteria::whereCriteria_id_subcr($data['criteriaid'])->get();

        return response()->json($aSubcriteriaCriteria, 200);

    }

    /**
    * get support by id
    */
    public function getSupportById(Request $request){
        $data  = $request->all();
        //print_r($data);die;
        $aSupport = array();
        if(isset($data['supportid']) && !empty($data['supportid'])){
            $aSupport = Support::find($data['supportid']);
        }

        $formatArray = array();
        //dump($aSupport->supportFormat->count());

        if(isset($aSupport->supportFormat) && $aSupport->supportFormat->count()>0){
            foreach ($aSupport->supportFormat as $key => $supportformat) {
                //dump($supportformat->format_id);
                
                $aFormat = Format::find($supportformat->format_id);
                //var_dump($aFormat);

                array_push($formatArray,array('id'=>$aFormat->id,"nom_frm"=>$aFormat->nom_frm, "pseudo_frm"=> $aFormat->pseudo_frm));
            }
            $aSupport->formatItems = $formatArray;
        }
        
        //dd($aSupport);
    
        return response()->json($aSupport,200);
        
    }

    public function getSubcriteriaByCriteriaId(Request $request){

        //print_r($request->all());die;

        $data = $request->all();

        $oSubcriteria = new Subcriteria;

        $aSubcriteriaTemp = array();
        $aCriteriaid= explode(",",$data['criteriaid']);

        foreach ($aCriteriaid as $key => $criteriaid) {
            $aSubcriteria = $oSubcriteria->getSubcriteriaByCriteriaId($criteriaid);

            //print_r(empty($aSubcriteria));
            if(isset($aSubcriteria) && count($aSubcriteria)>0){
                foreach ($aSubcriteria as $key => $aSubcriteriaVal) {
                    array_push($aSubcriteriaTemp,$aSubcriteriaVal);
                }
                
            }
        }
        
        //print_r($aSubcriteriaTemp);die;
        return response()->json($aSubcriteriaTemp);
    }

    public function getAllRubrique(){
        $aRubrique = Rubrique::all();
        return response()->json($aRubrique);
    }

    /**
    * get format by id
    */
    public function getFormatById(Request $request){
        $data = $request->all();

        $aFormat = Format::where('id',$data['formatid'])->get();

        //print_r($aFormat);die;

        return response()->json($aFormat);
    }
    /**
     * get impact by id
     */
    public function getImpactByIds(Request $request){
        $data = $request->all();
        $aImpactCriteria = [];
        if(isset($data['impactId'])){
            $impactIds=explode(",", $data['impactId']);
            foreach ($impactIds as $key => $impactid) {
                if (isset($impactid) && $impactid != null && !is_null($impactid)){
                    // get items of impact from impact_clients
                    //dump($impactid);                
                    $aImpacts[] = ImpactClient::whereImpactid_imp_cl($impactid)->get();
                    //$aImpacts[] = Impact::find($impactid);
                }
            }
        }
        foreach ($aImpacts as $key => $impactCl) {
            if (isset($impactCl) && $impactCl->count() > 0){
                if(!in_array($impactCl[0]->criteriaid_imp_cl, $aImpactCriteria)) {
                    $aImpactCriteria[$impactCl[0]->criteriaid_imp_cl][] = ImpactClient::join('impacts','impact_clients.impactid_imp_cl','=','impacts.id')
                                ->join('criterias','impact_clients.criteriaid_imp_cl','=','criterias.id')
                                ->leftJoin('supports','impacts.supportid_imp','=','supports.id')
                                ->where("impact_clients.impactid_imp_cl",$impactCl[0]->impactid_imp_cl)
                                ->where("impact_clients.criteriaid_imp_cl",$impactCl[0]->criteriaid_imp_cl)
                                ->first();
                    
                    $this->array_sort_by_column($aImpactCriteria[$impactCl[0]->criteriaid_imp_cl], "date_imp");                                
                }
            }
        }
        return view('emails.previmpactsmail',compact('aImpactCriteria'));
    }
    function array_sort_by_column(&$array, $column, $direction = SORT_DESC) {
        $reference_array = array();
    
        foreach($array as $key => $row) {
            $reference_array[$key] = $row[$column];
        }
    
        array_multisort($reference_array, $direction, $array);
    }

    /**
    * search impact reviews
    */
    public function searchImpact(Request $request){
        $data = $request->all();
        //dd($data);
        $aImpacts = Impact::leftJoin('impact_clients','impacts.id','=','impact_clients.impactid_imp_cl');
        $aImpacts->leftJoin('supports','impacts.supportid_imp','=','supports.id');
        $aImpacts->leftJoin('formats','impacts.formatid_imp','=','formats.id');
        $aImpacts->leftJoin('appears','impacts.appearid_imp','=','appears.id');
        $aImpacts->leftJoin('users','impacts.userid_imp','=','users.id');

        if(isset($data['datebegin']) && isset($data['dateend'])){

            $datestart = date('Y-m-d', strtotime($data['datebegin']));
            $dateend = date('Y-m-d', strtotime($data['dateend']));

            $aImpacts->whereBetween('date_imp',array($datestart,$dateend));
        }

        if(isset($data['authorid'])){
            $aImpacts->where('impacts.authorid_imp',$data['authorid']);
        }

        if(isset($data['paceid'])){
            $aImpacts->where('impacts.pace_imp',$data['paceid']);
        }

        if(isset($data['title'])){
            $aImpacts->where('impacts.title_imp',$data['title']);
        }
        if(isset($data['nature'])){
            $aImpacts->where('impacts.nature_imp',$data['nature']);
        }

        if(isset($data['language'])){
            $aImpacts->where('impacts.language_imp',intval($data['language']));
        }

        if(isset($data['print'])){
            $aImpacts->where('formats.print_frm',$data['print']);
        }
        if(isset($data['pubValue'])){
            $aImpacts->where('formats.value_ad_frm',$data['pubValue']);
        }
        if(isset($data['appear'])){
            $aImpacts->where('appears.id',$data['appear']);
        }

        if(isset($data['clientid'])){
            $aImpacts->where('impact_clients.clientid_imp_cl',$data['clientid']);
        }
        if(isset($data['criteria'])){
            $aImpacts->where('impact_clients.criteriaid_imp_cl',$data['criteria']);
        }

        if(isset($data['catSupport'])){
            $aImpacts->where('supports.cat_sup',$data['catSupport']);
        }

        if(isset($data['editOrientation'])){
            $aImpacts->where('supports.edit_orie_sup',$data['editOrientation']);
        }

        if(isset($data['periodReview'])){
            $aImpacts->where('supports.period_sup',$data['periodReview']);
        }

        if(isset($data['formatEdit'])){
            $aImpacts->where('supports.form_edit_sup',$data['formatEdit']);
        }

        if(isset($data['languageEdit'])){
            $aImpacts->where('supports.lng_edit_sup',$data['languageEdit']);
        }

        if(isset($data['supportid'])){
            $aImpacts->where('supportid_imp',$data['supportid']);
        }

        if (isset($data['period_tag_imp'])) {
            $aImpacts->where('period_tag_imp',$data['period_tag_imp']);
        }
        
        //$aImpacts->select('impacts.id','title_imp','date_imp','supportid_imp')->get();

        /* ->where('supportid_imp',$data['supportid'])
        ->where('impact_clients.clientid_imp_cl',$data['clientid'])
        ->select('id','title_imp','date_imp','supportid_imp')->get();*/
        //dd($aImpacts->get());
        //$aImpacts->get();
        //dd($aImpacts->select('impacts.id as impctid','impacts.*','impact_clients.*','supports.*')->toSql());

        $aImpactTemp = $aImpacts->select('impacts.id as impctid','impacts.*','impact_clients.*','supports.*','users.*')->get();

        //dd($aImpactTemp);

        foreach ($aImpactTemp as $key => $impact) {
            //dd($impact);
            $impact->support_name = $impact->support->name_sup;
        } 

        //dd(Datatables::of(Impact::query())->make(true));
        //return Datatables::of(Impact::query())->make();
        return response()->json($aImpactTemp);
    }

    public function ajaxTestDatatable(Request $request){
        $data = $request->all();

        $aImpacts = Impact::leftJoin('impact_clients','impacts.id','=','impact_clients.impactid_imp_cl');
        $aImpacts->leftJoin('client_companies','impact_clients.clientid_imp_cl','=','client_companies.id');
        $aImpacts->leftJoin('criterias','impact_clients.criteriaid_imp_cl','=','criterias.id');
        $aImpacts->leftJoin('subcriterias','impact_clients.subcriteriaid_imp_cl','=','subcriterias.id');
        //dd($aImpacts);
        if (isset($request['dateSearch']) && $request['dateSearch']!= "") {
            
            $datestart = date('Y-m-d', strtotime($data['dateSearch']));
            $aImpacts->whereBetween('impacts.date_imp',array($datestart,$datestart));
            // $aImpacts->where('impacts.date_imp','2019-01-08');
        }
        if (isset($request['searchValue']) && $request['searchValue']!= "") {
            $aImpacts->where('client_companies.id',$request['searchValue']);
        }
        if (isset($request['criteriaValue']) && $request['criteriaValue']!= ""){
            
            $aImpacts->where('criterias.id',$request['criteriaValue']);
            //dd($aImpacts->get());
        }
        if (isset($request['subcriteriaValue']) && $request['subcriteriaValue']!= ""){
            
            $aImpacts->where('subcriterias.id',$request['subcriteriaValue']);
            //dd($aImpacts->get());
        }
        //dd($aImpacts->get());
        //$imapcts = $aImpacts->get();
        $imapcts = Impact::join('impact_clients','impact_clients.impactid_imp_cl','=','impacts.userid_imp')
                                ->join('client_companies','impact_clients.clientid_imp_cl','=','client_companies.id')
                                ->join('users','impacts.userid_imp','=','users.id')
                                ->select('impacts.*','users.id as userid','users.role_id')
                                ->where("impacts.userid_imp",Auth::user()->id)
                                ->get();

        foreach ($imapcts as $key => $impact) {
            $clientImpacts = ImpactClient::whereImpactid_imp_cl($impact->id)->get();
            $aClientCompagny = [];
            $aCriteriaImpact = [];
            $aSubCriteriaImpact = [];
            foreach($clientImpacts as $j => $clientImpact) {
                $criteria = Criteria::find($clientImpact->criteriaid_imp_cl);
                $subCriteria = Subcriteria::find($clientImpact->subcriteriaid_imp_cl);

                $aClientCompagny[] = ClientCompany::find($clientImpact->clientid_imp_cl);
                $aCriteriaImpact[] = (isset($criteria) && !is_null($criteria) ? $criteria : []);
                $aSubCriteriaImpact[] = (isset($subCriteria) && !is_null($subCriteria) ? $subCriteria : []);
            }
            $impact->supportName = Support::find($impact->supportid_imp)->name_sup;
            //dd($impact);
            //->whereName_sup($request['searchValue'])
            $impact->clientCompany = (isset($aClientCompagny[0]) && !empty($aClientCompagny[0]))? $aClientCompagny[0]->nom_cl : NULL;
            $impact->criteriaImpact = (isset($aCriteriaImpact[0]) && !empty($aCriteriaImpact[0]))? $aCriteriaImpact[0]->label_cri : NULL;
            $impact->subCriteriaImpact = (isset($aSubCriteriaImpact[0]) && !empty($aSubCriteriaImpact[0]))? $aSubCriteriaImpact[0]->label_subcr : NULL;
            //$impact->subCriteriaImpact = $aSubCriteriaImpact;
            //dd($impact);
        }
        //dd($imapcts);
        return Datatables::collection($imapcts)->make();
    }
}
