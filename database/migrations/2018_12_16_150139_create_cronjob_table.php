<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronjobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cronjob', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 40);
            $table->text('data')->nullable();
            $table->integer('user_id')->unsigned()->default(0); // user who created the cron
            $table->datetime('executed_at')->nullable(); // the execution start time
            $table->datetime('completed_at')->nullable(); // the complete timestamp

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cronjob');
    }
}
