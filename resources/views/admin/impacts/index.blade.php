@extends('admin.layouts.master')

@section('content')
<style>
    #linkimpact, #linkedit{
        /* background:url('/public/image/Logodelete.png') no-repeat 0px 0px; */
        display: inline-block;
        width: 30px;
        height: 34px;
        float: left;
    }
    input.pdf-submit{
        background: url('/tlredp/public/image/Logopdf.png') no-repeat;
        display: inline-block;
        height: 30px;
        width: 30px;
        background-size: 30px 30px;
        border: 0;
    }
    input.delete-submit{
        background: url('/tlredp/public/image/Logodelete.png') no-repeat;
        display: inline-block;
        height: 30px;
        width: 30px;
        background-size: 30px 30px;
        border: 0;
    }
    input.summarypdf-submit {
        background: url('/tlredp/public/image/resume.png') no-repeat;
        display: inline-block;
        height: 30px;
        width: 30px;
        background-size: 30px 30px;
        border: 0;
    }
</style>
@if(Auth::user()->role_id == config('quickadmin.defaultRole') || Auth::user()->role_id == config('quickadmin.chargeveille') || Auth::user()->role_id == config('quickadmin.revue_audiovisuelle'))
    <p>{!! link_to_route('impacts.create', trans('quickadmin::admin.users-index-add_new'), [], ['class' => 'btn btn-success']) !!}</p>
@endif
<div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.impact-index-impact_list') }}</div>
            </div>
            <input type="text" class="datepicker date-list" id="date-impact-list" placeholder="Date">
            <div class="form-group">
                {!! Form::label('client_review', trans('quickadmin::admin.impact-create-clientcmp'), ['class'=>'col-sm-2 control-label']) !!}
                <div class="">
                    {{ Form::select('client_review',\App\ClientCompany::pluck('nom_cl','id'),'',['placeholder' => 'Choisir un client','id'=>'client-filter-impact','class'=>'form-control']) }}
                </div>
            </div>
            <div class="form-group">
                    {!! Form::label('criteria_review', trans('quickadmin::admin.criteria-create-labelcr'), ['class'=>'col-sm-2 control-label']) !!}
                    <div class="">
                            {{ Form::select('criteria_review', \App\Criteria::pluck('label_cri', 'id'),'',['placeholder' => 'Choisir critère','id'=>'criteria-filter-impact','class'=>'form-control']) }}
                    </div>
            </div>
            <div class="form-group">
                    {!! Form::label('subriteria_review', trans('quickadmin::admin.criteria-create-labelcr'), ['class'=>'col-sm-2 control-label']) !!}
                    <div class="">
                            {{ Form::select('subcriteria_review', \App\Subcriteria::pluck('label_subcr', 'id'),'',['placeholder' => 'Choisir subcritère','id'=>'subcriteria-filter-impact','class'=>'form-control']) }}
                    </div>
            </div>
            
            <div class="portlet-body">
                <table id="datatable" style="width:100% !important;" class="table table-striped table-hover table-responsive datatableImpact">
                    <thead>
                        <tr>
                            <th>{{ trans('quickadmin::admin.impact-index-name')  }}</th>
                            <th>{{ trans('quickadmin::admin.impact-index-date-parution') }}</th>
                            <th>{{ trans('quickadmin::admin.impact-index-title') }}</th>
                            <th>{{ trans('quickadmin::admin.impact-create-clientcmp')}}</th>
                            <th>{{ trans('quickadmin::admin.client-create-newcriteriacl')}}</th>
                            <th>{{ trans('quickadmin::admin.subcriteria-labelsubcr')}}</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>

                    <tbody>
                    
                    </tbody>
                </table>
            </div>
        </div>
    <script type="text/javascript">
        $(document).ready(function(){
            var baseUrl = window.location .protocol + "//" + window.location.host + "/" + window.location.pathname.split('/')[1];

            $('#client-filter-impact').chosen();
            $('#criteria-filter-impact').chosen();
            $('#subcriteria-filter-impact').chosen();

            $('#client-filter-impact').chosen().change(function() {
                console.log("chosen ",$(this).val());
                oTable.api().columns(3).search($(this).val()).draw();
            });

            $('#criteria-filter-impact').chosen().change(function() {
                console.log("chosen ",$(this).val());
                oTable.api().columns(4).search($(this).val()).draw();
            });

            $('#subcriteria-filter-impact').chosen().change(function() {
                console.log("chosen ",$(this).val());
                oTable.api().columns(5).search($(this).val()).draw();
            });

            //DataTable
            var oTable = $('.datatableImpact').on( 'draw.dt',  function () {
                
                console.log("draw",oTable.api());
                //oTable.api().column(1).search("04")
                // oTable.api().columns().every( function () {
                //         console.log("columns ",$(this).search());
                //     })
                })
            .dataTable({
                retrieve: true,
                serverSide: true,
                "drawCallback": function( settings ) {
                        var api = this.api();
                        // Output the data for the visible rows to the browser's console
                        console.log( "draw callback > ",api.rows( {page:'current'} ).data());
                },
                ajax: {
                    headers: {
                        'X-CSRF-TOKEN': $('input[name="_token"]').attr('value')
                    },
                    url: '{{URL::to("ajax/ajaxdt")}}',
                    type: 'POST',
                    data: function (d) {
                        console.log("data ", d);
                       console.log("data >>>",d.columns[1].search.value);
                       d.searchValue = d.columns[3].search.value;
                       d.criteriaValue = d.columns[4].search.value;
                       d.subcriteriaValue = d.columns[5].search.value;
                       d.dateSearch = d.columns[1].search.value;
                       console.log("data dateSearch >>>",d.dateSearch);
                       console.log("data searchValue >>>",d.searchValue);
                       console.log("data criteriaValue>>>",d.criteriaValue);
                       console.log("data subcriteriaValue>>>",d.subcriteriaValue);
                    }
                },
                "iDisplayLength": 10,
                "aaSorting": [],
                "columns": [
                    { data: '26' },
                    { data: '8' },
                    { data: '1' },
                    { data: '24' },
                    { data: '25' },
                    { data: '26' },
                 ],
                "columnDefs": [
                {
                    "visible": true, 
                    "targets": [ 0 ],
                    "searchable": true
                },
                {
                    "visible": true, 
                    "targets": [ 1 ],
                    "searchable": false
                },
                {
                    "visible": false, 
                    "targets": [ 3 ],
                    "searchable": false
                },
                {
                    "visible": false, 
                    "targets": [ 4 ],
                    "searchable": false
                },
                {
                    "visible": false, 
                    "targets": [ 5 ],
                    "searchable": false
                },
                {
                    "targets": 1,
                    "data": null,
                    "render": function ( data, type, row ) {
                        //formtargetconsole.log("date ",row[8]);
                        var formattedDate = new Date(row[8]);
                        var d = formattedDate.getDate();
                        var day = "";
                        var month = "";

                        if (d >= 10){
                            day = d
                        } else {
                            day = "0"+d
                        }
                        var m =  formattedDate.getMonth();
                        m += 1;  // JavaScript months are 0-11
                        if (m >= 10){
                            month = d
                        } else {
                            month = "0"+m
                        }
                        
                        var y = formattedDate.getFullYear();
                        //console.log(day+"/"+month+"/"+y);
                        return day+"-"+month+"-"+y;
                    },
                },
                {
                    "targets": 2,
                    "data": null,
                    "render": function ( data, type, row ) {
                        console.log("data title ",row[0]);
                        console.log("data title ",data);
                        var titleHtml = "<a href='/tlredp/public/admin/impacts-detail/"+row[0]+"'>"+data+"</a>";
                        
                        return titleHtml
                    },
                },
                {
                    "targets": 6,
                    "data": null,
                    "render": function ( data, type, row ) {
                        
                        if (data!=null && typeof data != "undefined"){
                            console.log("row ",data[17]);
                            return "<a href='"+data[17]+"' id='linkimpact' target='_blank'><image src='/tlredp/public/image/Logourl.png' style='width: 30px;height: 30px;' /></a>";
                        } else {
                            return "";
                        }
                    },
                },
                {
                    "targets": 7,
                    "data": null,
                    "render": function ( data, type, row ) {
                        //console.log("row ",data[16]);
                        var certificatForm = $('input[name="_token"]').attr('value');
                        var urlprintPost = '{{ URL::to("admin/print/impact") }}';
                        //console.log("token ",certificatForm);
                        var form = '<form method="POST" action="'+urlprintPost+'" accept-charset="UTF-8" class="form-horizontal" style="float: right;"><input name="_token" type="hidden" value="'+certificatForm+'">';
                        form += '<input name="impactid" type="hidden" value="'+data[0]+'">';
                        form += '<input class="pdf-submit" formtarget="_blank" type="submit" value="" ></form>';
                        return form;
                    },
                },
                {
                    
                    "targets": 8,
                    "data": null,
                    "render": function ( data, type, row ) {
                        console.log("row ",data[0]);
                        var url =  '{{ URL::to("admin/impacts/edit/") }}/'+data[0];
                        //console.log(url);
                        return "<a href='"+url+"' id='linkedit'><image src='/tlredp/public/image/Logoedit.png' style='width: 30px;height: 30px;' /></a>";
                    },
                },
                {
                    "targets": 9,
                    "data": null,
                    "render": function ( data, type, row ) {
                        // role user 
                        if (data[25] != 2) {
                            var certificatForm = $('input[name="_token"]').attr('value');
                        var urldeletePost = '{{ URL::to("admin/impacts/destroy/") }}/'+data[0];

                        var form = '<form method="POST" action="'+urldeletePost+'" accept-charset="UTF-8" class="delete-form-impact" style="display: inline-block;"><input name="_token" type="hidden" value="'+certificatForm+'">';
                        form += '<input name="_method" type="hidden" value="DELETE">';
                        form += '<input class="delete-submit" type="submit" value=""></form>';
                        $( ".delete-form-impact").submit(function( event ) {
                            //console.log(row);
                            if (confirm("Are you sure?")){

                            }else {
                                event.preventDefault();
                            }
                            
                        });
                        return form;
                        } else {
                            return "";
                        }
                        
                    }
                },
                {
                    "targets": 10,
                    "data": null,
                    "defaultContent": ""
                }
                ]
            });

            // console.log("draw",oTable.api().column(3).search("2m.ma"));

            // date
            $("#date-impact-list").datepicker({
                dateFormat: "dd-mm-yy",
                onSelect: function(date) {
                    console.log(date);
                    oTable.api().columns(1).search($(this).val()).draw();
                }
            });

            console.log("ready");
            $("input.date-list").keyup(function () {
                /* Filter on the column (the index) of this element */
                oTable.fnFilter( this.value, 1);
            });
            $("input.clientNameFiltre").keyup(function () {
                /* Filter on the column (the index) of this element */
                //oTable.fnFilter( this.value, 3);
                oTable.api().column(0).search(this.value).draw();
            } );
            $("input.criteriaFiltre").keyup(function () {
                console.log($(this));
                console.log(this.value)
                /* Filter on the column (the index) of this element */
                oTable.fnFilter( this.value, 4);
            } );
            $("input.subCriteriaFiltre").keyup(function () {
                console.log($(this));
                console.log(this.value)
                /* Filter on the column (the index) of this element */
                oTable.fnFilter( this.value, 5);
            } );
        });
    </script>
@endsection