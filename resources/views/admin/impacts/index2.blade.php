@extends('admin.layouts.master')

@section('content')
<style>
    #linkimpact, #linkedit{
        /* background:url('/public/image/Logodelete.png') no-repeat 0px 0px; */
        display: inline-block;
        width: 30px;
        height: 34px;
        float: left;
    }
    input.pdf-submit{
        background: url('/tlredp/public/image/Logopdf.png') no-repeat;
        display: inline-block;
        height: 30px;
        width: 30px;
        background-size: 30px 30px;
        border: 0;
    }
    input.delete-submit{
        background: url('/tlredp/public/image/Logodelete.png') no-repeat;
        display: inline-block;
        height: 30px;
        width: 30px;
        background-size: 30px 30px;
        border: 0;
    }
    input.summarypdf-submit {
        background: url('/tlredp/public/image/resume.png') no-repeat;
        display: inline-block;
        height: 30px;
        width: 30px;
        background-size: 30px 30px;
        border: 0;
    }
</style>
@if(Auth::user()->role_id == config('quickadmin.defaultRole') || Auth::user()->role_id == config('quickadmin.chargeveille'))
    <p>{!! link_to_route('impacts.create', trans('quickadmin::admin.users-index-add_new'), [], ['class' => 'btn btn-success']) !!}</p>
@endif
@if($imapcts->count() > 0)
<div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">{{ trans('quickadmin::admin.impact-index-impact_list') }}</div>
            </div>
            <input type="text" class="datepicker date-list" id="date-impact-list" placeholder="Date">
            <input type="text" name="client-name" class="clientNameFiltre" placeholder="{{ trans('quickadmin::admin.impact-create-clientcmp')}}" />
            <input type="text" name="criteria" class="criteriaFiltre" placeholder="{{ trans('quickadmin::admin.client-create-newcriteriacl')}}" />
            <input type="text" name="subCriteria" class="subCriteriaFiltre" placeholder="{{ trans('quickadmin::admin.subcriteria-labelsubcr')}}" />
            <div class="portlet-body">
                <table id="datatable" style="width:100% !important;" class="table table-striped table-hover table-responsive datatableImpact">
                    <thead>
                        <tr>
                            <th>{{ trans('quickadmin::admin.impact-index-name')  }}</th>
                            <th>{{ trans('quickadmin::admin.impact-index-date-parution') }}</th>
                            <th>{{ trans('quickadmin::admin.impact-index-title') }}</th>
                            <th>{{ trans('quickadmin::admin.impact-create-clientcmp')}}</th>
                            <th>{{ trans('quickadmin::admin.client-create-newcriteriacl')}}</th>
                            <th>{{ trans('quickadmin::admin.subcriteria-labelsubcr')}}</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach ($imapcts as $impact)
                        <tr>
                            <td class="uppercase-text">{{ $impact->support->name_sup }}</td>
                            <td>{{ Carbon\Carbon::parse($impact->date_imp)->format('d-m-Y') }}</td>
                            <td>{{ HTML::linkRoute('impacts.detail.index',$impact->title_imp,array('id'=>$impact->id)) }}</td>
                            <td>
                                @foreach ($impact->clientCompany as $client)
                                    <span>{{ $client->nom_cl }}</span>
                                @endforeach
                            </td>
                            <td>
                                @foreach ($impact->criteriaImpact as $criteria)
                                    <span>{{ $criteria->label_cri }}</span>
                                @endforeach
                            </td>
                            <td>
                                @foreach ($impact->subCriteriaImpact as $subCriteria)
                                    @if(!is_array($subCriteria))
                                        <span>{{ $subCriteria->label_subcr }}</span>
                                    @endif
                                @endforeach
                            </td>
                            <td>
                                @if($impact->link_imp)
                                    <a href="{{ $impact->link_imp }}" class="" id="linkimpact" target="_blank">
                                        <image src="/tlredp/public/image/Logourl.png" style="width: 30px;height: 30px;" />
                                    </a>
                                @endif
                            </td>
                            <td>{{-- PDF PRINT --}}
                                {!! Form::open(['route' => 'impacts.print.tcpdf', 'class' => 'form-horizontal','style'=>'float: right;'])!!}
                                {!! Form::hidden('impactid',$impact->id, []) !!}
                                {!! Form::submit('', array('class' => 'pdf-submit','formtarget'=>'_blank')) !!}
                                {!! Form::close() !!}
                            </td>
                            <td>
                                <a href="{{ url('admin/impacts/edit/'.$impact->id) }}" id="linkedit">
                                    <image src="/tlredp/public/image/Logoedit.png" style="width: 30px;height: 30px;" /> 
                                </a>
                            </td>
                            <td>
                                {!! Form::open(['style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => 'return confirm(\'' . trans('quickadmin::admin.users-index-are_you_sure') . '\');',  'route' => array('impact.destroy', $impact->id)]) !!}
                                {!! Form::submit('', array('class' => 'delete-submit')) !!}
                                {!! Form::close() !!}
                            </td>
                            <td>
                            @if($impact->resume_imp)
                                {!! Form::open(['route' => 'impacts.print.resumepdf', 'class' => 'form-horizontal','style'=>'float: right;'])!!}
                                {!! Form::hidden('impactid',$impact->id, []) !!}
                                {!! Form::submit('', array('class' => 'summarypdf-submit','formtarget'=>'_blank')) !!}
                                {!! Form::close() !!}
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @else
        {{ trans('quickadmin::admin.users-index-no_entries_found') }}
    @endif
    <script type="text/javascript">
        $(document).ready(function(){
            
            //DataTable
            var oTable = $('.datatableImpact').dataTable({
                retrieve: true,
                "iDisplayLength": 25,
                "aaSorting": [],
                "columnDefs": [
                        {
                            "targets": [ 1 ],
                            "visible": true,
                            "searchable": true
                        },
                        {
                            "targets": [ 3 ],
                            "visible": false,
                            "searchable": true
                        },
                        {
                            "targets": [ 4 ],
                            "visible": false,
                            "searchable": true
                        },
                        {
                            "targets": [ 5 ],
                            "visible": false,
                            "searchable": true
                        }
                ]
            });
            // date
            $("#date-impact-list").datepicker({
                dateFormat: "dd-mm-yy",
                onSelect: function(date) {
                    console.log(date);
                    oTable.fnFilter( this.value, 1);
                }
            });

            console.log("ready");
            $("input.date-list").keyup(function () {
                /* Filter on the column (the index) of this element */
                oTable.fnFilter( this.value, 1);
            });
            $("input.clientNameFiltre").keyup(function () {
                /* Filter on the column (the index) of this element */
                oTable.fnFilter( this.value, 3);
            } );
            $("input.criteriaFiltre").keyup(function () {
                console.log($(this));
                console.log(this.value)
                /* Filter on the column (the index) of this element */
                oTable.fnFilter( this.value, 4);
            } );
            $("input.subCriteriaFiltre").keyup(function () {
                console.log($(this));
                console.log(this.value)
                /* Filter on the column (the index) of this element */
                oTable.fnFilter( this.value, 5);
            } );
        });
    </script>
@endsection