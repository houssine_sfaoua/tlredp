<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appear extends Model
{
    protected $table = "appears";
    
    public $timestamps = false;

    protected $fillable = array("label_appe");
}
