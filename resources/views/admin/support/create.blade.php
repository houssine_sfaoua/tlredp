@extends('admin.layouts.master') 
<style>
	#reset-logo-image{
	display: inline-block;
    width: 49.6%;
    height: 33px;
    padding-top: 6px;
    margin-top: 13px;
    margin-bottom: 16px;
    margin-left: 0 !important;
}
</style>
@section('content') 

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{!! Form::open(['route' => 'support.store', 'class' => 'form-horizontal'])!!}
<div class="form-group">
	{!! Form::label('name_sup', trans('quickadmin::admin.support-create-name'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('name_sup', "", ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.support-create-name')])
		!!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('logosup', trans('quickadmin::admin.support-create-logo'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::file('logosup', ['class' => 'form-control','style'=>'display:none','id'=>'logosup-file']) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-10 col-sm-offset-2">
		<img src="#" style="display:none;" id="img-logo-support">
		<button id="reset-logo-image" class="btn btn-xs btn-danger col-md-6 col-md-offset-2" >Reset</button>
		<input type="text" id="logo-data" name="logo" style="display:none;" />
	</div>
</div>

<a href="#" id="openFileDialog" class="btn btn-xs btn-danger col-md-6 col-md-offset-2" style="width: 51%;height: 33px;padding-top: 6px;">Open</a>
<div style="clear:both;"></div>

<div class="form-group">
	{!! Form::label('origin_sup', trans('quickadmin::admin.support-create-origin'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('origin_sup', ['Presse nationale'=>'Presse nationale', 'Presse internationale'=>'Presse internationale'],'',['class'=>'form-control']) }}
	</div>
</div>

<div class="form-group">
	{!! Form::label('cat_sup', trans('quickadmin::admin.support-create-catsup'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('cat_sup', ['Agence de presse'=>'Agence de presse','Portail du Maroc'=>'Portail du Maroc','Presse électronique'=>'Presse électronique','Presse Papier'=>'Presse Papier'],'',['class'=>'form-control'])
		}}
	</div>
</div>

{{-- <div class="form-group">
	{!! Form::label('subcat_sup', trans('quickadmin::admin.support-create-subcatsupp'), ['class'=>'col-sm-2 control-label'])
	!!}
	<div class="col-sm-10">
		{{ Form::select('subcat_sup', ['Presse audiovisuelle'=>'Presse audiovisuelle', 'Presse écrite'=>'Presse écrite'],'',['class'=>'form-control']) }}
	</div>
</div> --}}

<div class="form-group">
	{!! Form::label('edit_orie_sup', trans('quickadmin::admin.support-create-orientation'), ['class'=>'col-sm-2 control-label'])
	!!}
	<div class="col-sm-10">
		{{ Form::select('edit_orie_sup', ['Presse partisane'=>'Presse partisane', 'Presse indépendante'=>'Presse indépendante','Presse officielle'=>'Presse officielle'],'',['class'=>'form-control'])
		}}
	</div>
</div>

<div class="form-group">
	{!! Form::label('period_sup', trans('quickadmin::admin.support-create-period'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('period_sup', ['Bi-mensuel'=>'Bi-mensuel','Quotidien'=>'Quotidien', 'Hebdomadaire'=>'Hebdomadaire','Mensuel'=>'Mensuel','Bimestriel'=>'Bimestriel','Trimestriel'=>'Trimestriel','En continu'=>'En continu'],'',['class'=>'form-control'])
		}}
	</div>
</div>

<div class="form-group">
	{!! Form::label('form_edit_sup', trans('quickadmin::admin.support-create-typeedit'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('form_edit_sup', ['Journal'=>'Journal', 'Magazine'=>'Magazine','En ligne'=>'En ligne'],'',['class'=>'form-control'])
		}}
	</div>
</div>

<div class="form-group">
	{!! Form::label('lng_edit_sup', trans('quickadmin::admin.support-create-lngedit'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('lng_edit_sup', ['Arabophone'=>'Arabophone', 'Francophone'=>'Francophone','Espagnol'=>'Espagnol','Anglophone'=>'Anglophone','Bilingue'=>'Bilingue','Trilingue'=>'Trilingue','Multilingue'=>'Multilingue'],'',['class'=>'form-control'])
		}}
	</div>
</div>

<div class="form-group">
	{!! Form::label('postition_sup', trans('quickadmin::admin.support-create-position'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{{ Form::select('postition_sup', ['Presse généraliste'=>'Presse généraliste','Presse MRE'=>'Presse MRE',
		'Presse officielle'=>'Presse officielle','Presse politique'=>'Presse politique','Presse politique (Sahara )'=>'Presse politique (Sahara )',
		'Presse RH'=>'Presse RH','Presse RSE'=>'Presse RSE','Presse satirique'=>'Presse satirique','Presse sportive'=>'Presse sportive',
		'Presse TIC'=>'Presse TIC','Presse Toursime'=>'Presse Toursime',
		'Presse medecine'=>'Presse medecine',
		'Presse masculine'=>'Presse masculine',
		'Presse maritime'=>'Presse maritime','Presse Management'=>'Presse Management',
		'Presse juridique'=>'Presse juridique','Presse Industrie'=>'Presse Industrie',
		'Presse Histoire'=>'Presse Histoire','Agregateur généraliste'=>'Agregateur généraliste',
		'presse financière'=>'presse financière','Presse feminine'=>'Presse feminine',
		'Presse économique'=>'Presse économique','Presse Ecologie & Environnement'=>'Presse Ecologie & Environnement','Presse eco & TIC'=>'Presse eco & TIC',
		'Presse Conso'=>'Presse Conso','Presse Cityguide'=>'Presse Cityguide',
		'Presse Art'=>'Presse Art','Presse auto/moto'=>'Presse auto/moto',
		'Presse BTP'=>'Presse BTP','Presse Art de vivre'=>'Presse Art de vivre',
		'Presse Agricole'=>'Presse Agricole','Presse Agroalimentaire'=>'Presse Agroalimentaire',
		'Généraliste'=>'Généraliste', 'économique'=>'économique','financier'=>'financier',
		'féminin'=>'féminin','satirique'=>'satirique','TIC'=>'TIC','Santé'=>'Santé','Industrie'=>'Industrie'],'',['class'=>'form-control'])
		}}
	</div>
</div>

<div class="form-group">
        {!! Form::label('format_id_sup', trans('quickadmin::admin.impact-create-format'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::select('format_id_sup', \App\Format::pluck('pseudo_frm', 'id'), null, ['class'=>'form-control','id'=>'format-select','multiple'=>'multiple']) !!}
        </div>
</div>

{!! Form::hidden('formatitems','', ['id'=>'formatitems']) !!}

<div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.criterias-create-btncreate'), ['class' => 'btn btn-primary']) !!}
        </div>
</div>


{!! Form::close() !!} 

<!-- Modal -->
<div id="fileModal" class="modal fade" role="dialog">
	<div class="modal-dialog" style="width: 100%;">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				{{-- <button type="button" class="close" data-dismiss="modal">&times;</button> --}}
				<h4 class="modal-title">Modal Header</h4>
			</div>
			<div class="modal-body">
				<div id="copper-container">
					<img id="crop-image" src="" style="">
					{{-- <canvas id="canvas" style=""></canvas> --}}
				</div>
				
			</div>	
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="crop-save">Enregistrer</button>
				<button type="button" class="btn btn-default"  id="close-modal">Annuler</button>
			</div>
		</div>

	</div>
</div>

<script src="{{ url('quickadmin/js') }}/support.js"></script>

@endsection