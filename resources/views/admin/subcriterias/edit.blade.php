@extends('admin.layouts.master')

@section('content')

{!! Form::open(['route' => 'subcriteria.store', 'class' => 'form-horizontal'])!!}

{!! Form::hidden('edit','', ['id'=>'support-id']) !!}

{!! Form::hidden('edit',$subcriteria->id, ['id'=>'subcriteria-id']) !!}

<div class="form-group">
	{!! Form::label('label_subcr', trans('quickadmin::admin.subcriteria-create-labelsubcr'), ['class'=>'col-sm-2 control-label']) !!}
	<div class="col-sm-10">
		{!! Form::text('label_subcr', $subcriteria->label_subcr, ['class'=>'form-control', 'placeholder'=> trans('quickadmin::admin.subcriteria-create-labelsubcr')]) !!}
	</div>
</div>

<div class="form-group">
        {!! Form::label('criteria_id_subcr', trans('quickadmin::admin.subcriteria-create-critid'), ['class'=>'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::select('criteria_id_subcr', \App\Criteria::pluck('label_cri', 'id'), null, ['class'=>'form-control','id'=>'critria-select']) !!}
        </div>
</div>

<div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            {!! Form::submit(trans('quickadmin::admin.criterias-create-btncreate'), ['class' => 'btn btn-primary']) !!}
        </div>
</div>

{!! Form::close() !!}

<script type="text/javascript">
$(document).ready(function(){
    
    var subcriteria = {!! json_encode($subcriteria) !!};

    console.log(subcriteria);

    $('#critria-select').chosen();

    $('#critria-select').val(subcriteria.criteria_id_subcr);
    $('#critria-select').trigger("chosen:updated").change();

    
});
</script>

@endsection