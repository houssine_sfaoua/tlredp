<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    protected $table = "supports";
    
    public $timestamps = false;

    protected $fillable = array("name_sup","origin_sup","cat_sup","edit_orie_sup","period_sup","form_edit_sup","lng_edit_sup","postition_sup"); 

    public function supportFormat()
    {
        return $this->hasMany('App\SupportFormat');
    }

    /**
    * get get subcriteria
    */
    /*public function subcriteria(){
        return $this->belongsTo('App\Subcriteria','subcrit_id_sup');
    }*/


    /**
    * get criteria
        */
    /*public function criteria(){
        return $this->belongsTo('App\Criteria','crit_id_sup');
    }*/


    
}
