<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Format extends Model
{
    protected $table = "formats";
    
    public $timestamps = false;

    protected $fillable = array("nom_frm","pseudo_frm","print_frm","page_frm","value_ad_frm","supportid_frm");

    public function isFormatSupport($formatid,$supportid){
       $aFormat = $this::where('id',$formatid)->where('supportid_frm',$supportid)->get();
       
       if(isset($aFormat) && $aFormat->count() > 0){
            return true;
       }else{
           return false;
       }
    }
}
