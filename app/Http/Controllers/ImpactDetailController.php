<?php

namespace App\Http\Controllers;

use App\Impact;
use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\Storage;
use App\ImpactClient;
use App\ClientCompany;
use App\PdfOrdersNum;
use PDFTCPDF;
use TCPDF_FONTS;

set_time_limit(0);
ini_set("memory_limit",-1);
ini_set('max_execution_time', 0);

class ImpactDetailController extends Controller
{
    
    public function index($id){
        //dd($id);

        $aImpact = Impact::find($id);

        $aImpact->authorobj = $aImpact->author;
        $aImpact->supportobj = $aImpact->support;
        $aImpact->rubriqueobj = $aImpact->rubrique;
        $aImpact->formatobj = $aImpact->format;
        //dd($aImpact);
        if(isset($aImpact->scan_imp)){
            $aImpact->images = json_decode($aImpact->scan_imp);
        }
        

        return view('admin.impact-detail.index',compact('aImpact'));
    }


    public function printPdf(Request $request){

        $data = $request->all();
        $aImpact = Impact::find($data["impactid"]);
        $date = \Carbon\Carbon::now();
        $clientImpact = ImpactClient::whereImpactid_imp_cl($aImpact->id)->first();
        

        $aImpact->authorobj = $aImpact->author;
        $aImpact->supportobj = $aImpact->support;
        $aImpact->rubriqueobj = $aImpact->rubrique;
        $aImpact->formatobj = $aImpact->format;
        $aImpact->clientCampany = ClientCompany::find($clientImpact->clientid_imp_cl);

        $aImpact->images = json_decode($aImpact->scan_imp);
        
        $namePdf =  str_replace([' ','.'],'_',$aImpact->supportobj->name_sup)."_".\Carbon\Carbon::parse($date)->format('d_m_Y');
        //dd($namePdf);
        $pdf = PDF::loadView('admin.pdf.test',compact('aImpact','namePdf'));
        print_r($pdf->stream($namePdf.'.pdf'));die;

        //return $pdf->download('test2.pdf');
        //return view('admin.pdf.test',compact('aImpact'));
    }

    public function printtcpdf(Request $request) {
        $data = $request->all();
        //dd($data);
        $aImpact = Impact::find($data["impactid"]);
        $date = \Carbon\Carbon::now();
        $clientImpact = ImpactClient::whereImpactid_imp_cl($aImpact->id)->first();
        
        $aImpact->authorobj = $aImpact->author;
        $aImpact->supportobj = $aImpact->support;
        $aImpact->rubriqueobj = $aImpact->rubrique;
        $aImpact->formatobj = $aImpact->format;
        $aImpact->clientCampany = ClientCompany::find($clientImpact->clientid_imp_cl);

        $aImpact->images = json_decode($aImpact->scan_imp);
        
        $namePdf =  str_replace([' ','.'],'_',$aImpact->supportobj->name_sup)."_".\Carbon\Carbon::parse($date)->format('d_m_Y')."_".PdfOrdersNum::getPdfNumOrder();

        /******************************************************************************** */
        PDFTCPDF::SetTitle($namePdf);
        PDFTCPDF::SetAutoPageBreak(false, 0);
        PDFTCPDF::setImageScale(PDF_IMAGE_SCALE_RATIO);

        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'ltr';
        $lg['a_meta_language'] = 'fr';
        $lg['w_page'] = 'page';

        $dinfontname = TCPDF_FONTS::addTTFfont('http://localhost:8888/tlredp/public/fonts/din-bold.ttf', 'TrueTypeUnicode', '', 32);
        $geometosfontname = TCPDF_FONTS::addTTFfont('http://localhost:8888/tlredp/public/fonts/Geometos.ttf', 'TrueTypeUnicode', '', 32);
        $geometosroundedfontname = TCPDF_FONTS::addTTFfont('http://localhost:8888/tlredp/public/fonts/Geometos_Rounded.ttf', 'TrueTypeUnicode', '', 32);
        $dinarabicfontname = TCPDF_FONTS::addTTFfont('http://localhost:8888/tlredp/public/fonts/DINNEXTLTARABIC-LIGHT.ttf', 'TrueTypeUnicode', '', 32);
    //dd($aImpact);
        // use the font
        PDFTCPDF::SetFont($geometosfontname, '', 13, '', false);

        // set some language-dependent strings (optional)
        PDFTCPDF::setLanguageArray($lg);
        $width = PDFTCPDF::getPageWidth();
        $heigh = PDFTCPDF::getPageHeight();

        // first page
        PDFTCPDF::AddPage();
        PDFTCPDF::SetFillColor(244, 244, 244);
        PDFTCPDF::writeHTMLCell($width, $heigh,0, 0, "", 0, 1, 1, true, '', false);
        PDFTCPDF::Image('http://localhost:8888/tlredp/public/image/pictoscan.png', 0, 0, $width, $heigh , 'PNG', '', '', false, 300, 'C', false, false, 0, 'CM', false, false);

            if(isset($aImpact->images))
            {

            foreach ($aImpact->images as $key=>$image)
            {

            //dd($aImpact);
            PDFTCPDF::AddPage();
            //PDFTCPDF::SetMargins(100, 100, 10, true);
            $html = "<h1>TEST TEST left </h1>";
            $righthtml = "<h1>Veille rédactionnelle</h1>";
            $clientName = "<h1></h1>";
            $container = "<div></div>";
            $descImpact = "<p>Article ".$aImpact->pace_imp." paru le ".\Carbon\Carbon::parse($aImpact->date_imp)->format('d-m-Y') ." dans la rubrique« ".$aImpact->rubriqueobj->nom_rub." », signé par ".$aImpact->authorobj->name_aut." <br/>Article de ".$aImpact->formatobj->nom_frm.", édité en ".$aImpact->formatobj->print_frm.", d’une valeur publicitaire équivalente à ".$aImpact->formatobj->value_ad_frm." MAD</p>";
            $containerImage = "<div></div>";
            $titleArticle = "<p>".$aImpact->title_imp."</p>";
            $supportDesc = "<p >".$aImpact->supportobj->form_edit_sup." . ".$aImpact->supportobj->period_sup ." . ".$aImpact->supportobj->lng_edit_sup." . ".$aImpact->supportobj->postition_sup."</p>";
            $y = PDFTCPDF::getY();
            

            $containerHeight = $heigh / 5;
            
            // dump($heigh);
            // dump($width);
            // dd($heigh - PDFTCPDF::getY());
            //$containerImageY = PDFTCPDF::getY();
            //PDFTCPDF::Rect(0, PDFTCPDF::getY(), $width,$heigh, 'F', array(), array(128,255,128));

            PDFTCPDF::SetFillColor(244, 244, 244);
            PDFTCPDF::writeHTMLCell($width, $containerHeight,0, 0, $container, 0, 1, 1, true, '', false);

            //PDFTCPDF::Image('http://localhost:8888/tlredp/storage/app/public/uploads/support/1/logo-2016.jpg', 0, 0, 75, 113, 'JPEG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 1, false, false, false);
            // LOGO
            PDFTCPDF::Image('http://localhost:8888/tlredp/public/image/logotlredp.png', 5, 5,40,'', 'PNG', '', 'T', false, 0, '', false, false, 1, false, false, false);
            //Image( $file, $x = '', $y = '', $w = 0, $h = 0, $type = '', $link = '', $align = '', $resize = false, $dpi = 300, $palign = '', $ismask = false, $imgmask = false, $border = 0, $fitbox = false, $hidden = false, $fitonpage = false, $alt = false, $altimgs = array() )

            // Print text using writeHTMLCell()
            PDFTCPDF::SetTextColor(0, 0, 0);
            // PDFTCPDF::SetFillColor(224, 82, 69);
            // //PDFTCPDF::Cell(0, 0, 'A4 PORTRAIT', 0, 0,'R');
            //dd(PDFTCPDF::getFontsList());
            // Veille redactionnelle title
            PDFTCPDF::SetFont($geometosfontname, '', PDFTCPDF::pixelsToUnits('13'), '', false);
            PDFTCPDF::writeHTMLCell(0,0, 5, 5, $righthtml, 0, 1, 1, true, 'R', true, true, '', false);
            // Client name
            PDFTCPDF::writeHTMLCell(0,0, 50, PDFTCPDF::getY(), $clientName, 0, 1, 1, true, 'R', true, true, '', false);
            //$this->Image($image_file, 90, 5, 40, '', 'PNG', '', 'T', false, 300, 'C', false, false, 0, false, false, false);
            // Support LOGO
            PDFTCPDF::Image('http://localhost:8888/tlredp/storage/app/public/uploads/support/'.$aImpact->supportobj->id.'/'.$aImpact->supportobj->logo_sup.'', 0, PDFTCPDF::getY() -2,20,'', 'PNG', '', 'T', false, 0, 'C', false, false, 1, false, false, false);
            // //writeHTMLCell( $w, $h, $x, $y, $html = '', $border = 0, $ln = 0, $fill = false, $reseth = true, $align = '', $autopadding = true )
            // // cell $w, $h = 0, $txt = '', $border = 0, $ln = 0, $align = '', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M' )
            // Title support
            PDFTCPDF::SetFont($geometosroundedfontname, '', 13, '', false);
            PDFTCPDF::writeHTMLCell(0,0, 5, PDFTCPDF::getImageRBY() + 3, $aImpact->supportobj->name_sup, 0, 1, 1, true, 'C', true, true, '', false);
            // Description Support
            PDFTCPDF::SetFont($geometosroundedfontname, '', 7, '', false);
            PDFTCPDF::writeHTMLCell(0,0, 5, PDFTCPDF::getY() + 1,$supportDesc, 0, 1, 1, true, 'C', true, true, '', false);
            PDFTCPDF::Line( 60, PDFTCPDF::getY() + 4, $width - 60 , PDFTCPDF::getY() + 4);

            //PDFTCPDF::SetFont ('helvetica', 'B' ,10, 'default', true );
            // PDFTCPDF::SetFontSize(9);
            // Title article
            PDFTCPDF::writeHTMLCell(0,0, 0, PDFTCPDF::getY() + 7, $titleArticle, 0, 1, 1, true, 'C', true, true, '', false);
            // Description Support / Format
            PDFTCPDF::SetFont($dinarabicfontname, 'B', 9, '', false);
            // PDFTCPDF::SetFont('aealarabiya', 'B', 9);
            PDFTCPDF::writeHTMLCell(0,0, 5, PDFTCPDF::getY(), $descImpact, 0, 1, 1, true, 'C', true, true, '', false);
            // Impact Image
            PDFTCPDF::Image('http://localhost:8888/tlredp/storage/app/public/uploads/impacts/'.$aImpact->id.'/'.$image->scan.'', 0, $containerHeight ,$width-50, $heigh - $containerHeight - 10, 'PNG', '', '', true, 300, 'C', false, false, 0, 'CM', false, false);
            
            PDFTCPDF::SetFillColor(244, 244, 244);
            PDFTCPDF::writeHTMLCell($width,10, 0, $heigh - 10, '', 0, 0, 1, true, 'C', true, true, '', false);
            }
        }// foreach image 
        // last page
        PDFTCPDF::AddPage();
        PDFTCPDF::SetFillColor(244, 244, 244);
        PDFTCPDF::writeHTMLCell($width, $heigh,0, 0, "", 0, 1, 1, true, '', false);
        PDFTCPDF::Image('http://localhost:8888/tlredp/public/image/logotlredp.png', 0, 0, $width - 150, $heigh , 'PNG', '', '', true, 300, 'C', false, false, 0, 'CM', false, false);
        PDFTCPDF::Output($namePdf.'.pdf');
    }

    public function printResumePdf(Request $request){
        $data = $request->all();
        $aImpact = Impact::find($data["impactid"]);
        $clientImpact = ImpactClient::whereImpactid_imp_cl($aImpact->id)->first();

        $aImpact->authorobj = $aImpact->author;
        $aImpact->supportobj = $aImpact->support;
        $aImpact->rubriqueobj = $aImpact->rubrique;
        $aImpact->formatobj = $aImpact->format;
        $aImpact->clientCampany = ClientCompany::find($clientImpact->clientid_imp_cl);

        //dd($aImpact);

        $pdf = PDF::loadView('admin.pdf.resumePdf',compact('aImpact'));
        print_r($pdf->stream('resumePdf.pdf'));die;
    }


    public function printWord(Request $request){


        $data = $request->all();

        $aImpact = Impact::find($data['impactidword']);

        $aImpact->authorobj = $aImpact->author;
        $aImpact->supportobj = $aImpact->support;
        $aImpact->rubriqueobj = $aImpact->rubrique;

        $aImpact->images = json_decode($aImpact->scan_imp);


        /***************************GENERATION WORD*****************************/
        $wordTest = new \PhpOffice\PhpWord\PhpWord();

        foreach ($aImpact->images as $key => $image) {
        $wordTest->addParagraphStyle('p2Style', array('align'=>'center', 'spaceAfter'=>100,'size'=>22));
        $wordTest->addParagraphStyle('textcenter', array('align'=>'center', 'spaceAfter'=>100));

        $newSection = $wordTest->addSection();

        $namesupporttxt = $aImpact->supportobj->name_sup;
if(isset($aImpact->supportobj->logo_sup)){
    $newSection->addImage(storage_path().'/app/public/uploads/support/'.$aImpact->supportobj->id.'/'.$aImpact->supportobj->logo_sup,array('height'=>'80'));
}else{
    
    $newSection->addImage(storage_path().'/app/public/uploads/support/1/logo-2016.jpg',array('height'=>'80'));
    
}
        

        $newSection->addText($namesupporttxt);
        

        $peridSuptxt = $aImpact->supportobj->period_sup;
        
        $newSection->addText($peridSuptxt);

        $newSection->addTextBreak();

        //$titleImpSection = $wordTest->addSection();

        $titleImpacttxt = $aImpact->title_imp;

        $newSection->addText($titleImpacttxt,array('size'=>22),'p2Style');

        $informationImpactTxt = "Article parue a ( date )en page ( page ) au format (publicitaire de tiers en hauteur)";

        $newSection->addText($informationImpactTxt);

        $newSection->addTextBreak();

        $newSection->addImage(storage_path().'/app/public/uploads/impacts/'.$aImpact->id.'/'.$image->scan,
        array('height'=>'400',
        'alignment'=>'center'));

        $newSection->addText(($key+1)."/".count($aImpact->images),'textcenter','textcenter');


        }



        //dd($wordTest);
       
        //array('name' => 'Tahoma', 'size' => 15, 'color' => 'red')
        //$newSection->addText($desc1);
    
        $objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($wordTest, 'Word2007');
        try {
            $objectWriter->save(storage_path('TestWordFile.docx'));
        } catch (Exception $e) {
            die($e->getMessage());
        }


        return response()->download(storage_path('TestWordFile.docx'));

    }
}
